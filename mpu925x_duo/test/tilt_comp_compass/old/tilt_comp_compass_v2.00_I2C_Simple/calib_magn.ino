
// -------------------------------
//  Calibrate magnetometer
// -------------------------------
void calibrate_magnetometer()
{
 // ----- Locals
 int mag_x, mag_y, mag_z;
 int status_reg_2;                                               // ST2 status register

 int mag_x_min =  32767;                                         // Raw data extremes
 int mag_y_min =  32767;
 int mag_z_min =  32767;
 int mag_x_max = -32768;
 int mag_y_max = -32768;
 int mag_z_max = -32768;

 float chord_x,  chord_y,  chord_z;                              // Used for calculating scale factors
 float chord_average;

 // ----- Display calibration message
 Serial.print("Rotate Compass");                                    // Print text to screen

 // ----- Record min/max XYZ compass readings
 for (int counter = 0; counter < 16000 ; counter ++)             // Run this code 16000 times
 {
   Loop_start = micros();                                        // Start loop timer
   if (counter % 1000 == 0)Serial.print(".");                        // Print a dot on the LCD every 1000 readings

   // ----- Point to status register 1
   Wire.beginTransmission(AK8963_I2C_address);                   // Open session with AK8963
   Wire.write(AK8963_status_reg_1);                              // Point to ST1[0] status bit
   Wire.endTransmission();
   Wire.requestFrom(AK8963_I2C_address, 1);                      // Request 1 data byte
   while (Wire.available() < 1);                                 // Wait for the data
   if (Wire.read() & AK8963_data_ready_mask)                     // Check data ready bit
   {
     // ----- Read data from each axis (LSB,MSB)
     Wire.requestFrom(AK8963_I2C_address, 7);                    // Request 7 data bytes
     while (Wire.available() < 7);                               // Wait for the data
     mag_x = (Wire.read() | Wire.read() << 8) * ASAX;            // Combine LSB,MSB X-axis, apply ASA corrections
     mag_y = (Wire.read() | Wire.read() << 8) * ASAY;            // Combine LSB,MSB Y-axis, apply ASA corrections
     mag_z = (Wire.read() | Wire.read() << 8) * ASAZ;            // Combine LSB,MSB Z-axis, apply ASA corrections
     status_reg_2 = Wire.read();                                 // Read status and signal data read

     // ----- Validate data
     if (!(status_reg_2 & AK8963_overflow_mask))                 // Check HOFL flag in ST2[3]
     {
       // ----- Find max/min xyz values
       mag_x_min = min(mag_x, mag_x_min);
       mag_x_max = max(mag_x, mag_x_max);
       mag_y_min = min(mag_y, mag_y_min);
       mag_y_max = max(mag_y, mag_y_max);
       mag_z_min = min(mag_z, mag_z_min);
       mag_z_max = max(mag_z, mag_z_max);
     }
   }
   delay(4);                                                     // Time interval between magnetometer readings
 }

 // ----- Calculate hard-iron offsets
 Mag_x_offset = (mag_x_max + mag_x_min) / 2;                     // Get average magnetic bias in counts
 Mag_y_offset = (mag_y_max + mag_y_min) / 2;
 Mag_z_offset = (mag_z_max + mag_z_min) / 2;

 // ----- Calculate soft-iron scale factors
 chord_x = ((float)(mag_x_max - mag_x_min)) / 2;                 // Get average max chord length in counts
 chord_y = ((float)(mag_y_max - mag_y_min)) / 2;
 chord_z = ((float)(mag_z_max - mag_z_min)) / 2;

 chord_average = (chord_x + chord_y + chord_z) / 3;              // Calculate average chord length

 Mag_x_scale = chord_average / chord_x;                          // Calculate X scale factor
 Mag_y_scale = chord_average / chord_y;                          // Calculate Y scale factor
 Mag_z_scale = chord_average / chord_z;                          // Calculate Z scale factor

 // ----- Record magnetometer offsets
 /*
    When active this feature sends the magnetometer data
    to the Serial Monitor then halts the program.
 */
 if (Record_data == true)
 {
   // ----- Display data extremes
   Serial.print("XYZ Max/Min: ");
   Serial.print(mag_x_min); Serial.print("\t");
   Serial.print(mag_x_max); Serial.print("\t");
   Serial.print(mag_y_min); Serial.print("\t");
   Serial.print(mag_y_max); Serial.print("\t");
   Serial.print(mag_z_min); Serial.print("\t");
   Serial.println(mag_z_max);
   Serial.println("");

   // ----- Display hard-iron offsets
   Serial.print("Hard-iron: ");
   Serial.print(Mag_x_offset); Serial.print("\t");
   Serial.print(Mag_y_offset); Serial.print("\t");
   Serial.println(Mag_z_offset);
   Serial.println("");

   // ----- Display soft-iron scale factors
   Serial.print("Soft-iron: ");
   Serial.print(Mag_x_scale); Serial.print("\t");
   Serial.print(Mag_y_scale); Serial.print("\t");
   Serial.println(Mag_z_scale);
   Serial.println("");

   // ----- Display fuse ROM values
   Serial.print("ASA: ");
   Serial.print(ASAX); Serial.print("\t");
   Serial.print(ASAY); Serial.print("\t");
   Serial.println(ASAZ);

   // ----- Halt program
   while (true);                                       // Wheelspin ... program halt
 }
}
