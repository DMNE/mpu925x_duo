
// ----- Libraries
#include <Wire.h>

// ----- LCD
int LCD_loop_counter;
int LCD_heading_buffer, LCD_pitch_buffer, LCD_roll_buffer;

// ----- Gyro
#define MPU9250_I2C_add0 0x68                                           // I2C address for MPU9250 with ad0 = 0
#define MPU9250_I2C_add1 0x69                                           // I2C address for MPU9250 with ad0 = 1
#define MPU9250_I2C_master_enable 0x6A                                  // USER_CTRL[5] = I2C_MST_EN
#define MPU9250_Interface_bypass_mux_enable 0x37                        // INT_PIN_CFG[1]= BYPASS_EN

int MPU9250_Add[] = {MPU9250_I2C_add0, MPU9250_I2C_add1};               // Massive for change address of MPU in programm.

#define Frequency 125                                                   // 8mS sample interval 
#define Sensitivity 65.5                                                // Gyro sensitivity (see data sheet)

#define Sensor_to_deg 1/(Sensitivity*Frequency)                         // Convert sensor reading to degrees
#define Sensor_to_rad Sensor_to_deg*DEG_TO_RAD                          // Convert sensor reading to radians

#define Loop_time 1000000/Frequency                                     // Loop time (uS)
long    Loop_start;                                                     // Loop start time (uS)

int     Gyro_x[] = {0 ,0}, Gyro_y[] = {0, 0}, Gyro_z[] = {0, 0};
long    Gyro_x_cal[] = {0, 0}, Gyro_y_cal[] = {0, 0}, Gyro_z_cal[] = {0, 0};
float   Gyro_pitch[] = {0, 0}, Gyro_roll[] = {0, 0}, Gyro_yaw[] = {0, 0};
float   Gyro_pitch_output[] = {0, 0}, Gyro_roll_output[] = {0, 0};

int     Gyro_5_x, Gyro_5_y, Gyro_5_z;
long    Gyro_5_x_cal, Gyro_5_y_cal, Gyro_5_z_cal;
float   Gyro_5_pitch, Gyro_5_roll, Gyro_5_yaw;
float   Gyro_5_pitch_output, Gyro_5_roll_output;

// ----- Accelerometer
long    Accel_x[] = {0, 0}, Accel_y[] = {0, 0}, Accel_z[] = {0, 0}, Accel_total_vector[] = {0, 0};
float   Accel_pitch[] = {0, 0}, Accel_roll[] = {0, 0};

long    Accel_5_x,      Accel_5_y,      Accel_5_z,    Accel_5_total_vector;
float   Accel_5_pitch,  Accel_5_roll;

// ----- Magnetometer
#define AK8963_I2C_address 0x0C                                             // I2C address for AK8963
#define AK8963_cntrl_reg_1 0x0A                                             // CNTL[4]=#bits, CNTL[3:0]=mode
#define AK8963_status_reg_1 0x02                                            // ST1[0]=data ready
#define AK8963_data_ready_mask 0b00000001                                   // Data ready mask
#define AK8963_overflow_mask 0b00001000                                     // Magnetic sensor overflow mask
#define AK8963_data 0x03                                                    // Start address of XYZ data                                                                
#define AK8963_fuse_ROM 0x10                                                // X,Y,Z fuse ROM

// ----- Compass heading
/*
 The magnetic declination for Lower Hutt, New Zealand is +22.5833 degrees
 Obtain your magnetic declination from http://www.magnetic-declination.com/
 Uncomment the declination code within the main loop() if you want True North.
*/
float   Declination = -12.14;                                             //  Degrees ... replace this declination with yours

int     Heading[] = {0, 0};

int     Mag_x[] = {0, 0}, Mag_y[] = {0, 0}, Mag_z[] = {0, 0};                  // Raw magnetometer readings
float   Mag_x_dampened[] = {0, 0}, Mag_y_dampened[] = {0, 0}, Mag_z_dampened[] = {0, 0};
float   Mag_x_hor[] = {0, 0}, Mag_y_hor[] = {0, 0};
float   Mag_pitch[] = {0, 0}, Mag_roll[] = {0, 0};

int     Heading_5;

int     Mag_5_x, Mag_5_y, Mag_5_z;                  // Raw magnetometer readings
float   Mag_5_x_dampened, Mag_5_y_dampened, Mag_5_z_dampened;
float   Mag_5_x_hor, Mag_5_y_hor;
float   Mag_5_pitch, Mag_5_roll;

// ----- Record compass offsets, scale factors, & ASA values
/*
  These values seldom change ... an occasional check is sufficient
  (1) Open your Arduino "Serial Monitor
  (2) Set "Record_data=true;" then upload & run program.
  (3) Replace the values below with the values that appear on the Serial Monitor.
  (4) Set "Record_data = false;" then upload & rerun program.
*/
bool    Record_data = false;

int     Mag_x_offset = 59,      Mag_y_offset = 302,     Mag_z_offset = -200;   // Hard-iron offsets for MPU-4
float   Mag_x_scale = 1.04,     Mag_y_scale = 1.01,     Mag_z_scale = 0.96;    // Soft-iron scale factors for MPU-4
float   ASAX = 1.18,            ASAY = 1.18,            ASAZ = 1.13;           // (A)sahi (S)ensitivity (A)djustment fuse ROM values for MPU-4.

int     Mag5_x_offset = -43,     Mag5_y_offset = 169,     Mag5_z_offset = -563;   // Hard-iron offsets for MPU-5
float   Mag5_x_scale = 1.04,     Mag5_y_scale = 1.03,     Mag5_z_scale = 0.93;    // Soft-iron scale factors for MPU-5
float   ASAX5 = 1.20,            ASAY5 = 1.20,            ASAZ5 = 1.16;           // (A)sahi (S)ensitivity (A)djustment fuse ROM values for MPU-5.

// ----- Pins
int AD4 = 11;                           // AD0 pin for MPU address 0x68
int AD5 = 12;                           // AD0 pin for MPU address 0x69

// ----- LED
const int LED = 13;                     // Status LED

// ----- Flags
bool Gyro_synchronised = false;
bool Flag = false;

// ----- Debug
#define Switch A0                       // Connect an SPST switch between A0 and GND to enable/disable tilt stabilazation
long Loop_start_time;
long Debug_start_time;

// --------------------
//  Read MPU 6050 data
// --------------------
void read_mpu_6050_data(int address, int i)
{
  /*
    Subroutine for reading the raw gyro and accelerometer data
  */

  // ----- Locals
  int     temperature;                                     // Needed when reading the MPU-6050 data ... not used

  // ----- Point to data
  Wire.beginTransmission(address);                         // Start communicating with the MPU-6050
  Wire.write(0x3B);                                        // Point to start of data
  Wire.endTransmission();                                  // End the transmission

  // ----- Read the data
  Wire.requestFrom(address, 14);                           // Request 14 bytes from the MPU-6050
  while (Wire.available() < 14);                           // Wait until all the bytes are received
  Accel_x[i] = Wire.read() << 8 | Wire.read();             // Combine MSB,LSB Accel_x variable
  Accel_y[i] = Wire.read() << 8 | Wire.read();             // Combine MSB,LSB Accel_y variable
  Accel_z[i] = Wire.read() << 8 | Wire.read();             // Combine MSB,LSB Accel_z variable
  temperature = Wire.read() << 8 | Wire.read();            // Combine MSB,LSB temperature variable
  Gyro_x[i] = Wire.read() << 8 | Wire.read();              // Combine MSB,LSB Gyro_x variable
  Gyro_y[i] = Wire.read() << 8 | Wire.read();              // Combine MSB,LSB Gyro_x variable
  Gyro_z[i] = Wire.read() << 8 | Wire.read();              // Combine MSB,LSB Gyro_x variable
}

// ----------------------------
//  Configure magnetometer
// ----------------------------
void configure_magnetometer(int address)
{
  /*
      The MPU-9250 contains an AK8963 magnetometer and an
      MPU-6050 gyro/accelerometer within the same package.

      To access the AK8963 magnetometer chip The MPU-9250 I2C bus
      must be changed to pass-though mode. To do this we must:
      - disable the MPU-9250 slave I2C and
      - enable the MPU-9250 interface bypass mux
  */
  // ----- Disable MPU9250 I2C master interface
  Wire.beginTransmission(address);                      // Open session with MPU9250
  Wire.write(MPU9250_I2C_master_enable);                            // Point USER_CTRL[5] = I2C_MST_EN
  Wire.write(0x00);                                                 // Disable the I2C master interface
  Wire.endTransmission();

  // ----- Enable MPU9250 interface bypass mux
  Wire.beginTransmission(address);                      // Open session with MPU9250
  Wire.write(MPU9250_Interface_bypass_mux_enable);                  // Point to INT_PIN_CFG[1] = BYPASS_EN
  Wire.write(0x02);                                                 // Enable the bypass mux
  Wire.endTransmission();

  // ----- Access AK8963 fuse ROM
  /* The factory sensitivity readings for the XYZ axes are stored in a fuse ROM.
      To access this data we must change the AK9863 operating mode.
  */
  Wire.beginTransmission(AK8963_I2C_address);                       // Open session with AK8963
  Wire.write(AK8963_cntrl_reg_1);                                   // CNTL[3:0] mode bits
  Wire.write(0b00011111);                                           // Output data=16-bits; Access fuse ROM
  Wire.endTransmission();
  delay(100);                                                       // Wait for mode change

  // ----- Get factory XYZ sensitivity adjustment values from fuse ROM
  /* There is a formula on page 53 of "MPU-9250, Register Map and Decriptions, Revision 1.4":
      Hadj = H*(((ASA-128)*0.5)/128)+1 where
      H    = measurement data output from data register
      ASA  = sensitivity adjustment value (from fuse ROM)
      Hadj = adjusted measurement data (after applying
  */
  Wire.beginTransmission(AK8963_I2C_address);                       // Open session with AK8963
  Wire.write(AK8963_fuse_ROM);                                      // Point to AK8963 fuse ROM
  Wire.endTransmission();
  Wire.requestFrom(AK8963_I2C_address, 3);                          // Request 3 bytes of data
  while (Wire.available() < 3);                                     // Wait for the data
  ASAX = (Wire.read() - 128) * 0.5 / 128 + 1;                       // Adjust data
  ASAY = (Wire.read() - 128) * 0.5 / 128 + 1;
  ASAZ = (Wire.read() - 128) * 0.5 / 128 + 1;

  // ----- Power down AK8963 while the mode is changed
  /*
      This wasn't necessary for the first mode change as the chip was already powered down
  */
  Wire.beginTransmission(AK8963_I2C_address);                       // Open session with AK8963
  Wire.write(AK8963_cntrl_reg_1);                                   // Point to mode control register
  Wire.write(0b00000000);                                           // Set mode to power down
  Wire.endTransmission();
  delay(100);                                                       // Wait for mode change

  // ----- Set output to mode 2 (16-bit, 100Hz continuous)
  Wire.beginTransmission(AK8963_I2C_address);                       // Open session with AK8963
  Wire.write(AK8963_cntrl_reg_1);                                   // Point to mode control register
  Wire.write(0b00010110);                                           // Output=16-bits; Measurements = 100Hz continuous
  Wire.endTransmission();
  delay(100);                                                       // Wait for mode change
}

// -----------------------------------
//  Configure the gyro & accelerometer
// -----------------------------------
void config_gyro(int address)
{
  // ----- Activate the MPU-6050
  Wire.beginTransmission(address);                         //Open session with the MPU-6050
  Wire.write(0x6B);                                     //Point to power management register
  Wire.write(0x00);                                     //Use internal 20MHz clock
  Wire.endTransmission();                               //End the transmission

  // ----- Configure the accelerometer (+/-8g)
  Wire.beginTransmission(address);                         //Open session with the MPU-6050
  Wire.write(0x1C);                                     //Point to accelerometer configuration reg
  Wire.write(0x10);                                     //Select +/-8g full-scale
  Wire.endTransmission();                               //End the transmission

  // ----- Configure the gyro (500dps full scale)
  Wire.beginTransmission(address);                         //Open session with the MPU-6050
  Wire.write(0x1B);                                     //Point to gyroscope configuration
  Wire.write(0x08);                                     //Select 500dps full-scale
  Wire.endTransmission();                               //End the transmission
}

// -----------------------------------
//  Calibrate gyro
// -----------------------------------
void calibrate_gyro(int address, int i)
{
  // ----- Display calibration message
  Serial.print("Calibrating gyro");Serial.print(i + 4); Serial.print("");                        //Print text to screen

  // ----- LED Status (ON = calibration start)
  pinMode(LED, OUTPUT);                                 //Set LED (pin 13) as output
  digitalWrite(LED, HIGH);                              //Turn LED on ... indicates startup

  // ----- Calibrate gyro
  for (int counter = 0; counter < 2000 ; counter ++)    //Run this code 2000 times
  {
    Loop_start = micros();
    if (counter % 125 == 0)Serial.print(".");              //Print a dot on the LCD every 125 readings
    read_mpu_6050_data(address, i);                               //Read the raw acc and gyro data from the MPU-6050
    Gyro_x_cal[i] += Gyro_x[i];                               //Add the gyro x-axis offset to the gyro_x_cal variable
    Gyro_y_cal[i] += Gyro_y[i];                               //Add the gyro y-axis offset to the gyro_y_cal variable
    Gyro_z_cal[i] += Gyro_z[i];                               //Add the gyro z-axis offset to the gyro_z_cal variable
    while (micros() - Loop_start < Loop_time);           // Wait until "Loop_time" microseconds have elapsed
  }
  Gyro_x_cal[i] /= 2000;                                   //Divide the gyro_x_cal variable by 2000 to get the average offset
  Gyro_y_cal[i] /= 2000;                                   //Divide the gyro_y_cal variable by 2000 to get the average offset
  Gyro_z_cal[i] /= 2000;                                   //Divide the gyro_z_cal variable by 2000 to get the average offset

  Serial.println();

  // ----- Status LED
  digitalWrite(LED, LOW);                               // Turn LED off ... calibration complete
}

// ----------------------------
// Config setup all sensors
// ----------------------------
void configurate_sensor(int address, int i)
{
  // ----- Configure the magnetometer
  configure_magnetometer(address);

  // ----- Calibrate the magnetometer
  /*
      Calibrate only needs to be done occasionally.
      Enter the magnetometer values into the "header"
      then set "Record_data = false".
  */
  if (Record_data == true)
  {
    calibrate_magnetometer();
  }

  // ----- Configure the gyro & magnetometer
  config_gyro(address);

  calibrate_gyro(address, i);

}

// -------------------------------
//  Read magnetometer
// -------------------------------
void read_magnetometer(int i)
{
  // ----- Locals
  int mag_x, mag_y, mag_z;
  int status_reg_2;

  // ----- Point to status register 1
  Wire.beginTransmission(AK8963_I2C_address);                   // Open session with AK8963
  Wire.write(AK8963_status_reg_1);                              // Point to ST1[0] status bit
  Wire.endTransmission();
  Wire.requestFrom(AK8963_I2C_address, 1);                      // Request 1 data byte
  while (Wire.available() < 1);                                 // Wait for the data
  if (Wire.read() & AK8963_data_ready_mask)                     // Check data ready bit
  {
    // ----- Read data from each axis (LSB,MSB)
    Wire.requestFrom(AK8963_I2C_address, 7);                    // Request 7 data bytes
    while (Wire.available() < 7);                               // Wait for the data
    mag_x = (Wire.read() | Wire.read() << 8) * ASAX;            // Combine LSB,MSB X-axis, apply ASA corrections
    mag_y = (Wire.read() | Wire.read() << 8) * ASAY;            // Combine LSB,MSB Y-axis, apply ASA corrections
    mag_z = (Wire.read() | Wire.read() << 8) * ASAZ;            // Combine LSB,MSB Z-axis, apply ASA corrections
    status_reg_2 = Wire.read();                                 // Read status and signal data read

    // ----- Validate data
    if (!(status_reg_2 & AK8963_overflow_mask))                 // Check HOFL flag in ST2[3]
    {
      Mag_x[i] = (mag_x - Mag_x_offset) * Mag_x_scale;
      Mag_y[i] = (mag_y - Mag_y_offset) * Mag_y_scale;
      Mag_z[i] = (mag_z - Mag_z_offset) * Mag_z_scale;
    }
  }
}

// --------------------------
//  Write LCD
// --------------------------
void write_LCD()
{
 /*
    Subroutine for updating the LCD display.
    In order to reduce the refresh time only one
    digit is updated each time around the main loop.

    IMPORTANT:
    The X and Y axes for the AK8963 magnetometer and MPU-6050 gyro are transposed:
     - The compass "pitch" is equal to the gyro "roll" and
     - The compass "roll" is equal to the "gyro "pitch".
 */

 // ----- Display compass heading
 LCD_loop_counter++;

 switch (LCD_loop_counter)
 {
   ////////////////////// Heading /////////////////////////
   case 1: // ----- Buffer heading reading
     LCD_heading_buffer = round(Heading[1]) - round(Heading[0]);                    // Buffer the (true north) heading because it will change
     Serial.print(" Heading"); /*Serial.print(i + 4);*/ Serial.print(" = ");                                     // Clear digits
     Serial.print(Heading[1]); Serial.print(" - "); Serial.print(Heading[0]);
     Serial.print(": ");
     Serial.print(LCD_heading_buffer);
     break;

   /////////////////////// Pitch ///////////////////////////
   case 2: // ----- Buffer the pitch angle
     LCD_pitch_buffer = (Gyro_pitch_output[1] - Gyro_pitch_output[0]) * 10;            // Buffer the gyro pitch angle because it will change
     Serial.print(" Gyro_pitch"); /*Serial.print(i + 4);*/ Serial.print(" = ");                                 // Set the LCD cursor to column 10, row 1
     Serial.print(Gyro_pitch_output[1]); Serial.print(" - ");  Serial.print(Gyro_pitch_output[0]);
     break;

   case 3: // ----- Display pitch sign
     (LCD_pitch_buffer < 0) ? Serial.print(":  -") : Serial.print(":  +");
     break;

   case 4: // ----- Display 1st pitch digit
     Serial.print(abs(LCD_pitch_buffer) / 1000);
     break;

   case 5: // ----- Display 2nd pitch digit
     Serial.print((abs(LCD_pitch_buffer) / 100) % 10);
     break;

   case 6: // ----- Display 3rd pitch digit
     Serial.print((abs(LCD_pitch_buffer) / 10) % 10);
     break;

   case 7: // ----- Display decimal point
     Serial.print(".");
     break;

   case 8: // ----- Display 4th pitch digit
     Serial.print(abs(LCD_pitch_buffer) % 10);
     break;

   ///////////////////// Roll //////////////////////////
   case 9: // ----- Buffer pitch reading
     LCD_roll_buffer = (Gyro_roll_output[1] - Gyro_pitch_output[0]) * 10;          // Buffer the gyro roll angle because it will change
     Serial.print(" Gyro_roll"); /*Serial.print(i + 4);*/ Serial.print(" = ");                              // Set the LCD cursor to column 1, row 0
     Serial.print(Gyro_roll_output[1]); Serial.print(" - ");  Serial.print(Gyro_roll_output[0]);
     break;

   case 10: // ----- Display roll sign
     (LCD_roll_buffer < 0) ? Serial.print(":  -") : Serial.print(":  +");
     break;

   case 11: // ----- Display 1st roll digit
     Serial.print(abs(LCD_roll_buffer) / 1000);
     break;

   case 12: // ----- Display 2nd roll digit
     Serial.print((abs(LCD_roll_buffer) / 100) % 10);
     break;

   case 13: // ----- Display 3rd roll digit
     Serial.print((abs(LCD_roll_buffer) / 10) % 10);
     break;

   case 14: // ----- Display decimal point
     Serial.print(".");
     break;

   case 15: // ----- Display 4th roll digit
     Serial.println(abs(LCD_roll_buffer) % 10);
     LCD_loop_counter = 0;
     break;
 }
}

// -----------------
//  Setup
// -----------------
void setup()
{
  // ----- Serial communication
  Serial.begin(115200);                                 //Use only for debugging
  Wire.begin();                                         //Start I2C as master
  Wire.setClock(400000);

  // ----- Provision to disable tilt stabilization
  /*
      Connect a jumper wire between A0 and GRN to disable the "tilt stabilazation"
  */
  pinMode(Switch, INPUT_PULLUP);                        // Short A0 to GND to disable tilt stabilization

  // ----- Status LED
  pinMode(LED, OUTPUT);                                 // Set LED (pin 13) as output
  pinMode(AD4, OUTPUT);                                 // Set AD4 (pin 11) as output
  pinMode(AD5, OUTPUT);                                 // Set AD5 (pin 12) as output
  digitalWrite(LED, LOW);                               // Turn LED off
  digitalWrite(AD4, LOW);                               // Set pin LOW to 0x68 address
  digitalWrite(AD5, HIGH);                              // Set pin HIGH to 0x69 address

  // ----- Start-up message
  Serial.print(" Working pragramm");                           // Print text to screen
  Serial.println(" V2.01");                              // Print text to screen
  delay(2000);                                          // Allow time to read

  for(int i = 0; i < sizeof(MPU9250_Add)/2; i++)
  {
    configurate_sensor(MPU9250_Add[i], i);
  }

  Debug_start_time = micros();                          // Controls data display rate to Serial Monitor
  Loop_start_time = micros();                           // Controls the Gyro refresh rate
}

// ----------------------------
// Main loop
// ----------------------------
void loop()
{
  ////////////////////////////////////////////
  //        PITCH & ROLL CALCULATIONS       //
  ////////////////////////////////////////////

  /*
      --------------------
      MPU-9250 Orientation
      --------------------
      Component side up
      X-axis facing forward
  */

  for(int i = 0; i < sizeof(MPU9250_Add)/2; i++)
  {
    // ----- read the raw accelerometer and gyro data
    read_mpu_6050_data(MPU9250_Add[i], i);                                             // Read the raw acc and gyro data from the MPU-6050

    // ----- Adjust for offsets
    Gyro_x[i] -= Gyro_x_cal[i];                                             // Subtract the offset from the raw gyro_x value
    Gyro_y[i] -= Gyro_y_cal[i];                                             // Subtract the offset from the raw gyro_y value
    Gyro_z[i] -= Gyro_z_cal[i];                                             // Subtract the offset from the raw gyro_z value

    // ----- Calculate travelled angles
    /*
      ---------------------------
      Adjust Gyro_xyz signs for:
      ---------------------------
      Pitch (Nose - up) = +ve reading
      Roll (Right - wing down) = +ve reading
      Yaw (Clock - wise rotation)  = +ve reading
    */
    Gyro_pitch[i] += -Gyro_y[i] * Sensor_to_deg;                            // Integrate the raw Gyro_y readings
    Gyro_roll[i] += Gyro_x[i] * Sensor_to_deg;                              // Integrate the raw Gyro_x readings
    Gyro_yaw[i] += -Gyro_z[i] * Sensor_to_deg;                              // Integrate the raw Gyro_x readings

    // ----- Compensate pitch and roll for gyro yaw
    Gyro_pitch[i] += Gyro_roll[i] * sin(Gyro_z[i] * Sensor_to_rad);            // Transfer the roll angle to the pitch angle if the Z-axis has yawed
    Gyro_roll[i] -= Gyro_pitch[i] * sin(Gyro_z[i] * Sensor_to_rad);            // Transfer the pitch angle to the roll angle if the Z-axis has yawed

    // ----- Accelerometer angle calculations
    Accel_total_vector[i] = sqrt((Accel_x[i] * Accel_x[i]) + (Accel_y[i] * Accel_y[i]) + (Accel_z[i] * Accel_z[i]));   // Calculate the total (3D) vector
    Accel_pitch[i] = asin((float)Accel_x[i] / Accel_total_vector[i]) * RAD_TO_DEG;                         //Calculate the pitch angle
    Accel_roll[i] = asin((float)Accel_y[i] / Accel_total_vector[i]) * RAD_TO_DEG;                         //Calculate the roll angle

    // ----- Zero any residual accelerometer readings
    /*
        Place the accelerometer on a level surface
        Adjust the following two values until the pitch and roll readings are zero
    */
    Accel_pitch[i] -= -0.2f;                                             //Accelerometer calibration value for pitch
    Accel_roll[i] -= -1.6f;                                               //Accelerometer calibration value for roll

    // ----- Correct for any gyro drift
    if (Gyro_synchronised)
    {
      // ----- Gyro & accel have been synchronised
      Gyro_pitch[i] = Gyro_pitch[i] * 0.9996 + Accel_pitch[i] * 0.0004;        //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
      Gyro_roll[i] = Gyro_roll[i] * 0.9996 + Accel_roll[i] * 0.0004;           //Correct the drift of the gyro roll angle with the accelerometer roll angle
    }
    else
    {
      // ----- Synchronise gyro & accel
      Gyro_pitch[i] = Accel_pitch[i];                                       //Set the gyro pitch angle equal to the accelerometer pitch angle
      Gyro_roll[i] = Accel_roll[i];                                         //Set the gyro roll angle equal to the accelerometer roll angle
      Gyro_synchronised = true;                                             //Set the IMU started flag
    }

    // ----- Dampen the pitch and roll angles
    Gyro_pitch_output[i] = Gyro_pitch_output[i] * 0.9 + Gyro_pitch[i] * 0.1;   //Take 90% of the output pitch value and add 10% of the raw pitch value
    Gyro_roll_output[i] = Gyro_roll_output[i] * 0.9 + Gyro_roll[i] * 0.1;      //Take 90% of the output roll value and add 10% of the raw roll value

    ////////////////////////////////////////////
    //        MAGNETOMETER CALCULATIONS       //
    ////////////////////////////////////////////
    /*
        --------------------------------
        Instructions for first time use
        --------------------------------
        Calibrate the compass for Hard-iron and Soft-iron
        distortion by temporarily setting the header to read
        bool    Record_data = true;

        Turn on your Serial Monitor before uploading the code.

        Slowly tumble the compass in all directions until a
        set of readings appears in the Serial Monitor.

        Copy these values into the appropriate header locations.

        Edit the header to read
        bool    Record_data = false;

        Upload the above code changes to your Arduino.

        This step only needs to be done occasionally as the
        values are reasonably stable.
    */

    // ----- Read the magnetometer
    read_magnetometer(i);

    // ----- Fix the pitch, roll, & signs
    /*
        MPU-9250 gyro and AK8963 magnetometer XY axes are orientated 90 degrees to each other
        which means that Mag_pitch equates to the Gyro_roll and Mag_roll equates to the Gryro_pitch

        The MPU-9520 and AK8963 Z axes point in opposite directions
        which means that the sign for Mag_pitch must be negative to compensate.
    */
    Mag_pitch[i] = -Gyro_roll_output[i] * DEG_TO_RAD;
    Mag_roll[i] = Gyro_pitch_output[i] * DEG_TO_RAD;

    // ----- Apply the standard tilt formulas
    Mag_x_hor[i] = Mag_x[i] * cos(Mag_pitch[i]) + Mag_y[i] * sin(Mag_roll[i]) * sin(Mag_pitch[i]) - Mag_z[i] * cos(Mag_roll[i]) * sin(Mag_pitch[i]);
    Mag_y_hor[i] = Mag_y[i] * cos(Mag_roll[i]) + Mag_z[i] * sin(Mag_roll[i]);

    // ----- Disable tilt stabization if switch closed
    if (!(digitalRead(Switch)))  //TO DO: This chabged.
    {
      // ---- Test equations
      Mag_x_hor[i] = Mag_x[i];
      Mag_y_hor[i] = Mag_y[i];
    }

    // ----- Dampen any data fluctuations
    Mag_x_dampened[i] = Mag_x_dampened[i] * 0.9 + Mag_x_hor[i] * 0.1;
    Mag_y_dampened[i] = Mag_y_dampened[i] * 0.9 + Mag_y_hor[i] * 0.1;

    // ----- Calculate the heading
    Heading[i] = atan2(Mag_x_dampened[i], Mag_y_dampened[i]) * RAD_TO_DEG;  // Magnetic North

    /*
        By convention, declination is positive when magnetic north
        is east of true north, and negative when it is to the west.
    */

    Heading[i] += Declination;               // Geographic North //TO DO: This changed.
    if (Heading[i] > 360.0) Heading[i] -= 360.0;
    if (Heading[i] < 0.0) Heading[i] += 360.0;

    // ----- Allow for under/overflow
    if (Heading[i] < 0) Heading[i] += 360;
    if (Heading[i] >= 360) Heading[i] -= 360;
  }

  // ----- Display Heading, Pitch, and Roll
  write_LCD();

  // ----- Loop control
  /*
      Adjust the loop count for a yaw reading of 360 degrees
      when the MPU-9250 is rotated exactly 360 degrees.
      (Compensates for any 16 MHz Xtal oscillator error)
  */
  while ((micros() - Loop_start_time) < 8000);
  Loop_start_time = micros();
}
