// ----- Libraries
#include <SPI.h>
//#include <calib_magn.ino>

// ----- LCD
int LCD_loop_counter;
int LCD_heading_buffer, LCD_pitch_buffer, LCD_roll_buffer;

// ----- MPU6050
#define RESET_REG 0x00
#define INT_20_MHz 0x00
#define GYRO_CONF 0x1B
#define ACCEL_CONF 0x1C
#define ACCEL_XOUT_H 0x3B                                               // address of register Accelerometer MSB
#define GYRO_XOUT_H 0x43                                                // address of register Gyroscope MSB
#define MPU9250_I2C_master_enable 0x6A                                  // USER_CTRL[5] = I2C_MST_EN
#define MPU9250_PWR_MGMT_1 0x6B                                         // PWR_MGMT_1 [7] = H_RESET
#define MPU9250_Interface_bypass_mux_enable 0x37                        // INT_PIN_CFG[1]= BYPASS_EN
#define I2C_MST_CLK           0x0D
#define I2C_MST_EN            0x20
#define I2C_MST_CTRL          0x24
#define I2C_SLV0_ADDR         0x25
#define I2C_SLV0_REG          0x26
#define I2C_SLV0_CTRL         0x27
#define EXT_SENS_DATA_00      0x49
#define I2C_SLV0_DO           0x63
#define I2C_SLV0_EN           0x80

#define Frequency 125                                                   // 8mS sample interval 
#define Sensitivity 65.5                                                // Gyro sensitivity (see data sheet)

#define Sensor_to_deg 1/(Sensitivity*Frequency)                         // Convert sensor reading to degrees
#define Sensor_to_rad Sensor_to_deg*DEG_TO_RAD                          // Convert sensor reading to radians

#define Loop_time 1000000/Frequency                                     // Loop time (uS)
long    Loop_start;                                                     // Loop start time (uS)

int     Gyro_x,     Gyro_y,     Gyro_z;
long    Gyro_x_cal, Gyro_y_cal, Gyro_z_cal;
float   Gyro_pitch, Gyro_roll, Gyro_yaw;
float   Gyro_pitch_output, Gyro_roll_output;

// ----- Accelerometer
long    Accel_x,      Accel_y,      Accel_z,    Accel_total_vector;
float   Accel_pitch,  Accel_roll;

// ----- Magnetometer
#define AK8963_I2C_address 0x0C                                             // I2C address for AK8963
#define AK8963_cntrl_reg_1 0x0A                                             // CNTL[4]=#bits, CNTL[3:0]=mode
#define AK8963_status_reg_1 0x02                                            // ST1[0]=data ready
#define AK8963_data_ready_mask 0x01                                         // Data ready mask
#define AK8963_overflow_mask 0x08                                           // Magnetic sensor overflow mask
#define AK8963_data 0x03                                                    // Start address of XYZ data                                                                
#define AK8963_fuse_ROM 0x10                                                // X,Y,Z fuse ROM
#define AK8963_PWR_DWN 0x00
#define AK8963_ACCESS_FUSE_ROM 0x0F
#define AK8963_16_BIT_MODE 0x10
#define AK8963_MODE_2 0x06

// ----- Compass heading
/*
 The magnetic declination for Lower Hutt, New Zealand is +22.5833 degrees
 Obtain your magnetic declination from http://www.magnetic-declination.com/
 Uncomment the declination code within the main loop() if you want True North.
*/
float   Declination = -12.14;                                             //  Degrees ... replace this declination with yours

int     Heading;

int     Mag_x,                Mag_y,                Mag_z;                  // Raw magnetometer readings
float   Mag_x_dampened,       Mag_y_dampened,       Mag_z_dampened;
float   Mag_x_hor, Mag_y_hor;
float   Mag_pitch, Mag_roll;

// ----- Record compass offsets, scale factors, & ASA values
/*
  These values seldom change ... an occasional check is sufficient
  (1) Open your Arduino "Serial Monitor
  (2) Set "Record_data=true;" then upload & run program.
  (3) Replace the values below with the values that appear on the Serial Monitor.
  (4) Set "Record_data = false;" then upload & rerun program.
*/
bool    Record_data = false;

int     Mag_x_offset = 59,      Mag_y_offset = 302,     Mag_z_offset = -200;   // Hard-iron offsets for MPU-4
float   Mag_x_scale = 1.04,     Mag_y_scale = 1.01,     Mag_z_scale = 0.96;    // Soft-iron scale factors for MPU-4
float   ASAX = 1.18,            ASAY = 1.18,            ASAZ = 1.13;           // (A)sahi (S)ensitivity (A)djustment fuse ROM values for MPU-4.

// ----- Pins
int AD4 = 10;                           // AD0 pin for MPU address 0x68

int MPU9250_Add[] = {AD4};               // Massive for change address of MPU in programm.

// ----- Flags
bool Gyro_synchronised = false;
bool Flag = false;

// ----- Debug
#define Switch A0                       // Connect an SPST switch between A0 and GND to enable/disable tilt stabilazation
long Loop_start_time;
long Debug_start_time;

// ----- SPI
bool _useSPIHS;
const uint8_t SPI_READ = 0x80;          // BIN(1000 0000) flag for read data from sensor
const uint32_t SPI_LS_CLOCK = 1000000;  // 1MHz speed data transmit
const uint32_t SPI_HS_CLOCK = 15000000; // 15MHz
int buffer[7];     // Buffer for read data from sensor

// --------------------
// SPI communication
// --------------------
void SPI_write(int sensor, int address, int data)
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
  digitalWrite(sensor, LOW);
  SPI.transfer(address);
  SPI.transfer(data);
  digitalWrite(sensor, HIGH);
  SPI.endTransaction();
}

void SPI_read(int sensor, int address, int count, int* data)
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
  digitalWrite(sensor, LOW);
  SPI.transfer(address | SPI_READ);
  
  for(int i = 0; i < count; i++)
  {
    data[i] = SPI.transfer(0x00);
  }
  
  digitalWrite(sensor, HIGH);
  SPI.endTransaction();
}

// -------------------------------
//  Read magnetometer
// -------------------------------

void read_magnetometer_registers(int sensor, int subAddress, int count, int* dest)
{
  SPI_write(sensor, I2C_SLV0_ADDR, AK8963_I2C_address | SPI_READ);
  SPI_write(sensor, I2C_SLV0_REG, subAddress);
  SPI_write(sensor, I2C_SLV0_CTRL, I2C_SLV0_EN | count);
  delay(1);
  SPI_read(sensor, EXT_SENS_DATA_00, count, dest);
}

void write_magnetometer_register(int sensor, int subAddress, int data)
{
  SPI_write(sensor, I2C_SLV0_ADDR, AK8963_I2C_address);
  SPI_write(sensor, I2C_SLV0_REG, subAddress);
  SPI_write(sensor, I2C_SLV0_DO, data);
  SPI_write(sensor, I2C_SLV0_CTRL, I2C_SLV0_EN | 1);
}

void read_magnetometer(int sensor)
{
 // ----- Locals
 int mag_x, mag_y, mag_z;
 int status_reg_2;
 int count = 7; // Number of registers to read from magnetometer
 int dest[count]; // Massive to storage magnetometer data

  // Then connection to sensor is SPI, Magnetometer read thru Slave registers
 read_magnetometer_registers(sensor, AK8963_data, count, dest);
 
 mag_x = (dest[0] | dest[1] << 8) * ASAX;
 mag_y = (dest[2] | dest[3] << 8) * ASAY;
 mag_z = (dest[4] | dest[5] << 8) * ASAZ;
 status_reg_2 = dest[6];

 if (!(status_reg_2 & AK8963_overflow_mask))                 // Check HOFL flag in ST2[3]
 {
   Mag_x = (mag_x - Mag_x_offset) * Mag_x_scale;
   Mag_y = (mag_y - Mag_y_offset) * Mag_y_scale;
   Mag_z = (mag_z - Mag_z_offset) * Mag_z_scale;
 }
}

// ----------------------------
//  Configure magnetometer
// ----------------------------
void configure_magnetometer(int sensor)
{
 /*
    The MPU-9250 contains an AK8963 magnetometer and an
    MPU-6050 gyro/accelerometer within the same package.

    To access the AK8963 magnetometer chip The MPU-9250 I2C bus
    must be changed to pass-though mode. To do this we must:
     - disable the MPU-9250 slave I2C and
     - enable the MPU-9250 interface bypass mux
 */
 // ----- Enable MPU9250 I2C master interface
 SPI_write(sensor, MPU9250_I2C_master_enable, I2C_MST_EN);  // Enable I2C master interface

 // ----- Access AK8963 fuse ROM
 /* The factory sensitivity readings for the XYZ axes are stored in a fuse ROM.
    To access this data we must change the AK9863 operating mode.
 */
  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_16_BIT_MODE | AK8963_ACCESS_FUSE_ROM); // CNTL[3:0] mode bits. Output data=16-bits; Access fuse ROM
  delay(100);                                                       // Wait for mode change

 // ----- Get factory XYZ sensitivity adjustment values from fuse ROM
 /* There is a formula on page 53 of "MPU-9250, Register Map and Decriptions, Revision 1.4":
     Hadj = H*(((ASA-128)*0.5)/128)+1 where
     H    = measurement data output from data register
     ASA  = sensitivity adjustment value (from fuse ROM)
     Hadj = adjusted measurement data (after applying
 */
  int ASA[3]; // Store ASA from Mag
  read_magnetometer_registers(sensor, AK8963_fuse_ROM, 3, ASA); // Point to AK8963 fuse ROM. Request 3 bytes of data

 ASAX = (ASA[0] - 128) * 0.5 / 128 + 1;                       // Adjust data
 ASAY = (ASA[1] - 128) * 0.5 / 128 + 1;
 ASAZ = (ASA[2] - 128) * 0.5 / 128 + 1;

 // ----- Power down AK8963 while the mode is changed
 /*
    This wasn't necessary for the first mode change as the chip was already powered down
 */
  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_PWR_DWN);  // Set mode to power down
  delay(100);                                                     // Wait for mode change

 // ----- Set output to mode 2 (16-bit, 100Hz continuous)
  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_16_BIT_MODE | AK8963_MODE_2);  // Output=16-bits; Measurements = 100Hz continuous
  delay(100);                                                       // Wait for mode change
}

// -----------------------------------
//  Configure the gyro & accelerometer
// -----------------------------------
void config_gyro(int sensor)
{
 // ----- Activate the MPU-6050
 SPI_write(sensor, MPU9250_PWR_MGMT_1, INT_20_MHz); //Use internal 20MHz clock

 // ----- Configure the accelerometer (+/-8g)
 SPI_write(sensor, ACCEL_CONF, 0x10); //Select +/-8g full-scale

 // ----- Configure the gyro (500dps full scale)
 SPI_write(sensor, GYRO_CONF, 0x08);  //Select 500dps full-scale
}

// --------------------
//  Read MPU 6050 data
// --------------------
void read_mpu_6050_data(int sensor)
{
 /*
   Subroutine for reading the raw gyro and accelerometer data
 */

 // ----- Locals
 int dest[14];  // Array to storage data from sensor.
 int temperature;                                  // Needed when reading the MPU-6050 data ... not used

 // ----- Read the data
 SPI_read(sensor, ACCEL_XOUT_H, 14, dest);
 
 Accel_x = dest[0] << 8 | dest[1];             // Combine MSB,LSB Accel_x variable
 Accel_y = dest[2] << 8 | dest[3];             // Combine MSB,LSB Accel_y variable
 Accel_z = dest[4] << 8 | dest[5];             // Combine MSB,LSB Accel_z variable
 temperature = dest[6] << 8 | dest[7];         // Combine MSB,LSB temperature variable
 Gyro_x = dest[8] << 8 | dest[9];              // Combine MSB,LSB Gyro_x variable
 Gyro_y = dest[10] << 8 | dest[11];              // Combine MSB,LSB Gyro_x variable
 Gyro_z = dest[12] << 8 | dest[13];              // Combine MSB,LSB Gyro_x variable
}

// -----------------------------------
//  Calibrate gyro
// -----------------------------------
void calibrate_gyro(int sensor)
{
 // ----- Display calibration message
 Serial.print("Calibrating gyro");                        //Print text to screen

 // ----- Calibrate gyro
 for (int counter = 0; counter < 2000 ; counter ++)    //Run this code 2000 times
 {
   Loop_start = micros();
   if (counter % 125 == 0)Serial.print(".");              //Print a dot on the LCD every 125 readings
   read_mpu_6050_data(sensor);                               //Read the raw acc and gyro data from the MPU-6050
   Gyro_x_cal += Gyro_x;                               //Add the gyro x-axis offset to the gyro_x_cal variable
   Gyro_y_cal += Gyro_y;                               //Add the gyro y-axis offset to the gyro_y_cal variable
   Gyro_z_cal += Gyro_z;                               //Add the gyro z-axis offset to the gyro_z_cal variable
   while (micros() - Loop_start < Loop_time);           // Wait until "Loop_time" microseconds have elapsed
 }
 Gyro_x_cal /= 2000;                                   //Divide the gyro_x_cal variable by 2000 to get the average offset
 Gyro_y_cal /= 2000;                                   //Divide the gyro_y_cal variable by 2000 to get the average offset
 Gyro_z_cal /= 2000;                                   //Divide the gyro_z_cal variable by 2000 to get the average offset

 Serial.println();

}

// --------------------------
//  Write LCD
// --------------------------
void write_LCD()
{
 /*
    Subroutine for updating the LCD display.
    In order to reduce the refresh time only one
    digit is updated each time around the main loop.

    IMPORTANT:
    The X and Y axes for the AK8963 magnetometer and MPU-6050 gyro are transposed:
     - The compass "pitch" is equal to the gyro "roll" and
     - The compass "roll" is equal to the "gyro "pitch".
 */
 // ----- Locals
 int digit1, digit2;

 // ----- Display compass heading
 LCD_loop_counter++;

 switch (LCD_loop_counter)
 {
   ////////////////////// Heading /////////////////////////
   case 1: // ----- Buffer heading reading
     LCD_heading_buffer = round(Heading);                    // Buffer the (true north) heading because it will change
     Serial.print("Heading = ");                                     // Clear digits
     Serial.print(LCD_heading_buffer);
     break;

   /////////////////////// Pitch ///////////////////////////
   case 2: // ----- Buffer the pitch angle
     Serial.print("  Gyro_pitch = ");                                 // Set the LCD cursor to column 10, row 1
     LCD_pitch_buffer = Gyro_pitch_output * 10;            // Buffer the gyro pitch angle because it will change
     Serial.print(Gyro_pitch_output);
     break;

   case 3: // ----- Display pitch sign
     (LCD_pitch_buffer < 0) ? Serial.print("  -") : Serial.print("  +");
     break;

   case 4: // ----- Display 1st pitch digit
     Serial.print(abs(LCD_pitch_buffer) / 1000);
     break;

   case 5: // ----- Display 2nd pitch digit
     Serial.print((abs(LCD_pitch_buffer) / 100) % 10);
     break;

   case 6: // ----- Display 3rd pitch digit
     Serial.print((abs(LCD_pitch_buffer) / 10) % 10);
     break;

   case 7: // ----- Display decimal point
     Serial.print(".");
     break;

   case 8: // ----- Display 4th pitch digit
     Serial.print(abs(LCD_pitch_buffer) % 10);
     break;

   ///////////////////// Roll //////////////////////////
   case 9: // ----- Buffer pitch reading
     Serial.print("  Gyro_roll = ");                              // Set the LCD cursor to column 1, row 0
     LCD_roll_buffer = Gyro_roll_output * 10;          // Buffer the gyro roll angle because it will change
     Serial.print(Gyro_roll_output);
     break;

   case 10: // ----- Display roll sign
     (LCD_roll_buffer < 0) ? Serial.print("  -") : Serial.print("  +");
     break;

   case 11: // ----- Display 1st roll digit
     Serial.print(abs(LCD_roll_buffer) / 1000);
     break;

   case 12: // ----- Display 2nd roll digit
     Serial.print((abs(LCD_roll_buffer) / 100) % 10);
     break;

   case 13: // ----- Display 3rd roll digit
     Serial.print((abs(LCD_roll_buffer) / 10) % 10);
     break;

   case 14: // ----- Display decimal point
     Serial.print(".");
     break;

   case 15: // ----- Display 4th roll digit
     Serial.println(abs(LCD_roll_buffer) % 10);
     LCD_loop_counter = 0;
     break;
 }
}

// -----------------
//  Setup
// -----------------
void setup()
{
 // ----- Serial communication
 Serial.begin(115200);                                 //Use only for debugging
 SPI.begin();                                         //Start I2C as master

 // ----- Provision to disable tilt stabilization
 /*
    Connect a jumper wire between A0 and GRN to disable the "tilt stabilazation"
 */
 pinMode(Switch, INPUT_PULLUP);                        // Short A0 to GND to disable tilt stabilization

 // ----- Status
 pinMode(AD4, OUTPUT);                                 // Set AD4 (pin 11) as output
 digitalWrite(AD4, LOW);                               // Set pin LOW to 0x68 address

 SPI_write(MPU9250_Add, MPU9250_PWR_MGMT_1, RESET_REG);  // Setup MPU9250 to default
 
 // ----- Start-up message
 Serial.print(" Working pragramm");                           // Print text to screen
 Serial.println(" V2.10 SPI");                              // Print text to screen
 delay(2000);                                          // Allow time to read

 // ----- Configure the magnetometer
 configure_magnetometer(MPU9250_Add);

//  // ----- Calibrate the magnetometer
//  /*
//     Calibrate only needs to be done occasionally.
//     Enter the magnetometer values into the "header"
//     then set "Record_data = false".
//  */
//  if (Record_data == true)
//  {
//     calibrate_magnetometer();
//  }

 // ----- Configure the gyro & magnetometer
 config_gyro(MPU9250_Add);

 calibrate_gyro(MPU9250_Add);

 Debug_start_time = micros();                          // Controls data display rate to Serial Monitor
 Loop_start_time = micros();                           // Controls the Gyro refresh rate
}

// ----------------------------
// Main loop
// ----------------------------
void loop()
{
 ////////////////////////////////////////////
 //        PITCH & ROLL CALCULATIONS       //
 ////////////////////////////////////////////

 /*
    --------------------
    MPU-9250 Orientation
    --------------------
    Component side up
    X-axis facing forward
 */

 // ----- read the raw accelerometer and gyro data
 read_mpu_6050_data(MPU9250_Add);                                             // Read the raw acc and gyro data from the MPU-6050

 // ----- Adjust for offsets
 Gyro_x -= Gyro_x_cal;                                             // Subtract the offset from the raw gyro_x value
 Gyro_y -= Gyro_y_cal;                                             // Subtract the offset from the raw gyro_y value
 Gyro_z -= Gyro_z_cal;                                             // Subtract the offset from the raw gyro_z value

 // ----- Calculate travelled angles
 /*
   ---------------------------
   Adjust Gyro_xyz signs for:
   ---------------------------
   Pitch (Nose - up) = +ve reading
   Roll (Right - wing down) = +ve reading
   Yaw (Clock - wise rotation)  = +ve reading
 */
 Gyro_pitch += -Gyro_y * Sensor_to_deg;                            // Integrate the raw Gyro_y readings
 Gyro_roll += Gyro_x * Sensor_to_deg;                              // Integrate the raw Gyro_x readings
 Gyro_yaw += -Gyro_z * Sensor_to_deg;                              // Integrate the raw Gyro_x readings

 // ----- Compensate pitch and roll for gyro yaw
 Gyro_pitch += Gyro_roll * sin(Gyro_z * Sensor_to_rad);            // Transfer the roll angle to the pitch angle if the Z-axis has yawed
 Gyro_roll -= Gyro_pitch * sin(Gyro_z * Sensor_to_rad);            // Transfer the pitch angle to the roll angle if the Z-axis has yawed

 // ----- Accelerometer angle calculations
 Accel_total_vector = sqrt((Accel_x * Accel_x) + (Accel_y * Accel_y) + (Accel_z * Accel_z));   // Calculate the total (3D) vector
 Accel_pitch = asin((float)Accel_x / Accel_total_vector) * RAD_TO_DEG;                         //Calculate the pitch angle
 Accel_roll = asin((float)Accel_y / Accel_total_vector) * RAD_TO_DEG;                         //Calculate the roll angle

 // ----- Zero any residual accelerometer readings
 /*
    Place the accelerometer on a level surface
    Adjust the following two values until the pitch and roll readings are zero
 */
 Accel_pitch -= -0.2f;                                             //Accelerometer calibration value for pitch
 Accel_roll -= -1.6f;                                               //Accelerometer calibration value for roll

 // ----- Correct for any gyro drift
 if (Gyro_synchronised)
 {
   // ----- Gyro & accel have been synchronised
   Gyro_pitch = Gyro_pitch * 0.9996 + Accel_pitch * 0.0004;        //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
   Gyro_roll = Gyro_roll * 0.9996 + Accel_roll * 0.0004;           //Correct the drift of the gyro roll angle with the accelerometer roll angle
 }
 else
 {
   // ----- Synchronise gyro & accel
   Gyro_pitch = Accel_pitch;                                       //Set the gyro pitch angle equal to the accelerometer pitch angle
   Gyro_roll = Accel_roll;                                         //Set the gyro roll angle equal to the accelerometer roll angle
   Gyro_synchronised = true;                                             //Set the IMU started flag
 }

 // ----- Dampen the pitch and roll angles
 Gyro_pitch_output = Gyro_pitch_output * 0.9 + Gyro_pitch * 0.1;   //Take 90% of the output pitch value and add 10% of the raw pitch value
 Gyro_roll_output = Gyro_roll_output * 0.9 + Gyro_roll * 0.1;      //Take 90% of the output roll value and add 10% of the raw roll value

 ////////////////////////////////////////////
 //        MAGNETOMETER CALCULATIONS       //
 ////////////////////////////////////////////
 /*
    --------------------------------
    Instructions for first time use
    --------------------------------
    Calibrate the compass for Hard-iron and Soft-iron
    distortion by temporarily setting the header to read
    bool    Record_data = true;

    Turn on your Serial Monitor before uploading the code.

    Slowly tumble the compass in all directions until a
    set of readings appears in the Serial Monitor.

    Copy these values into the appropriate header locations.

    Edit the header to read
    bool    Record_data = false;

    Upload the above code changes to your Arduino.

    This step only needs to be done occasionally as the
    values are reasonably stable.
 */

 // ----- Read the magnetometer
 read_magnetometer(MPU9250_Add);

 // ----- Fix the pitch, roll, & signs
 /*
    MPU-9250 gyro and AK8963 magnetometer XY axes are orientated 90 degrees to each other
    which means that Mag_pitch equates to the Gyro_roll and Mag_roll equates to the Gryro_pitch

    The MPU-9520 and AK8963 Z axes point in opposite directions
    which means that the sign for Mag_pitch must be negative to compensate.
 */
 Mag_pitch = -Gyro_roll_output * DEG_TO_RAD;
 Mag_roll = Gyro_pitch_output * DEG_TO_RAD;

 // ----- Apply the standard tilt formulas
 Mag_x_hor = Mag_x * cos(Mag_pitch) + Mag_y * sin(Mag_roll) * sin(Mag_pitch) - Mag_z * cos(Mag_roll) * sin(Mag_pitch);
 Mag_y_hor = Mag_y * cos(Mag_roll) + Mag_z * sin(Mag_roll);

 // ----- Disable tilt stabization if switch closed
 if (!(digitalRead(Switch)))  //TO DO: This changed.
 {
   // ---- Test equations
   Mag_x_hor = Mag_x;
   Mag_y_hor = Mag_y;
 }

 // ----- Dampen any data fluctuations
 Mag_x_dampened = Mag_x_dampened * 0.9 + Mag_x_hor * 0.1;
 Mag_y_dampened = Mag_y_dampened * 0.9 + Mag_y_hor * 0.1;

 // ----- Calculate the heading
 Heading = atan2(Mag_x_dampened, Mag_y_dampened) * RAD_TO_DEG;  // Magnetic North

 /*
    By convention, declination is positive when magnetic north
    is east of true north, and negative when it is to the west.
 */

 Heading += Declination;               // Geographic North //TO DO: This changed.
 if (Heading > 360.0) Heading -= 360.0;
 if (Heading < 0.0) Heading += 360.0;

 // ----- Allow for under/overflow
 if (Heading < 0) Heading += 360;
 if (Heading >= 360) Heading -= 360;

 // ----- Display Heading, Pitch, and Roll
 write_LCD();

 // ----- Loop control
 /*
    Adjust the loop count for a yaw reading of 360 degrees
    when the MPU-9250 is rotated exactly 360 degrees.
    (Compensates for any 16 MHz Xtal oscillator error)
 */
 while ((micros() - Loop_start_time) < 8000);
 Loop_start_time = micros();
}
