#include <Arduino.h>
#include <SPI.h>
// #include <Servo.h>
//#include <Wire.h>

//AK8963 Registers
#define AK8963_ADDRESS  0x0C
#define AK8963_PRW_DWN  0x00
#define WHO_AM_I_AK8963 0x00    //should return 0x48
#define INFO            0x01
#define AK8963_ST1      0x02    //data ready statu bit 0
#define AK8963_XOUT_L   0x03    //data
#define AK8963_XOUT_H   0x04
#define AK8963_YOUT_L   0x05
#define AK8963_YOUT_H   0x06
#define AK8963_ZOUT_L   0x07
#define AK8963_ZOUT_H   0x08
#define AK8963_ST2      0x09    //data overflow bit and data read error status bit2
#define AK8963_CNTL     0x0A    //Power down(0000) single-measurement(0001) self-test(1000) fuse ROM(1111) modes on bits 3:0
#define AK8963_ASTC     0x0C    //self-test control
#define AK8963_ASAX     0x10    //fuseROM x-axis sensitivity adjustment value
#define AK8963_ASAY     0x11    //fuseROM y-axis sensitivity adjustment value
#define AK8963_ASAZ     0x12    //fuseROM z-axis sensitivity adjustment value

//MPU9250 Registers
#define SELF_TEST_X_GYRO    0x00
#define SELF_TEST_Y_GYRO    0x01
#define SELF_TEST_Z_GYRO    0x02
/*
#define X_FINE_GAIN         0x03    //[7:0] fine gain
#define Y_FINE_GAIN         0x04
#define Z_FINE_GAIN         0x05
#define XA_OFFSET_H         0x06    //user-defined trim values for accelerometer
#define XA_OFFSET_L_TC      0x07
#define YA_OFFSET_H         0x08
#define YA_OFFSET_L_TC      0x09
#define ZA_OFFSET_H         0x0A
#define ZA_OFFSET_L_TC      0x0B    */

#define SELF_TEST_X_ACCEL   0x0D
#define SELF_TEST_Y_ACCEL   0x0E
#define SELF_TEST_Z_ACCEL   0x0F

#define SELF_TEST_A         0x10

#define XG_OFFSET_H         0x13    //user-defined trim value for gyroscope
#define XG_OFFSET_L         0x14
#define YG_OFFSET_H         0x15
#define YG_OFFSET_L         0x16
#define ZG_OFFSET_H         0x17
#define ZG_OFFSET_L         0x18
#define SMPLRT_DIV          0x19
#define CONFIG              0x1A
#define GYRO_CONFIG         0x1B
#define ACCEL_CONFIG        0x1C
#define ACCEL_CONFIG_2      0x1D
#define LP_ACCEL_ODR        0x1E
#define WOM_THR             0x1F

#define MOT_DUR             0x20    //duration counter threshold for motion interrupt generation, 1kHz rate, LSB = 1ms
#define ZMOT_THR            0x21    //zero-motion detection threshold bits [7:0]
#define ZRMOT_DUR           0x22    //duration counter threshold foro zero-motion interrupt generation, 16Hz rate, LSB = 64ms

#define FIFO_EN             0x23
#define I2C_MST_CTRL        0x24
#define I2C_SLV0_ADDR       0x25
#define I2C_SLV0_REG        0x26
#define I2C_SLV0_CTRL       0x27
#define I2C_SLV1_ADDR       0x28
#define I2C_SLV1_REG        0x29
#define I2C_SLV1_CTRL       0x2A
#define I2C_SLV2_ADDR       0x2B
#define I2C_SLV2_REG        0x2C
#define I2C_SLV2_CTRL       0x2D
#define I2C_SLV3_ADDR       0x2E
#define I2C_SLV3_REG        0x2F
#define I2C_SLV3_CTRL       0X30
#define I2C_SLV4_ADDR       0x31
#define I2C_SLV4_REG        0x32
#define I2C_SLV4_DO         0x33
#define I2C_SLV4_CTRL       0x34
#define I2C_SLV4_DI         0x35
#define I2C_MST_STATUS      0x36   
#define INT_PIN_CFG         0x37
#define INT_ENABLE          0x38
#define DMP_INT_STATUS      0x39    //check DMP interrupt
#define INT_STATUS          0x3A
#define ACCEL_XOUT_H        0x3B
#define ACCEL_XOUT_L        0x3C
#define ACCEL_YOUT_H        0x3D
#define ACCEL_YOUT_L        0x3E
#define ACCEL_ZOUT_H        0x3F
#define ACCEL_ZOUT_L        0x40
#define TEMP_OUT_H          0x41
#define TEMP_OUT_L          0x42
#define GYRO_XOUT_H         0x43
#define GYRO_XOUT_L         0x44
#define GYRO_YOUT_H         0x45
#define GYRO_YOUT_L         0x46
#define GYRO_ZOUT_H         0x47
#define GYRO_ZOUT_L         0x48
#define EXT_SENS_DATA_00    0x49
#define EXT_SENS_DATA_01    0x4A
#define EXT_SENS_DATA_02    0x4B
#define EXT_SENS_DATA_03    0x4C
#define EXT_SENS_DATA_04    0x4D
#define EXT_SENS_DATA_05    0x4E
#define EXT_SENS_DATA_06    0x4F
#define EXT_SENS_DATA_07    0x50
#define EXT_SENS_DATA_08    0x51
#define EXT_SENS_DATA_09    0x52
#define EXT_SENS_DATA_10    0x53
#define EXT_SENS_DATA_11    0x54
#define EXT_SENS_DATA_12    0x55
#define EXT_SENS_DATA_13    0x56
#define EXT_SENS_DATA_14    0x57
#define EXT_SENS_DATA_15    0x58
#define EXT_SENS_DATA_16    0x59
#define EXT_SENS_DATA_17    0x5A
#define EXT_SENS_DATA_18    0x5B
#define EXT_SENS_DATA_19    0x5C
#define EXT_SENS_DATA_20    0x5D
#define EXT_SENS_DATA_21    0x5E
#define EXT_SENS_DATA_22    0x5F
#define EXT_SENS_DATA_23    0x60
#define MOT_DETECT_STATUS   0x61
#define I2C_SLV0_DO         0x63
#define I2C_SLV1_DO         0x64
#define I2C_SLV2_DO         0x65
#define I2C_SLV3_DO         0x66
#define I2C_MST_DELAY_CTRL  0x67
#define SIGNAL_PATH_RESET   0x68
#define MOT_DETECT_CTRL     0x69
#define USER_CTRL           0x6A
#define PWR_MGMT_1          0x6B
#define PWR_MGMT_2          0x6C
#define DMP_BANK            0x6D
#define DMP_RW_PNT          0x6E
#define DMP_REG             0x6F
#define DMP_REG_1           0x70
#define DMP_REG_2           0x71
#define FIFO_COUNT_H        0x72
#define FIFO_COUNT_L        0x73
#define FIFO_R_W            0x74
#define WHO_AM_I_MPU9250    0x75
#define XA_OFFSET_H         0x77
#define XA_OFFSET_L         0x78
#define YA_OFFSET_H         0x7A
#define YA_OFFSET_L         0x7B
#define ZA_OFFSET_H         0x7D
#define ZA_OFFSET_L         0x7E
#define PWR_RESET           0x80

//#define MPU9250_ADDRESS     0x69
#define MPU925X_ADDRESS     0x68

#define AHRS true
#define SerialDebug true

Servo servo_Pitch;
Servo servo_Yaw;
Servo servo_Roll;

// Set initial input parameters
enum Ascale
{
    AFS_2G = 0,
    AFS_4G,
    AFS_8G,
    AFS_16G
};

enum Gscale
{
    GFS_250DPS = 0,
    GFS_500DPS,
    GFS_1000DPS,
    GFS_2000DPS
};

enum Mscale
{
    MFS_14BITS = 0,
    MFS_16BITS
};

//Specify sensor full scale
uint8_t Gscale = GFS_250DPS;
uint8_t Ascale = AFS_2G;
uint8_t Mscale = MFS_16BITS;
uint8_t Mmode = 0x02;
float aRes, gRes, mRes; //scale resolutions per LSB for the sensors

//Pin definitions
int AD0_MPU9250 = 12;    //these pin set up address for MPU9250 (0x68/0x69)
//int AD0_MPU9255 = 3;  //these pin set up address for MPU9255 (0x68/0x69)
int intPin = 2;    //these can be changed, 2 and 3 are the Arduinos ext int pins
int sclPin = 13;
int sdaPin = 11;
int csPin = 10;
int setPin = LOW;   //set address MPU

//Set up massive for sensors
int16_t accelCount[3];  //stores the 16-bit signed accelerometer output
int16_t gyroCount[3];   //stores the 16-bit signed gyro sensor output
int16_t magCount[3];    //stores the 16-bit signed magnetometer sensor output
float magCalibration[3] = {0, 0, 0}, magBias[3] = {0, 0, 0};    //Factory mag calibration and mag bias
float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0};    //Bias corrections for gyro and accelerometer
int16_t tempCount;  //temperature raw count output
float temperature;  //stores the real internal chip temperature in degrees Celsius
float SelfTest[6];  //holds result of gyro and accelerometer self test

//global constants for 9 DoF fusion and AHRS (Attitude and Heading Reference System)
float GyroMeasError = PI * (40.0f / 180.0f);
float GyroMeasDrift = PI * (0.0f / 180.0f);
float beta = sqrt(3.0f / 4.0f) * GyroMeasError;
float zeta = sqrt(3.0f / 4.0f) * GyroMeasDrift;
#define Kp 2.0f * 5.0f  //these are the free parameters in the Mahony filter and fusion
#define Ki 0.0f

//set up variables for sensors
uint32_t delt_t_MPU9250 = 0;    //used to control display output rate
uint32_t count_MPU9250 = 0, sumCount_MPU9250 = 0;   //used to control display output rate
float pitch_MPU9250, yaw_MPU9250, roll_MPU9250;
float delta_t_MPU9250 = 0.0f, sum_MPU9250 = 0.0f;   //integration interval for both filter schemes
uint32_t lastUpdate_MPU9250 = 0, firstUpdate_MPU9250 = 0;   //used to calculate integration interval
uint32_t Now_MPU9250 = 0;   //used to calculate integration interval

float ax_MPU9250, ay_MPU9250, az_MPU9250;   //variables to hold latest sensor data values
float gx_MPU9250, gy_MPU9250, gz_MPU9250;
float mx_MPU9250, my_MPU9250, mz_MPU9250;
float q_MPU9250[4] = {1.0f, 0.0f, 0.0f, 0.0f};  // vector to hold quaternion
float eInt_MPU9250[3] = {0.0f, 0.0f, 0.0f}; //vector to hold integral error for Mahony method

//SPI
bool _useSPIHS;
const uint8_t SPI_READ = 0x80;
const uint32_t SPI_LS_CLOCK = 1000000;  //1MHz
const uint32_t SPI_HS_CLOCK = 15000000; //15MHz

//wire.h read and write protocol
void writeByte(uint8_t subAddress, uint8_t data)
{
    SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
    digitalWrite(csPin, LOW);
    SPI.transfer(subAddress);
    SPI.transfer(data);
    digitalWrite(csPin, HIGH);
    SPI.endTransaction();
/* I2C connection
    Wire.beginTransmission(address);    //initialize the tx buffer
    Wire.write(subAddress); //put slave register address in tx buffer
    Wire.write(data);   //put data in tx buffer
    Wire.endTransmission(); //send the tx buffer
*/
}

uint8_t readByte(uint8_t subAddress)
{
    uint8_t data;   //data will store the register data.

    // begin the transaction
    SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
    digitalWrite(csPin,LOW); // select the MPU9250 chip
    SPI.transfer(subAddress | SPI_READ); // specify the starting register address
    data = SPI.transfer(0x00); // read the data
    digitalWrite(csPin,HIGH); // deselect the MPU9250 chip
    SPI.endTransaction(); // end the transaction

    /* I2C connection
    Wire.beginTransmission(address);    //initialize the tx buffer
    Wire.write(subAddress); //put slave register address in tx buffer
    Wire.endTransmission(false);    //send the tx buffer, but send a restart to keep connection alive
    Wire.requestFrom(address, (uint8_t)1);  //read one byte from slave register address
    data = Wire.read(); //fill rx buffer with result
    */
    
    return data;    //return data read from slave register
}

void readBytes(uint8_t subAddress, uint8_t count, uint8_t* dest)
{
    SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
    digitalWrite(csPin, LOW);
    SPI.transfer(subAddress | SPI_READ);

    for(uint8_t i = 0; i < count; i++)
    {
        dest[i] = SPI.transfer(0x00);
    }

    digitalWrite(csPin, HIGH);
    SPI.endTransaction();
    /* I2C connection
    Wire.beginTransmission(address);    //initialize the tx buffer
    Wire.write(subAddress); //put slave register address in tx buffer
    Wire.endTransmission(false);    //send the tx buffer, but send a restart to keep connection alive
    
    uint8_t i = 0;
    Wire.requestFrom(address, count);   //read bytes from slave regiter address

    while (Wire.available())
    {
        dest[i++] = Wire.read();    //put read results in the rx buffer
    }
    */
}

//accelerometer and gyroscope self-test; check calibration wrt factory settings
void MPU9250SelfTest(float * destination)   //should return percent deviation from factory trim values, +/- 14 or less deviation is a pass
{
    uint8_t rawData[6] = {0, 0, 0, 0, 0, 0};
    uint8_t selfTest_MPU9250[6];
    int16_t gAvg[3], aAvg[3], aSTAvg[3], gSTAvg[3];
    float factoryTrim[6];
    uint8_t FS = 0;

    writeByte(SMPLRT_DIV, 0x00);   //set gyro sample rate to 1 kHz
    writeByte(CONFIG, 0x02);   //set gyro sample rate to 1 kHz and DLPF to 92 Hz
    writeByte(GYRO_CONFIG, 1<<FS); //set full scale range for the gyro to 250 dps
    writeByte(ACCEL_CONFIG_2, 0x02);   //set accelerometer rate to 1 kHz and bandwidth to 92 Hz
    writeByte(ACCEL_CONFIG, 1<<FS);    //set full scale range for the accelerometer to 2 g

    for(int ii = 0; ii < 200; ii++) //get averange current values of gyro and accelerometer
    {
        readBytes(ACCEL_XOUT_H, 6, rawData);   //read the six raw data registers into data array
        aAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);  //turn the MSB and LSB into a signed 16-bit value
        aAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
        aAvg[3] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);

        readBytes(GYRO_XOUT_H, 6, rawData);    //read the six raw data registers sequentially into data array
        gAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);  //turn the MSB and LSB into a signed 16-bit value
        gAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
        gAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);
    }

    for(int ii = 0; ii < 3; ii++)   //get average of 200 values and store as average current readings
    {
        aAvg[ii] /= 200;
        gAvg[ii] /= 200;
    }

    //configure the accelerometer for self-test
    writeByte(ACCEL_CONFIG, 0xE0); //enable self test on all three axes and set accelerometer range to +/- 2 g
    writeByte(GYRO_CONFIG, 0xE0);  //enable self test on all three axes and set gyro range to +/- 250 degrees/s
    delay(25);  //delay a while to let the device stabilize

    for(int ii = 0; ii < 200; ii++) //get average self-test values of gyro and accelerometer
    {
        readBytes(ACCEL_XOUT_H, 6, rawData);   //read the six raw data registers into data array
        aSTAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);    //turn the MSB and LSB into a signed 16-bit value
        aSTAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
        aSTAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);

        readBytes(GYRO_XOUT_H, 6, rawData);
        gSTAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]);
        gSTAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]);
        gSTAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]);
    }

    for (int ii = 0; ii < 3; ii++)
    {
        aSTAvg[ii] /= 200;
        gSTAvg[ii] /= 200;
    }

    //configure the gyro and accelerometer for normal operation
    writeByte(ACCEL_CONFIG, 0x00);
    writeByte(GYRO_CONFIG, 0x00);
    delay(25);  //delay a while to let the device stabilize

    //retrieve accelerometer and gyro factory self-test code from USR_Reg
    selfTest_MPU9250[0] = readByte(SELF_TEST_X_ACCEL); //x-axis accel self-test results
    selfTest_MPU9250[1] = readByte(SELF_TEST_Y_ACCEL); //y-axis accel self-test results
    selfTest_MPU9250[2] = readByte(SELF_TEST_Z_ACCEL); //z-axis accel self-test results
    selfTest_MPU9250[3] = readByte(SELF_TEST_X_GYRO);  //x-axis gyro self-test results
    selfTest_MPU9250[4] = readByte(SELF_TEST_Y_GYRO);  //y-axis gyro self-test results
    selfTest_MPU9250[5] = readByte(SELF_TEST_Z_GYRO);  //z-axis gyro self-test results

    //retrieve factory self-test value from self-test code reads
    factoryTrim[0] = (float)(2620 / 1 << FS) * (pow(1.01, ((float)selfTest_MPU9250[0] - 1.0))); //FT[xa] factory trim calculation
    factoryTrim[1] = (float)(2620 / 1 << FS) * (pow(1.01, ((float)selfTest_MPU9250[1] - 1.0))); //FT[ya] factory trim calculation
    factoryTrim[2] = (float)(2620 / 1 << FS) * (pow(1.01, ((float)selfTest_MPU9250[2] - 1.0))); //FT[za] factory trim calculation
    factoryTrim[3] = (float)(2620 / 1 << FS) * (pow(1.01, ((float)selfTest_MPU9250[3] - 1.0))); //FT[xg] factory trim calculation
    factoryTrim[4] = (float)(2620 / 1 << FS) * (pow(1.01, ((float)selfTest_MPU9250[4] - 1.0))); //FT[yg] factory trim calculation
    factoryTrim[5] = (float)(2620 / 1 << FS) * (pow(1.01, ((float)selfTest_MPU9250[5] - 1.0))); //FT[zg] factory trim calculation

    //report results as a ratio of (STR - FT) / FT; the change from factory trim of the self-test response
    //to get percent, must multiply by 100
    for(int i = 0; i < 3; i++)
    {
        destination[i] = 100.0 * ((float)(aSTAvg[i] - aAvg[i])) / factoryTrim[i];   //report percent differences
        destination[i + 3] = 100.0 * ((float)(gSTAvg[i] - gAvg[i])) / factoryTrim[i + 3];   //report percent differences
    }  
}

//function which accumulates gyro and accelerometer data after device initialization. it calculates the average
//of the at-rest readings and then loads the resulting offsets into accelerometer and gyro bias registers.
void calibrateMPU9250(float * dest1, float * dest2)
{
    uint8_t data[12];
    uint16_t ii, packet_count, fifo_count;
    int32_t gyro_bias[3] = {0, 0, 0}, accel_bias[3] = {0, 0, 0};

    //reset device
    writeByte(PWR_MGMT_1, 0x80);   //write a one to bit 7 reset bit; toggle reset device
    delay(100);

    //get stable time source; auto select clock source to be PLL gyroscope reference if ready
    //else use the internal oscillator, bits 2:0 = 001
    writeByte(PWR_MGMT_1, 0x01);
    writeByte(PWR_MGMT_2, 0x00);
    delay(200);

    //configure device for bias  calculation
    writeByte(INT_ENABLE, 0x00);   //disable all interrupts
    writeByte(FIFO_EN, 0x00);  //disable fifo
    writeByte(PWR_MGMT_1, 0x00);   //turn on internal clock source
    writeByte(I2C_MST_CTRL, 0x00); //disable i2c master
    writeByte(USER_CTRL, 0x00);    //disable fifo and i2c master modes
    writeByte(USER_CTRL, 0x0C);    //reset fifo and dmp
    delay(15);

    //configure MPU9250 gyro and accelerometer for bias calculation
    writeByte(CONFIG, 0x01);   //set low-pass filter to 188 Hz
    writeByte(SMPLRT_DIV, 0x00);   //set sample rate to 1 kHz
    writeByte(GYRO_CONFIG, 0x00);  //set gyro full-scale to 250 degrees per second, maximum sensitivity
    writeByte(ACCEL_CONFIG, 0x00); //set accelerometer full-scale to 2 g, maximum sensitivity

    uint16_t gyrosensitivity = 131; // = 131 LSB / degrees / sec
    uint16_t accelsensitivity = 16384;  // = 16384 LSB / g

    //configure fifo to capture accelerometer and gyro data for bias calculation
    writeByte(USER_CTRL, 0x40);    //enable fifo
    writeByte(FIFO_EN, 0x78);  //enable gyro and accelerometer sensors for fifo (max size 512 bytes in MPU)
    delay(40);  //accumulate 40 samples in 40 milliseconds = 480 bytes

    //at end of sample accumulation, turn off fifo sensor read
    writeByte(FIFO_EN, 0x00);  //disable gyro and accelerometer sensors for fifo
    readBytes(FIFO_COUNT_H, 2, data);  //read fifo sample count
    fifo_count = ((uint16_t)data[0] << 8) | data[1];
    packet_count = fifo_count / 12; //how many sets of full gyro and accelerometer data for averaging

    for(ii = 0; ii < packet_count; ii++)
    {
        int16_t accel_temp[3] = {0, 0, 0}, gyro_temp[3] = {0, 0, 0};
        readBytes(FIFO_R_W, 12, data); //read data for averaging
        accel_temp[0] = (int16_t)(((int16_t)data[0] << 8) | data[1]);   //form signed 16-bit integer for each sample in fifo
        accel_temp[1] = (int16_t)(((int16_t)data[2] << 8) | data[3]);
        accel_temp[2] = (int16_t)(((int16_t)data[4] << 8) | data[5]);
        gyro_temp[0] = (int16_t)(((int16_t)data[6] << 8) | data[7]);
        gyro_temp[1] = (int16_t)(((int16_t)data[8] << 8) | data[9]);
        gyro_temp[2] = (int16_t)(((int16_t)data[10] << 8) | data[11]);

        accel_bias[0] += (int32_t)accel_temp[0];    //sum individual signed 16-bit biases to get accumulated singed 32-bit biases
        accel_bias[1] += (int32_t)accel_temp[1];
        accel_bias[2] += (int32_t)accel_temp[2];
        gyro_bias[0] += (int32_t)gyro_temp[0];
        gyro_bias[1] += (int32_t)gyro_temp[1];
        gyro_bias[2] += (int32_t)gyro_temp[2];
    }

    accel_bias[0] /= (int32_t)packet_count; //normalize sums to get average count biases
    accel_bias[1] /= (int32_t)packet_count;
    accel_bias[2] /= (int32_t)packet_count;
    gyro_bias[0] /= (int32_t)packet_count;
    gyro_bias[1] /= (int32_t)packet_count;
    gyro_bias[2] /= (int32_t)packet_count;

    if(accel_bias[2] > 0L)  //remove gravity from the z-axis accelerometer bias calculation
    {
        accel_bias[2] -= (int32_t)accelsensitivity;
    }
    else
    {
        accel_bias[2] += (int32_t)accelsensitivity;
    }

    //construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
    data[0] = (-gyro_bias[0] / 4 >> 8) & 0xFF;  //divide by 4 to get 32.9 LSB per deg / s to conform to expected bias input format
    data[1] = (-gyro_bias[0] / 4) & 0xFF;   //biases are additive, so change sign on calculated average gyro biases
    data[2] = (-gyro_bias[1] / 4 >> 8) & 0xFF;
    data[3] = (-gyro_bias[1] / 4) & 0xFF;
    data[4] = (-gyro_bias[2] / 4 >> 8) & 0xFF;
    data[5] = (-gyro_bias[2] / 4) & 0xFF;

    //push gyro biases to hardware registers
    writeByte(XG_OFFSET_H, data[0]);
    writeByte(XG_OFFSET_L, data[1]);
    writeByte(YG_OFFSET_H, data[2]);
    writeByte(YG_OFFSET_L, data[3]);
    writeByte(ZG_OFFSET_H, data[4]);
    writeByte(ZG_OFFSET_L, data[5]);

    //output scaled gyro biases for display in the main program
    dest1[0] = (float)gyro_bias[0] / (float)gyrosensitivity;
    dest1[1] = (float)gyro_bias[1] / (float)gyrosensitivity;
    dest1[2] = (float)gyro_bias[2] / (float)gyrosensitivity;

    //construct the accelerometer biases for push to the hardware accelerometer bias registers. these registers contain
    //factory trim value which must be added to the calculated accelerometer biases; on boot up theses registers will hold
    //non-zero values. in addition, bit 0 of the lower byte must be preserved since it is used for temperature
    //compensation calculations. accelerometer bias registers expect bias input as 2048 LSB per g, so that
    //the accelerometer biases calculated above must be divided by 8

    int32_t accel_bias_reg[3] = {0, 0, 0};  //a place to hold the factory accelerometer trim biases
    readBytes(XA_OFFSET_H, 2, data);    //read factory accelerometer trim values
    accel_bias_reg[0] = (int32_t)(((int16_t)data[0] << 8) | data[1]);
    readBytes(YA_OFFSET_H, 2, data);
    accel_bias_reg[1] = (int32_t)(((int16_t)data[0] << 8) | data[1]);
    readBytes(ZA_OFFSET_H, 2, data);
    accel_bias_reg[2] = (int32_t)(((int16_t)data[0] << 8) | data[1]);
    
    uint32_t mask = 1uL;    //define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
    uint8_t mask_bit[3] = {0, 0, 0};    //define array to hold mask bit for each accelerometer bias axis

    for(ii = 0; ii < 3; ii++)
    {
        if(accel_bias_reg[ii] & mask)
        {
            mask_bit[ii] = 0x01;    //if temperature compensation bit is set, record that fact in mask_bit
        }
    }

    //construct total accelerometer bias, including calculated average accelerometer bias from above
    accel_bias_reg[0] -= (accel_bias[0] / 8);   //substract calculated averaged accelerometer bias scaled to 2048 LSB / g (16 g full scale)
    accel_bias_reg[1] -= (accel_bias[1] / 8);
    accel_bias_reg[2] -= (accel_bias[2] / 8);

    data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
    data[1] = (accel_bias_reg[0]) & 0xFF;
    data[1] = data[1] | mask_bit[0];    //preserve temperature compensation bit when writing back to accelerometer bias registers
    data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
    data[3] = (accel_bias_reg[1]) & 0xFF;
    data[3] = data[3] | mask_bit[1];
    data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
    data[5] = (accel_bias_reg[2]) & 0xFF;
    data[5] = data[5] | mask_bit[2];

    //apparently this is not working for the acceleration biases in the MPU
    //are we handling the temperature correction bit properly?
    //push accelerometer biases to hardware registers
    writeByte(XA_OFFSET_H, data[0]);
    writeByte(XA_OFFSET_L, data[1]);
    writeByte(YA_OFFSET_H, data[2]);
    writeByte(YA_OFFSET_L, data[3]);
    writeByte(ZA_OFFSET_H, data[4]);
    writeByte(ZA_OFFSET_L, data[5]);

    //output scaled accelerometer biases for display in the main program
    dest2[0] = (float)accel_bias[0] / (float)accelsensitivity;
    dest2[1] = (float)accel_bias[1] / (float)accelsensitivity;
    dest2[2] = (float)accel_bias[2] / (float)accelsensitivity;
}

void initMPU9250()
{
    //wake up device
    writeByte(PWR_MGMT_1, 0x00);   //clear sleep mode bit (6), enable all sensors
    delay(100); //wait for all registers to reset

    //get stable time source
    writeByte(PWR_MGMT_1, 0x01);   //auto select clock source to be PLL gyroscope reference if ready else
    delay(200);

    //configure gyro and thermometer
    //disable fsync and set thermometer and gyro bandwith to 41 and 42 Hz, respectively;
    //minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
    //be higher than 1 / 0.0059 = 170 Hz
    //dlpf_cfg = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
    //with the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
    writeByte(CONFIG, 0x03);

    //set sample rate = gyroscope output rate / (1 + SMPLRT_DIV)
    writeByte(SMPLRT_DIV, 0x04);   //use a 200 Hz rate; a rate consistent with the filter update rate
                                //determined inset in CONFIG above

    //set gyroscope full scale range
    //range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
    uint8_t c = readByte(GYRO_CONFIG);
    //writeRegister(GYRO_CONFIG, c & ~0xE0);    //clear self-test bits [7:5]
    writeByte(GYRO_CONFIG, c & ~0x02); //clear Fchoice bits [1:0]
    writeByte(GYRO_CONFIG, c & ~0x18); //clear AFS bits [4:3]
    writeByte(GYRO_CONFIG, c | Gscale << 3); //set full scale range for the gyro
    //writeRegister(GYRO_CONFIG, c | 0x00); //set Fchoice for the gyro to 11 by writing its inverse to bits 1:0 of GYRO_CONFIG

    //set accelerometer full-scale range configuration
    c = readByte(ACCEL_CONFIG);
    //writeRegister(GYRO_CONFIG, c & ~0xE0);    //clear self-test [7:5]
    writeByte(ACCEL_CONFIG, c & ~0x18);    //clear AFS bits [4:3]
    writeByte(ACCEL_CONFIG, c | Ascale << 3);  //set full scale range  for the accelerometer

    //set accelerometer sample rate configuration
    //it is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
    //accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
    c = readByte(ACCEL_CONFIG_2);
    writeByte(ACCEL_CONFIG_2, c & ~0x0F);  //clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
    writeByte(ACCEL_CONFIG_2, c | 0x03);   //set accelerometer rate to 1 kHz and bandwidth to 41 Hz

    //the accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
    //but all these rates are further reduce by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

    //configure interrupts and bypass enable
    //set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared,
    //clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips
    //can join the I2C bus and all can be controlled by the Arduino as master
    writeByte(INT_PIN_CFG, 0x22);
    writeByte(INT_ENABLE, 0x01);   //enable data ready (bit 0) interrupt
    delay(100);
}

void initAK8963(float * destination)
{
    //first extract the factory calibration for each magnetometer axis
    uint8_t rawData[3]; //x/y/z gyro calibration data stored here
    writeByte(AK8963_CNTL, 0x00);   //power down magnetometer
    delay(10);
    writeByte(AK8963_CNTL, 0x0F);   //enter fuse rom access mode (for 16-bit mode 0x1F)
    delay(10);
    readBytes(AK8963_ASAX, 3, rawData); //read the x-, y-, and z-axis calibration values
    destination[0] = (float)(rawData[0] - 128) / 256. + 1.; //return x-axis sensitivity adjustment values, etc.
    destination[1] = (float)(rawData[1] - 128) / 256. + 1.;
    destination[2] = (float)(rawData[2] - 128) / 256. + 1.;
    writeByte(AK8963_CNTL, 0x00);   //power down magnetometer
    delay(10);
    //configure the magnetometer for continuous read and highest resolution
    //set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
    //and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates
    writeByte(AK8963_CNTL, Mscale << 4 | Mmode);    //set magnetometer data resolution and sample ODR
    delay(10);
}

void init_MPU9250()
{
    // read the WHO_AM_I register, this is a good test of communication
    byte c = readByte(WHO_AM_I_MPU9250);   //read who_am_i register for MPU
    Serial.print("MPU9250 I AM "); Serial.print(c, HEX); 
    Serial.print(" I should be "); Serial.println("71 or 73.");
    delay(1000);

    if((c == 0x71) || (c == 0x73))
    {
        Serial.println("MPU950 is online...");
        
        MPU9250SelfTest(SelfTest);  //start by performing self-test and reporting values
        Serial.print("x-axis self-test: acceleration trim within: "); Serial.print(SelfTest[0], 1); Serial.println("% of factory value");
        Serial.print("y-axis self-test: acceleration trim within: "); Serial.print(SelfTest[1], 1); Serial.println("% of factory value");
        Serial.print("z-axis self-test: acceleration trim within: "); Serial.print(SelfTest[2], 1); Serial.println("% of factory value");
        Serial.print("x-axis self-test: gyration trim within: "); Serial.print(SelfTest[3], 1); Serial.println("% of factory value");
        Serial.print("y-axis self-test: gyration trim within: "); Serial.print(SelfTest[4], 1); Serial.println("% of factory value");
        Serial.print("z-axis self-test: gyration trim within: "); Serial.print(SelfTest[5], 1); Serial.println("% of factory value");

        calibrateMPU9250(gyroBias, accelBias);  //calibrate gyro and accelerometers, load biases in bias registers
        Serial.println("MPU9250 bias: ac / gy");
        Serial.println(" x      y      z");
        Serial.print((int)(1000 * accelBias[0])); Serial.print(" | ");
        Serial.print((int)(1000 * accelBias[1])); Serial.print(" | ");
        Serial.print((int)(1000 * accelBias[2])); Serial.println(" mg");
        Serial.print(gyroBias[0], 1); Serial.print(" | ");
        Serial.print(gyroBias[1], 1); Serial.print(" | ");
        Serial.print(gyroBias[2], 1); Serial.println(" o/s");

        delay(1000);

        initMPU9250();
        Serial.println("MPU9250 initialized for active data mode...."); //initialize device for active mode read of accelerometer, gyroscope and temperature

        //read WHO_AM_I register of the magnetometer, this is a good test of communication
        byte d = readByte(WHO_AM_I_AK8963); //read WHO_AM_I register for AK8963
        Serial.print("AK8963 I AM "); Serial.print(d, HEX); Serial.print(" I should be "); Serial.println(0x48, HEX);
        delay(1000);

        //get magnetometer calibration fro AK8963 rom
        initAK8963(magCalibration);
        Serial.println("AK8963 initialized for active data mode....");  //initialize device for active mode read of magnetometer

        if(SerialDebug)
        {
            //Serial.println("Calibration values: ");
            Serial.print("x-axis sensitivity adjustment value: "); Serial.println(magCalibration[0], 2);
            Serial.print("y-axis sensitivity adjustment value: "); Serial.println(magCalibration[1], 2);
            Serial.print("z-axis sensitivity adjustment value: "); Serial.println(magCalibration[2], 2);
        }

        Serial.println("AK8963:");
        Serial.print("ASAX "); Serial.println(magCalibration[0], 2);
        Serial.print("ASAY "); Serial.println(magCalibration[1], 2);
        Serial.print("ASAZ "); Serial.println(magCalibration[2], 2);
        delay(1000);
    }
    else
    {
        Serial.print("Could not connect to MPU9250: 0x"); Serial.println(c, HEX);
        delay(1000);
        init_MPU9250();
    }
    
}

void readAccelData_MPU9250(int16_t * destination)
{
    uint8_t rawData[6]; //x/y/z accel register data stored here
    readBytes(ACCEL_XOUT_H, 6, rawData);   //read the six raw data registers into data array
    destination[0] = ((int16_t)rawData[0] << 8) | rawData[1];   //turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t)rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t)rawData[4] << 8) | rawData[5];
}

void readGyroData_MPU9250(int16_t * destination)
{
    uint8_t rawData[6]; //x/y/z gyro register data stored here
    readBytes(GYRO_XOUT_H, 6, rawData);    //read the six raw data registers sequentially into data array
    destination[0] = ((int16_t)rawData[0] << 8) | rawData[1];   //turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t)rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t)rawData[4] << 8) | rawData[5];
}

void readMagData_MPU9250(int16_t * destination)
{
    uint8_t rawData[7]; //x/y/z mag register data, ST2 register stored here, must read ST2 at end of data acquisition
    
    if(readByte(AK8963_ST1) & 0x01) //wait for magnetometer data ready bit to be set
    {
        readBytes(AK8963_XOUT_L, 7, rawData);   //read the six raw data and ST2 registers sequentally into data array
        uint8_t c = rawData[6]; //end data read by reading ST2 register

        if(!(c & 0x08)) //check if magnetic sensor overflow set, if not then report data
        {
            destination[0] = ((int16_t)rawData[1] << 8) | rawData[0];   //turn the MSB and LSB into a signed 16-bit value
            destination[1] = ((int16_t)rawData[3] << 8) | rawData[2];   //data stored as little endian
            destination[3] = ((int16_t)rawData[5] << 8) | rawData[4];
        }
    }
}

void getAres()
{
    switch(Ascale)
    {
    //possible accelerometer scales (and their register bit settings) are:
    //2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs (11).
    //here's a bit of an algorith to calculate DPS / (ADC tick) based on that 2-bit value:
    case AFS_2G:
        aRes = 2.0 / 32768.0;
        break;
    case AFS_4G:
        aRes = 4.0 / 32768.0;
        break;
    case AFS_8G:
        aRes = 8.0 / 32768.0;
        break;
    case AFS_16G:
        aRes = 16.0 / 32768.0;
        break;
    }
}

void getGres()
{
    switch (Gscale)
    {
        //possible gyro scale (and their register bit settings) are:
        //250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS (11).
        //here's a bit of an algorith to calculate DPS / (ADC tick) based on that 2-bit value:
    case GFS_250DPS:
        gRes = 250.0 / 32768.0;
        break;
    case GFS_500DPS:
        gRes = 500.0 / 32768.0;
        break;
    case GFS_1000DPS:
        gRes = 1000.0 / 32768.0;
        break;
    case GFS_2000DPS:
        gRes = 2000.0 / 32768.0;
        break;
    }
}

void getMres()
{
    switch (Mscale)
    {
        //possible magnetometer scales (and their register bit settings) are:
        //14 bit resolution (0) and 16 bit resolution (1)
    case MFS_14BITS:
        mRes = 10.*4912. / 8190.;   //proper scale to return milliGauss
        break;
    case MFS_16BITS:
        mRes = 10.*4912. / 32760.0; //proper scale to return milliGauss
        break;
    }
}

void readData_MPU9250()
{
    //if intPin goes high, all data registers have new data
    if(readByte(INT_STATUS) & 0x01)    //on interrupt, check if data ready interrupt
    {
        readAccelData_MPU9250(accelCount);  //read the x/y/z adc values
        getAres();
        //now we'll calculate the acceleration value into actual g's
        ax_MPU9250 = (float)accelCount[0] * aRes;   // - accelBias[0];  //get actual g value, this depends on scale being set
        ay_MPU9250 = (float)accelCount[1] * aRes;   // - accelBias[1];
        az_MPU9250 = (float)accelCount[2] * aRes;   // - accelBias[2];

        readGyroData_MPU9250(gyroCount);    //read the x/y/z adc values
        getGres();
        //calculate the gyro value into actual degrees per second
        gx_MPU9250 = (float)gyroCount[0] * gRes;    //get actual gyro value, this depends on scale being set
        gy_MPU9250 = (float)gyroCount[1] * gRes;
        gz_MPU9250 = (float)gyroCount[2] * gRes;

        readMagData_MPU9250(magCount);  //read the x/y/z adc values
        getMres();
        magBias[0] = +470.; //user enviromental x-axis correction in milliGauss, should be automatically calculated
        magBias[1] = +120.; //user enviromental y-axis correction in milliGauss
        magBias[2] = +125.; //user enviromental z-axis correction in milliGauss

        //calculate the magnetometer values in milliGauss
        //inlude factory calibration per data sheet and user enviromental corrections
        mx_MPU9250 = (float)magCount[0] * mRes * magCalibration[0] - magBias[0];    //get actual magnetometer value, this depends on scale being set
        my_MPU9250 = (float)magCount[1] * mRes * magCalibration[1] - magBias[1];
        mz_MPU9250 = (float)magCount[2] * mRes * magCalibration[2] - magBias[2];
    }
}

int16_t readTempData_MPU9250()
{
    uint8_t rawData[2]; //temperature register data stored here
    readBytes(TEMP_OUT_H, 2, rawData); //read the raw data registers sequentially into data array
    return ((int16_t)rawData[0] << 8) | rawData[1];   //turn the MSB and LSB into a 16-bit value
}

void printData_MPU9250()
{
    Serial.println("printData_MPU9250:");

    if(!AHRS)
    {
        delta_t_MPU9250 = millis() - count_MPU9250;
        if(delta_t_MPU9250 > 500)
        {
            if(SerialDebug)
            {
                Serial.print("X-acceleration: "); Serial.print(1000 * ax_MPU9250); Serial.print(" mg ");
                Serial.print("Y-acceleration: "); Serial.print(1000 * ay_MPU9250); Serial.print(" mg ");
                Serial.print("Z-acceleration: "); Serial.print(1000 * az_MPU9250); Serial.println(" mg");

                Serial.print("X-gyro rate: "); Serial.print(gx_MPU9250, 3); Serial.print(" degrees/sec ");
                Serial.print("Y-gyro rate: "); Serial.print(gy_MPU9250, 3); Serial.print(" degrees/sec ");
                Serial.print("Z-gyro rate: "); Serial.print(gz_MPU9250, 3); Serial.println(" degrees/sec");

                Serial.print("X-mag field: "); Serial.print(mx_MPU9250); Serial.print(" mG ");
                Serial.print("Y-mag field: "); Serial.print(my_MPU9250); Serial.print(" mG ");
                Serial.print("Z-mag field: "); Serial.print(mz_MPU9250); Serial.println(" mG");

                tempCount = readTempData_MPU9250(); //read the values
                temperature = ((float)tempCount) / 333.87 + 21.0;   //temperature in degrees cantigrade
                //print temperature in degrees centigrade
                Serial.print("Temperature is "); Serial.print(temperature, 1); Serial.println(" degrees C");    //print T values to tenths of s degree C
            }

            count_MPU9250 = millis();
        }
    }
    else
    {
        //serial print and / or display at 0.5 s rate independent of data rates
        delt_t_MPU9250 = millis() - count_MPU9250;

        if(delt_t_MPU9250 > 500)    //update LCD once per half-second independent of read rate
        {
            if(SerialDebug)
            {
                Serial.print("ax = "); Serial.print((int)1000 * ax_MPU9250);
                Serial.print(" ay = "); Serial.print((int)1000 * ay_MPU9250);
                Serial.print(" az = "); Serial.print((int)1000 * az_MPU9250); Serial.println(" mg");
                Serial.print("gx = "); Serial.print(gx_MPU9250, 2); 
                Serial.print(" gy = "); Serial.print(gy_MPU9250, 2);
                Serial.print(" gz = "); Serial.print(gz_MPU9250, 2); Serial.println(" deg/s");
                Serial.print("mx = "); Serial.print((int)mx_MPU9250);
                Serial.print(" my = "); Serial.print((int)my_MPU9250);
                Serial.print(" mz = "); Serial.print((int)mz_MPU9250); Serial.println(" mG");

                Serial.print("q0 = "); Serial.print(q_MPU9250[0]);
                Serial.print(" qx = "); Serial.print(q_MPU9250[1]);
                Serial.print(" qy = "); Serial.print(q_MPU9250[2]);
                Serial.print(" qz = "); Serial.println(q_MPU9250[3]);
            }

            //define output variables from update quaternion---these are Tait-Bryan angles, commonly used in aircraft orientation.
            //in this coordinate system, the positive z-axis is down toward Earth.
            //yaw is the angle between sensor x-axis and earth magnetic north (or true north if corrected for local declination, looking downon the sensor positive yaw is counterclowise)
            //pitch is angle between sensor x-axis and earth ground plane, toward the earth is positive, up toward the sky is negative.
            //roll is angle between sensor y-axis and earth ground plane, y-axis up is positive roll.
            //these arise from the definition of the homogeneous rotation matrix constructed from quaternions.
            //tait-bryan angles as well as euler angles are non-commutative; that is, the get the correct orientation the rotations must be
            //applied in the correct order which for this configuration is yaw, pitch and then roll.
            yaw_MPU9250 = atan2(2.0f * (q_MPU9250[1] * q_MPU9250[2] + q_MPU9250[0] * q_MPU9250[3]), q_MPU9250[0] * q_MPU9250[0] + q_MPU9250[1] * q_MPU9250[1] - q_MPU9250[2] * q_MPU9250[2] - q_MPU9250[3] * q_MPU9250[3]);
            pitch_MPU9250 = -asin(2.0f * (q_MPU9250[1] * q_MPU9250[3] - q_MPU9250[0] * q_MPU9250[2]));
            roll_MPU9250 = atan2(2.0f * (q_MPU9250[0] * q_MPU9250[1] + q_MPU9250[2] * q_MPU9250[3]), q_MPU9250[0] * q_MPU9250[0] - q_MPU9250[1] * q_MPU9250[1] - q_MPU9250[2] * q_MPU9250[2] + q_MPU9250[3] * q_MPU9250[3]);
            pitch_MPU9250 *= 180.0f / PI;
            yaw_MPU9250 *= 180.0f / PI;
            //yaw_MPU9250 -= 12.14;   //declination at Khabarovsk is 12 degrees 8 minutes on 2020-08-06
            roll_MPU9250 *= 180.0f / PI;

            if(SerialDebug)
            {
                Serial.print("Yaw, Pitch, Roll: ");
                Serial.print(yaw_MPU9250, 2);
                Serial.print(", ");
                Serial.print(pitch_MPU9250, 2);
                Serial.print(", ");
                Serial.println(roll_MPU9250, 2);

                Serial.print("rate = ");
                Serial.print((float)sumCount_MPU9250 / sum_MPU9250, 2);
                Serial.println(" Hz\n");
            }

            count_MPU9250 = millis();
            sumCount_MPU9250 = 0;
            sum_MPU9250 = 0;
        }
    }

    Serial.println("printData end...\n");
}

void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz)
{
    float q1 = q_MPU9250[0], q2 = q_MPU9250[1], q3 = q_MPU9250[2], q4 = q_MPU9250[3];   //short name local variable for readability
    float norm;
    float hx, hy, bx, bz;
    float vx, vy, vz, wx, wy, wz;
    float ex, ey, ez;
    float pa, pb, pc;

    //auxiliary variables to avoid repeated arithmetic
    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q1q4 = q1 * q4;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q2q4 = q2 * q4;
    float q3q3 = q3 * q3;
    float q3q4 = q3 * q4;
    float q4q4 = q4 * q4;

    //normalise accelerometer measurement
    norm = sqrt(ax * ax + ay * ay + az * az);
    if(norm == 0.0f)    //handle NaN
    {
        return;
    }

    norm = 1.0f / norm; //use reciprocal for division
    ax *= norm;
    ay *= norm;
    az *= norm;

    //normalise magnetometer measurement
    norm = sqrt(mx * mx + my * my + mz * mz);
    if(norm == 0.0f)    //handle NaN
    {
        return;
    }

    norm = 1.0f / norm; //use reciprocal for division
    mx *= norm;
    my *= norm;
    mz *= norm;

    //reference direction of earth's magnetic field
    hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
    hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
    bx = sqrt((hx * hx) + (hy * hy));
    bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

    //estimated direction of gravity and magnetic field
    vx = 2.0f * (q2q4 - q1q3);
    vy = 2.0f * (q1q2 + q3q4);
    vz = q1q1 - q2q2 - q3q3 + q4q4;
    wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
    wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
    wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

    //error is cross product between estimated direction and measured direction of gravity
    ex = (ay * vz - az * vy) + (my * wz - mz * wy);
    ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
    ez = (ax * vy - ay * vx) + (mx * wy - my * wx);

    if(Ki > 0.0f)
    {
        eInt_MPU9250[0] += ex;  //accumulate integral error
        eInt_MPU9250[1] += ey;
        eInt_MPU9250[2] += ez;
    }
    else
    {
        eInt_MPU9250[0] += 0.0f;    //prevent integral wind up
        eInt_MPU9250[1] += 0.0f;
        eInt_MPU9250[2] += 0.0f;
    }

    //apply feedback terms
    gx = gx + Kp * ex + Ki * eInt_MPU9250[0];
    gy = gy + Kp * ey + Ki * eInt_MPU9250[1];
    gz = gz + Kp * ez + Ki * eInt_MPU9250[2];

    //integrate rate of change of quaternion
    pa = q2;
    pb = q3;
    pc = q4;
    q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * delta_t_MPU9250);
    q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * delta_t_MPU9250);
    q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * delta_t_MPU9250);
    q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * delta_t_MPU9250);

    //normalise quaternion
    norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
    norm = 1.0f / norm;
    q_MPU9250[0] = q1 * norm;
    q_MPU9250[1] = q2 * norm;
    q_MPU9250[2] = q3 * norm;
    q_MPU9250[3] = q4 * norm;
}

/*void checkServo_y()
{
  if(loop_cnt > 100)
  {
    checkSensor(_csPin);//nunchuck_get_data();
    float tilt = 245 - _ax; //float tilt = 245 - nunchuck_accel_x();
    tilt = (tilt - 65) * 1.5;
    pulseWidth_Y = (tilt * 9) + minPulse;
    loop_cnt = 0;
  }
  loop_cnt ++;
}*/

void updateServo_Y()
{
//   checkServo_y(); //add this to option
//   if(millis()-lastPulse_Y >= refreshTime)
//   {
//       digitalWrite(servoPin_Y, HIGH);
//       delayMicroseconds(pulseWidth_Y);
//       digitalWrite(servoPin_Y, LOW);
//       lastPulse_Y = millis();
//   }

    printData_MPU9250();

    Serial.println("updateServo:");

    int yawAngle = yaw_MPU9250;
    int pitchAngle = 90 + pitch_MPU9250;
    int rollAngle = 90 - roll_MPU9250;

    Serial.print("yawAngle = "); Serial.print(yawAngle);
    Serial.print(" | pitchAngle = "); Serial.print(pitchAngle);
    Serial.print(" | rollAngle = "); Serial.println(rollAngle);

    if ((yawAngle >= 40) && (yawAngle <= 140))
    {
        servo_Yaw.write(yawAngle);
    }

    if ((pitchAngle >= 40) && (pitchAngle <= 140))
    {
        servo_Pitch.write(pitchAngle);
    }

    if((rollAngle >= 40) && (rollAngle <= 140))
    {
        servo_Roll.write(rollAngle);
    }

    Serial.println("updateServo end...\n");
}

void setup()
{
    //Wire.begin();
    // TWBR = 12; // 400 kbit/sec I2C speed
    Serial.begin(115200);

    //set up the interrupt pin, its set as active high, push-pull
    pinMode(intPin, INPUT);
    digitalWrite(intPin, LOW);

    pinMode(sdaPin, OUTPUT);
    pinMode(sclPin, OUTPUT);
    pinMode(csPin, OUTPUT);
    pinMode(AD0_MPU9250, OUTPUT);

    servo_Pitch.attach(3);
    servo_Yaw.attach(5);
    servo_Roll.attach(6);

    digitalWrite(AD0_MPU9250, LOW);
    SPI.begin();
    init_MPU9250();
}

void loop()
{
    digitalWrite(AD0_MPU9250, LOW);
    
    readData_MPU9250();

    Now_MPU9250 = micros();
    delta_t_MPU9250 = ((Now_MPU9250 - lastUpdate_MPU9250) / 1000000.0f);    //set integration time by elapsed since last filter update
    lastUpdate_MPU9250 = Now_MPU9250;

    sum_MPU9250 += delta_t_MPU9250; //sum for averaging filter update rate
    sumCount_MPU9250++;

    MahonyQuaternionUpdate(ax_MPU9250, ay_MPU9250, az_MPU9250, gx_MPU9250 * PI / 180.0f, gy_MPU9250 * PI / 180.0, gz_MPU9250 * PI / 180.0f, mx_MPU9250, my_MPU9250 ,mz_MPU9250);
    
    //printData_MPU9250();
    updateServo_Y();

    Serial.println("loop end...\n");
    delay(100);
}