/************************************************************************/
// Use SPI interface to talk to MPU9250
// Arduino Nano v3.0 version
/************************************************************************/

#include <Arduino.h>
#include <SPI.h>

/************************************************************************/
    enum GyroRange
    {
      GYRO_RANGE_250DPS,
      GYRO_RANGE_500DPS,
      GYRO_RANGE_1000DPS,
      GYRO_RANGE_2000DPS
    };
    enum AccelRange
    {
      ACCEL_RANGE_2G,
      ACCEL_RANGE_4G,
      ACCEL_RANGE_8G,
      ACCEL_RANGE_16G    
    };
    enum DlpfBandwidth
    {
      DLPF_BANDWIDTH_184HZ,
      DLPF_BANDWIDTH_92HZ,
      DLPF_BANDWIDTH_41HZ,
      DLPF_BANDWIDTH_20HZ,
      DLPF_BANDWIDTH_10HZ,
      DLPF_BANDWIDTH_5HZ
    };
    // spi
    uint8_t _csPin;
    bool _useSPIHS;
    const uint8_t SPI_READ = 0x80;
    const uint32_t SPI_LS_CLOCK = 1000000;  // 1 MHz
    const uint32_t SPI_HS_CLOCK = 15000000; // 15 MHz
    // track success of interacting with sensor
    int _status;
    // buffer for reading from sensor
    uint8_t _buffer[21];
    // data counts
    int16_t _axcounts,_aycounts,_azcounts;
    int16_t _gxcounts,_gycounts,_gzcounts;
    int16_t _hxcounts,_hycounts,_hzcounts;
    int16_t _tcounts;
    // data buffer
    /*float accelMass[3][3] = {{0, 1, 2}, {3, 4, 5},{6, 7, 8}}; */ //Массив массивов
    float _ax, _ay, _az; 
    float _gx, _gy, _gz;
    float _hx, _hy, _hz;
    float _t;
    // scale factors
    float _accelScale;
    float _gyroScale;
    float _magScaleX, _magScaleY, _magScaleZ;
    const float _tempScale = 333.87f;
    const float _tempOffset = 21.0f;
    // configuration
    uint8_t _accelRange;
    uint8_t _gyroRange;
    uint8_t _bandwidth;
    uint8_t _srd;
    // gyro bias estimation
    size_t _numSamples = 100;
    double _gxbD, _gybD, _gzbD;
    float _gxb, _gyb, _gzb;
    // transformation matrix
    /* transform the accel and gyro axes to match the magnetometer axes */
    const int16_t tX[3] = {0,  1,  0}; 
    const int16_t tY[3] = {1,  0,  0};
    const int16_t tZ[3] = {0,  0, -1};
    // constants
    const float G = 9.807f;
    const float _d2r = 3.14159265359f/180.0f;
    // MPU9250 registers
    #define ACCEL_OUT             0x3B
    #define EXT_SENS_DATA_00      0x49
    #define ACCEL_CONFIG          0x1C
    #define ACCEL_FS_SEL_16G      0x18
    #define GYRO_CONFIG           0x1B
    #define GYRO_FS_SEL_250DPS    0x00
    #define GYRO_FS_SEL_500DPS    0x08
    #define GYRO_FS_SEL_1000DPS   0x10
    #define GYRO_FS_SEL_2000DPS   0x18
    #define ACCEL_CONFIG2         0x1D
    #define ACCEL_DLPF_184        0x01
    #define ACCEL_DLPF_92         0x02
    #define ACCEL_DLPF_41         0x03
    #define ACCEL_DLPF_20         0x04
    #define ACCEL_DLPF_10         0x05
    #define ACCEL_DLPF_5          0x06
    #define CONFIG                0x1A
    #define GYRO_DLPF_184         0x01
    #define GYRO_DLPF_92          0x02
    #define GYRO_DLPF_41          0x03
    #define GYRO_DLPF_20          0x04
    #define GYRO_DLPF_10          0x05
    #define GYRO_DLPF_5           0x06
    #define SMPDIV                0x19
    #define PWR_MGMNT_1           0x6B
    #define PWR_RESET             0x80
    #define CLOCK_SEL_PLL         0x01
    #define PWR_MGMNT_2           0x6C
    #define SEN_ENABLE            0x00
    #define USER_CTRL             0x6A
    #define I2C_MST_EN            0x20
    #define I2C_MST_CLK           0x0D
    #define I2C_MST_CTRL          0x24
    #define I2C_SLV0_ADDR         0x25
    #define I2C_SLV0_REG          0x26
    #define I2C_SLV0_DO           0x63
    #define I2C_SLV0_CTRL         0x27
    #define I2C_SLV0_EN           0x80
    #define I2C_READ_FLAG         0x80
    #define WHO_AM_I              0x75
    // AK8963 registers
    #define AK8963_I2C_ADDR       0x0C
    #define AK8963_HXL            0x03 
    #define AK8963_CNTL1          0x0A
    #define AK8963_PWR_DOWN       0x00
    #define AK8963_CNT_MEAS1      0x12
    #define AK8963_CNT_MEAS2      0x16
    #define AK8963_FUSE_ROM       0x0F
    #define AK8963_CNTL2          0x0B
    #define AK8963_RESET          0x01
    #define AK8963_ASA            0x10
    #define AK8963_WHO_AM_I       0x00
    // CS pins
    #define NCS_PIN_1   6
    #define NCS_PIN_2   7
    #define NCS_PIN_3   8
    #define NCS_PIN_4   9
    #define NCS_PIN_5   10
    #define MOSI_PIN    11
    #define MISO_PIN    12
    #define SCK_PIN     13
    
    int status;

    int NCS_PINS[] = {NCS_PIN_1/*, NCS_PIN_2, NCS_PIN_3, NCS_PIN_4, NCS_PIN_5*/};

/* reads registers from MPU9250 given a starting register address, number of bytes, and a pointer to store data */
int readRegisters(int _csPin, uint8_t subAddress, uint8_t count, uint8_t* dest){
    // begin the transaction
    if(_useSPIHS){
      SPI.beginTransaction(SPISettings(SPI_HS_CLOCK, MSBFIRST, SPI_MODE3));
    }
    else{
      SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
    }
    digitalWrite(_csPin,LOW); // select the MPU9250 chip
    SPI.transfer(subAddress | SPI_READ); // specify the starting register address
    for(uint8_t i = 0; i < count; i++){
      dest[i] = SPI.transfer(0x00); // read the data
    }
    digitalWrite(_csPin,HIGH); // deselect the MPU9250 chip
    SPI.endTransaction(); // end the transaction
    return 1;
}

/* writes a byte to MPU9250 register given a register address and data */
int writeRegister(int _csPin, uint8_t subAddress, uint8_t data){
  /* write data to device */
    SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3)); // begin the transaction
    digitalWrite(_csPin,LOW); // select the MPU9250 chip
    SPI.transfer(subAddress); // write the register address
    SPI.transfer(data); // write the data
    digitalWrite(_csPin,HIGH); // deselect the MPU9250 chip
    SPI.endTransaction(); // end the transaction
  
  delay(10);
  
  /* read back the register */
  readRegisters(_csPin, subAddress,1,_buffer);
  /* check the read back register against the written register */
  if(_buffer[0] == data) {
    return 1;
  }
  else{
    return -1;
  }
}

/* gets the MPU9250 WHO_AM_I register value, expected to be 0x71 */
int whoAmI(int _csPin){
  // read the WHO AM I register
  if (readRegisters(_csPin, WHO_AM_I,1,_buffer) < 0) {
    return -1;
  }
  // return the register value
  Serial.println(_buffer[0]);
  return _buffer[0];
}

/* reads registers from the AK8963 */
int readAK8963Registers(int _csPin, uint8_t subAddress, uint8_t count, uint8_t* dest){
  // set slave 0 to the AK8963 and set for read
  if (writeRegister(_csPin, I2C_SLV0_ADDR,AK8963_I2C_ADDR | I2C_READ_FLAG) < 0) {
    return -1;
  }
  // set the register to the desired AK8963 sub address
  if (writeRegister(_csPin, I2C_SLV0_REG,subAddress) < 0) {
    return -2;
  }
  // enable I2C and request the bytes
  if (writeRegister(_csPin, I2C_SLV0_CTRL,I2C_SLV0_EN | count) < 0) {
    return -3;
  }
  delay(1); // takes some time for these registers to fill
  // read the bytes off the MPU9250 EXT_SENS_DATA registers
  _status = readRegisters(_csPin, EXT_SENS_DATA_00,count,dest); 
  return _status;
}

/* writes a register to the AK8963 given a register address and data */
int writeAK8963Register(int _csPin, uint8_t subAddress, uint8_t data){
  // set slave 0 to the AK8963 and set for write
  if (writeRegister(_csPin, I2C_SLV0_ADDR,AK8963_I2C_ADDR) < 0) {
    return -1;
  }
  // set the register to the desired AK8963 sub address 
  if (writeRegister(_csPin, I2C_SLV0_REG,subAddress) < 0) {
    return -2;
  }
  // store the data for write
  if (writeRegister(_csPin, I2C_SLV0_DO,data) < 0) {
    return -3;
  }
  // enable I2C and send 1 byte
  if (writeRegister(_csPin, I2C_SLV0_CTRL,I2C_SLV0_EN | (uint8_t)1) < 0) {
    return -4;
  }
  // read the register and confirm
  if (readAK8963Registers(_csPin, subAddress,1,_buffer) < 0) {
    return -5;
  }
  if(_buffer[0] == data) {
    return 1;
  } else{
    return -6;
  }
}

/* gets the AK8963 WHO_AM_I register value, expected to be 0x48 */
int whoAmIAK8963(int _csPin){
  // read the WHO AM I register
  if (readAK8963Registers(_csPin, AK8963_WHO_AM_I,1,_buffer) < 0) {
    return -1;
  }
  // return the register value
  return _buffer[0];
}

/* sets the DLPF bandwidth to values other than default */
int setDlpfBandwidth(int _csPin, uint8_t bandwidth) {
  // use low speed SPI for register setting
  _useSPIHS = false;
  switch(bandwidth) {
    case DLPF_BANDWIDTH_184HZ: {
      if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_184) < 0){ // setting accel bandwidth to 184Hz
        return -1;
      } 
      if(writeRegister(_csPin, CONFIG,GYRO_DLPF_184) < 0){ // setting gyro bandwidth to 184Hz
        return -2;
      }
      break;
    }
    case DLPF_BANDWIDTH_92HZ: {
      if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_92) < 0){ // setting accel bandwidth to 92Hz
        return -1;
      } 
      if(writeRegister(_csPin, CONFIG,GYRO_DLPF_92) < 0){ // setting gyro bandwidth to 92Hz
        return -2;
      }
      break;
    }
    case DLPF_BANDWIDTH_41HZ: {
      if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_41) < 0){ // setting accel bandwidth to 41Hz
        return -1;
      } 
      if(writeRegister(_csPin, CONFIG,GYRO_DLPF_41) < 0){ // setting gyro bandwidth to 41Hz
        return -2;
      }
      break;
    }
    case DLPF_BANDWIDTH_20HZ: {
      if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_20) < 0){ // setting accel bandwidth to 20Hz
        return -1;
      } 
      if(writeRegister(_csPin, CONFIG,GYRO_DLPF_20) < 0){ // setting gyro bandwidth to 20Hz
        return -2;
      }
      break;
    }
    case DLPF_BANDWIDTH_10HZ: {
      if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_10) < 0){ // setting accel bandwidth to 10Hz
        return -1;
      } 
      if(writeRegister(_csPin, CONFIG,GYRO_DLPF_10) < 0){ // setting gyro bandwidth to 10Hz
        return -2;
      }
      break;
    }
    case DLPF_BANDWIDTH_5HZ: {
      if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_5) < 0){ // setting accel bandwidth to 5Hz
        return -1;
      } 
      if(writeRegister(_csPin, CONFIG,GYRO_DLPF_5) < 0){ // setting gyro bandwidth to 5Hz
        return -2;
      }
      break;
    }
  }
  _bandwidth = bandwidth;
  return 1;
}

/* sets the sample rate divider to values other than default */
int setSrd(int _csPin, uint8_t srd) {
  // use low speed SPI for register setting
  _useSPIHS = false;
  /* setting the sample rate divider to 19 to facilitate setting up magnetometer */
  if(writeRegister(_csPin, SMPDIV,19) < 0){ // setting the sample rate divider
    return -1;
  }
  if(srd > 9){
    // set AK8963 to Power Down
    if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_PWR_DOWN) < 0){
      return -2;
    }
    delay(100); // long wait between AK8963 mode changes  
    // set AK8963 to 16 bit resolution, 8 Hz update rate
    if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_CNT_MEAS1) < 0){
      return -3;
    }
    delay(100); // long wait between AK8963 mode changes     
    // instruct the MPU9250 to get 7 bytes of data from the AK8963 at the sample rate
    readAK8963Registers(_csPin, AK8963_HXL,7,_buffer);
  } else {
    // set AK8963 to Power Down
    if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_PWR_DOWN) < 0){
      return -2;
    }
    delay(100); // long wait between AK8963 mode changes  
    // set AK8963 to 16 bit resolution, 100 Hz update rate
    if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_CNT_MEAS2) < 0){
      return -3;
    }
    delay(100); // long wait between AK8963 mode changes     
    // instruct the MPU9250 to get 7 bytes of data from the AK8963 at the sample rate
    readAK8963Registers(_csPin, AK8963_HXL,7,_buffer);    
  } 
  /* setting the sample rate divider */
  if(writeRegister(_csPin, SMPDIV,srd) < 0){ // setting the sample rate divider
    return -4;
  } 
  _srd = srd;
  return 1; 
}

/* reads the most current data from MPU9250 and stores in buffer */
int readSensor(int _csPin) {
  _useSPIHS = true; // use the high speed SPI for data readout
  // grab the data from the MPU9250
  if (readRegisters(_csPin, ACCEL_OUT, 21, _buffer) < 0) {
    return -1;
  }
  // combine into 16 bit values
  _axcounts = (((int16_t)_buffer[0]) << 8) | _buffer[1];  
  _aycounts = (((int16_t)_buffer[2]) << 8) | _buffer[3];
  _azcounts = (((int16_t)_buffer[4]) << 8) | _buffer[5];
  _tcounts = (((int16_t)_buffer[6]) << 8) | _buffer[7];
  _gxcounts = (((int16_t)_buffer[8]) << 8) | _buffer[9];
  _gycounts = (((int16_t)_buffer[10]) << 8) | _buffer[11];
  _gzcounts = (((int16_t)_buffer[12]) << 8) | _buffer[13];
  _hxcounts = (((int16_t)_buffer[15]) << 8) | _buffer[14];
  _hycounts = (((int16_t)_buffer[17]) << 8) | _buffer[16];
  _hzcounts = (((int16_t)_buffer[19]) << 8) | _buffer[18];
  // transform and convert to float values
  _ax = (float)(tX[0]*_axcounts + tX[1]*_aycounts + tX[2]*_azcounts) * _accelScale;
  _ay = (float)(tY[0]*_axcounts + tY[1]*_aycounts + tY[2]*_azcounts) * _accelScale;
  _az = (float)(tZ[0]*_axcounts + tZ[1]*_aycounts + tZ[2]*_azcounts) * _accelScale;
  _gx = ((float)(tX[0]*_gxcounts + tX[1]*_gycounts + tX[2]*_gzcounts) * _gyroScale) - _gxb;
  _gy = ((float)(tY[0]*_gxcounts + tY[1]*_gycounts + tY[2]*_gzcounts) * _gyroScale) - _gyb;
  _gz = ((float)(tZ[0]*_gxcounts + tZ[1]*_gycounts + tZ[2]*_gzcounts) * _gyroScale) - _gzb;
  _hx = (float)(_hxcounts) * _magScaleX;
  _hy = (float)(_hycounts) * _magScaleY;
  _hz = (float)(_hzcounts) * _magScaleZ;
  _t = ((((float) _tcounts) - _tempOffset)/_tempScale) + _tempOffset;
  return 1;
}

/* sets the gyro full scale range to values other than default */
int setGyroRange(int _csPin, uint8_t range) {
  // use low speed SPI for register setting
  _useSPIHS = false;
  switch(range) {
    case GYRO_RANGE_250DPS: {
      // setting the gyro range to 250DPS
      if(writeRegister(_csPin, GYRO_CONFIG,GYRO_FS_SEL_250DPS) < 0){
        return -1;
      }
      _gyroScale = 250.0f/32767.5f * _d2r; // setting the gyro scale to 250DPS
      break;
    }
    case GYRO_RANGE_500DPS: {
      // setting the gyro range to 500DPS
      if(writeRegister(_csPin, GYRO_CONFIG,GYRO_FS_SEL_500DPS) < 0){
        return -1;
      }
      _gyroScale = 500.0f/32767.5f * _d2r; // setting the gyro scale to 500DPS
      break;  
    }
    case GYRO_RANGE_1000DPS: {
      // setting the gyro range to 1000DPS
      if(writeRegister(_csPin, GYRO_CONFIG,GYRO_FS_SEL_1000DPS) < 0){
        return -1;
      }
      _gyroScale = 1000.0f/32767.5f * _d2r; // setting the gyro scale to 1000DPS
      break;
    }
    case GYRO_RANGE_2000DPS: {
      // setting the gyro range to 2000DPS
      if(writeRegister(_csPin, GYRO_CONFIG,GYRO_FS_SEL_2000DPS) < 0){
        return -1;
      }
      _gyroScale = 2000.0f/32767.5f * _d2r; // setting the gyro scale to 2000DPS
      break;
    }
  }
  _gyroRange = range;
  return 1;
}

/* estimates the gyro biases */
int calibrateGyro(int _csPin) {
  // set the range, bandwidth, and srd
  if (setGyroRange(_csPin, GYRO_RANGE_250DPS) < 0) {
    return -1;
  }
  if (setDlpfBandwidth(_csPin, DLPF_BANDWIDTH_20HZ) < 0) {
    return -2;
  }
  if (setSrd(_csPin, 19) < 0) {
    return -3;
  }

  // take samples and find bias
  _gxbD = 0;
  _gybD = 0;
  _gzbD = 0;
  for (size_t i=0; i < _numSamples; i++) {
    readSensor(_csPin);
    _gxbD += (_gx + _gxb)/((double)_numSamples);
    _gybD += (_gy + _gyb)/((double)_numSamples);
    _gzbD += (_gz + _gzb)/((double)_numSamples);
    delay(20);
  }
  _gxb = (float)_gxbD;
  _gyb = (float)_gybD;
  _gzb = (float)_gzbD;

  // set the range, bandwidth, and srd back to what they were
  if (setGyroRange(_csPin, _gyroRange) < 0) {
    return -4;
  }
  if (setDlpfBandwidth(_csPin, _bandwidth) < 0) {
    return -5;
  }
  if (setSrd(_csPin, _srd) < 0) {
    return -6;
  }
  return 1;
}

/* starts communication with the MPU-9250 */
int begin(int _csPin){
//if( _useSPI ) { // using SPI for communication
    // use low speed SPI for register setting
    _useSPIHS = false;
    // setting CS pin to output
    pinMode(_csPin,OUTPUT);
    // setting CS pin high
    digitalWrite(_csPin,HIGH);
    // begin SPI communication
    SPI.begin();
//}
  // select clock source to gyro
  if(writeRegister(_csPin, PWR_MGMNT_1, CLOCK_SEL_PLL) < 0){
    return -1;
  }
  // enable I2C master mode
  if(writeRegister(_csPin, USER_CTRL, I2C_MST_EN) < 0){
    return -2;
  }
  // set the I2C bus speed to 400 kHz
  if(writeRegister(_csPin, I2C_MST_CTRL, I2C_MST_CLK) < 0){
    return -3;
  }
  // set AK8963 to Power Down
  writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_PWR_DOWN);
  // reset the MPU9250
  writeRegister(_csPin, PWR_MGMNT_1, PWR_RESET);
  // wait for MPU-9250 to come back up
  delay(1);
  // reset the AK8963
  writeAK8963Register(_csPin, AK8963_CNTL2, AK8963_RESET);
  // select clock source to gyro
  if(writeRegister(_csPin, PWR_MGMNT_1, CLOCK_SEL_PLL) < 0){
    return -4;
  }
  // check the WHO AM I byte, expected value is 0x71 (decimal 113) or 0x73 (decimal 115)
  if((whoAmI(_csPin) != 113)&&(whoAmI(_csPin) != 115)){
    return -5;
  }
  // enable accelerometer and gyro
  if(writeRegister(_csPin, PWR_MGMNT_2,SEN_ENABLE) < 0){
    return -6;
  }
  // setting accel range to 16G as default
  if(writeRegister(_csPin, ACCEL_CONFIG,ACCEL_FS_SEL_16G) < 0){
    return -7;
  }
  _accelScale = G * 16.0f/32767.5f; // setting the accel scale to 16G
  _accelRange = ACCEL_RANGE_16G;
  // setting the gyro range to 2000DPS as default
  if(writeRegister(_csPin, GYRO_CONFIG,GYRO_FS_SEL_2000DPS) < 0){
    return -8;
  }
  _gyroScale = 2000.0f/32767.5f * _d2r; // setting the gyro scale to 2000DPS
  _gyroRange = GYRO_RANGE_2000DPS;
  // setting bandwidth to 184Hz as default
  if(writeRegister(_csPin, ACCEL_CONFIG2,ACCEL_DLPF_184) < 0){ 
    return -9;
  } 
  if(writeRegister(_csPin, CONFIG,GYRO_DLPF_184) < 0){ // setting gyro bandwidth to 184Hz
    return -10;
  }
  _bandwidth = DLPF_BANDWIDTH_184HZ;
  // setting the sample rate divider to 0 as default
  if(writeRegister(_csPin, SMPDIV,0x00) < 0){ 
    return -11;
  } 
  _srd = 0;
  // enable I2C master mode
  if(writeRegister(_csPin, USER_CTRL,I2C_MST_EN) < 0){
    return -12;
  }
  // set the I2C bus speed to 400 kHz
  if( writeRegister(_csPin, I2C_MST_CTRL,I2C_MST_CLK) < 0){
    return -13;
  }
  // check AK8963 WHO AM I register, expected value is 0x48 (decimal 72)
  if( whoAmIAK8963(_csPin) != 72 ){
    return -14;
  }
  /* get the magnetometer calibration */
  // set AK8963 to Power Down
  if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_PWR_DOWN) < 0){
    return -15;
  }
  delay(100); // long wait between AK8963 mode changes
  // set AK8963 to FUSE ROM access
  if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_FUSE_ROM) < 0){
    return -16;
  }
  delay(100); // long wait between AK8963 mode changes
  // read the AK8963 ASA registers and compute magnetometer scale factors
  readAK8963Registers(_csPin, AK8963_ASA,3,_buffer);
  _magScaleX = ((((float)_buffer[0]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f; // micro Tesla
  _magScaleY = ((((float)_buffer[1]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f; // micro Tesla
  _magScaleZ = ((((float)_buffer[2]) - 128.0f)/(256.0f) + 1.0f) * 4912.0f / 32760.0f; // micro Tesla 
  // set AK8963 to Power Down
  if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_PWR_DOWN) < 0){
    return -17;
  }
  delay(100); // long wait between AK8963 mode changes  
  // set AK8963 to 16 bit resolution, 100 Hz update rate
  if(writeAK8963Register(_csPin, AK8963_CNTL1,AK8963_CNT_MEAS2) < 0){
    return -18;
  }
  delay(100); // long wait between AK8963 mode changes
  // select clock source to gyro
  if(writeRegister(_csPin, PWR_MGMNT_1,CLOCK_SEL_PLL) < 0){
    return -19;
  }       
  // instruct the MPU9250 to get 7 bytes of data from the AK8963 at the sample rate
  readAK8963Registers(_csPin, AK8963_HXL,7,_buffer);
  // estimate gyro bias
  if (calibrateGyro(_csPin) < 0) {
    return -20;
  }
  // successful init, return 1
  Serial.print("Sensor OK! ");
  return 1;
}

void setup(){
  // serial to display data
  Serial.begin(115200);
  while(!Serial) {}
  

  // start communication with MPU9250 
  for (int i=0, j=1; i < sizeof(NCS_PINS)/2; i++, j++){
    Serial.print("Setup sensor "); Serial.print(j); Serial.print(" begin. ");
    status = begin(NCS_PINS[i]);
    if (status < 0) {
      Serial.print("NCS_PIN_"); Serial.print(j); Serial.println(" initialization unsuccessful");
      Serial.println("Check MPU9250 wiring or try cycling power");
      Serial.print("Status_init: ");
      Serial.println(status);
      delay(500);
      setup();
    }
    Serial.print("Setup sensor "); Serial.print(j); Serial.println(" complete.");
  }
}

void loop(){
  // read the sensor MPU9250
  for (int i=0, j=1; i < sizeof(NCS_PINS)/2; i++, j++){
    readSensor(NCS_PINS[i]);
    // display the data
    Serial.print("NCS_PIN_"); Serial.print(j); Serial.print(":");
    Serial.print("\t");
    Serial.print(_ax,6);
    Serial.print("\t");
    Serial.print(_ay,6);
    Serial.print("\t");
    Serial.print(_az,6);
    Serial.print("\t");
    Serial.print(_gx,6);
    Serial.print("\t");
    Serial.print(_gy,6);
    Serial.print("\t");
    Serial.print(_gz,6);
    Serial.print("\t");
    Serial.print(_hx,6);
    Serial.print("\t");
    Serial.print(_hy,6);
    Serial.print("\t");
    Serial.println(_hz,6);
    // if (j == 4){
    // Serial.println("");}
    // else {
    // Serial.print("\t");}
    delay(100);
  }
}
