#include <Arduino.h>
#include <MPU9250.h>
#include <Servo.h>
#include <SPI.h>

Servo servoARM;

int servoPin_Y = 9;
int sclPin = 13;
int sdaPin = 11;
int adoPin = 12;
int ncsPin = 10;

int pulseWidth_Y = 0;
uint32_t refreshTime = 20;
unsigned long lastPulse_Y;
int minPulse = 700;
int loop_cnt = 0;

//

enum AccelRange
{
  ACCEL_RANGE_16G
};

enum GyroRange
{
  GYRO_RANGE_2000DPS,
  GYRO_RANGE_250DPS
};

enum DlpfBandwidth
{
  DLPF_BANDWIDTH_184HZ,
  DLPF_BANDWIDTH_20HZ
};

uint8_t _csPin;
bool _useSPIHS;
const uint8_t SPI_READ = 0x80;
const uint32_t SPI_LS_CLOCK = 1000000;
const uint32_t SPI_HS_CLOCK = 15000000;

int _status;

uint8_t _buffer[21];

int16_t _axcounts, _aycounts, _azcounts;
int16_t _gxcounts, _gycounts, _gzcounts;
int16_t _hxcounts, _hycounts, _hzcounts;
int16_t _tcounts;

float _ax, _ay, _az;
float _gx, _gy, _gz;
float _hx, _hy, _hz;
float _t;

size_t _numSamples = 100;
double _gxbD, _gybD, _gzbD;
float _gxb, _gyb, _gzb;

const int16_t tX[3] = {0, 1, 0};
const int16_t tY[3] = {1, 0, 0};
const int16_t tZ[3] = {0, 0, -1};

float _accelScale;
float _gyroScale;
float _magScaleX, _magScaleY, _magScaleZ;
const float _tempScale = 333.87f;
const float _tempOffset = 21.0f;

uint8_t _accelRange;
uint8_t _gyroRange;
uint8_t _bandwidth;
uint8_t _srd;

const float G = 9.807f;
const float _d2r = 3.14159265359f / 180.0f;

#define ACCEL_OUT             0x3B
#define ACCEL_CONFIG          0x1C
#define ACCEL_CONFIG_2        0x1D
#define ACCEL_FS_SEL_16G      0x18
#define ACCEL_DLPF_184        0x01
#define ACCEL_DLPF_20         0x04
#define EXT_SENS_DATA_00      0x49
#define PWR_MGMNT_1           0x6B
#define PWR_MGMNT_2           0x6C
#define PWR_RESET             0x80
#define SEN_ENABLE            0x00
#define CLOCK_SEL_PLL         0x01
#define USER_CTRL             0x6A
#define GYRO_CONFIG           0x1B
#define GYRO_FS_SEL_2000DPS   0x18
#define GYRO_FS_SEL_250DPS    0x00
#define GYRO_DLPF_184         0x01
#define GYRO_DLPF_20          0x04
#define CONFIG                0x1A
#define SMPDIV                0x19
#define I2C_MST_EN            0x20
#define I2C_MST_CTRL          0x24
#define I2C_MST_CLK           0x0D
#define I2C_SLV0_ADDR         0x25
#define I2C_SLV0_REG          0x26
#define I2C_SLV0_EN           0x80
#define I2C_SLV0_CTRL         0x27
#define I2C_SLV0_D0           0x63
#define I2C_READ_FLAG         0x80
#define WHO_AM_I              0x75
#define AK8963_I2C_ADDR       0x0C
#define AK8963_CNTL1          0x0A
#define AK8963_CNTL2          0x0B
#define AK8963_PWR_DOWN       0x00
#define AK8963_RESET          0x01
#define AK8963_FUSE_ROM       0x0F
#define AK8963_ASA            0x10
#define AK8963_CNT_MEAS_1     0x12
#define AK8963_CNT_MEAS_2     0x16
#define AK8963_HXL            0x03
#define AK8963_WHO_AM_I       0x00

int readRegisters(int _csPin, uint8_t subAddres, uint8_t count, uint8_t* dest) //Ok!
{
  if(_useSPIHS)
  {
    SPI.beginTransaction(SPISettings(SPI_HS_CLOCK, MSBFIRST, SPI_MODE3));
  }
  else
  {
    SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));
  }

  digitalWrite(_csPin, LOW);

  SPI.transfer(subAddres | SPI_READ);

  for(uint8_t i = 0; i < count; i++)
  {
    dest[i] = SPI.transfer(0x00);
  }

  digitalWrite(_csPin, HIGH);

  SPI.endTransaction();

  return 1;  
}

int writeRegister(int _csPin, uint8_t subAddres, uint8_t data) //Ok!
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3));

  digitalWrite(_csPin, LOW);

  SPI.transfer(subAddres);
  SPI.transfer(data);

  digitalWrite(_csPin, HIGH);

  SPI.endTransaction();

  delay(10);

  readRegisters(_csPin, subAddres, 1, _buffer);

  if(_buffer[0] == data)
  {
    return 1;
  }
  else
  {
    return -1;
  }
}

int readAK8963Registers(int _csPin, uint8_t subAddres, uint8_t count, uint8_t* dest) //Ok!
{
  if(writeRegister(_csPin, I2C_SLV0_ADDR, AK8963_I2C_ADDR | I2C_READ_FLAG) < 0)
  {
    return -1;
  }

  if(writeRegister(_csPin, I2C_SLV0_REG, subAddres) < 0)
  {
    return -2;
  }

  if(writeRegister(_csPin, I2C_SLV0_CTRL, I2C_SLV0_EN | count) < 0)
  {
    return -3;
  }

  delay(1);

  _status = readRegisters(_csPin, EXT_SENS_DATA_00, count, dest);
  return _status;
}

int writeAK8963Register(int _csPin, uint8_t subAddres, uint8_t data) //Ok!
{
  if(writeRegister(_csPin, I2C_SLV0_ADDR, AK8963_I2C_ADDR) < 0)
  {
    return -1;
  }

  if(writeRegister(_csPin, I2C_SLV0_REG, subAddres) < 0)
  {
    return -2;
  }

  if(writeRegister(_csPin, I2C_SLV0_D0, data) < 0)
  {
    return -3;
  }

  if(writeRegister(_csPin, I2C_SLV0_CTRL, I2C_SLV0_EN | (uint8_t)1) < 0)
  {
    return -4;
  }

  if(readAK8963Registers(_csPin, subAddres, 1, _buffer) < 0)
  {
    return -5;
  }

  if(_buffer[0] == data)
  {
    return 1;
  }
  else
  {
    return -6;
  }
  
}

int checkSensor(int _csPin) //Ok! readSensor
{
  _useSPIHS = true;

  if (readRegisters(_csPin, ACCEL_OUT, 21, _buffer) < 0)
  {
    return -1;
  }

  _axcounts = (((int16_t)_buffer[0]) << 8) | _buffer[1];
  _aycounts = (((int16_t)_buffer[2]) << 8) | _buffer[3];
  _azcounts = (((int16_t)_buffer[4]) << 8) | _buffer[5];
  _tcounts = (((int16_t)_buffer[6]) << 8) | _buffer[7];
  _gxcounts = (((int16_t)_buffer[8]) << 8) | _buffer[9];
  _gycounts = (((int16_t)_buffer[10]) << 8) | _buffer[11];
  _gzcounts = (((int16_t)_buffer[12]) << 8) | _buffer[13];
  _hxcounts = (((int16_t)_buffer[14]) << 8) | _buffer[15];
  _hycounts = (((int16_t)_buffer[16]) << 8) | _buffer[17];
  _hzcounts = (((int16_t)_buffer[18]) << 8) | _buffer[19];

  _ax = (float)(tX[0] * _axcounts + tX[1] * _aycounts + tX[2] * _azcounts) * _accelScale;
  _ay = (float)(tY[0] * _axcounts + tY[1] * _aycounts + tY[2] * _azcounts) * _accelScale;
  _az = (float)(tZ[0] * _axcounts + tZ[1] * _aycounts + tZ[2] * _azcounts) * _accelScale;
  _gx = ((float)(tX[0] * _gxcounts + tX[1] * _gycounts + tX[2] * _gzcounts) * _gyroScale) - _gxb;
  _gy = ((float)(tY[0] * _gxcounts + tY[1] * _gycounts + tY[2] * _gzcounts) * _gyroScale) - _gyb;
  _gx = ((float)(tZ[0] * _gxcounts + tZ[1] * _gycounts + tZ[2] * _gzcounts) * _gyroScale) - _gzb;
  _hx = (float)(_hxcounts) * _magScaleX;
  _hy = (float)(_hycounts) * _magScaleY;
  _hz = (float)(_hzcounts) * _magScaleZ;
  _t = ((((float) _tcounts) - _tempOffset) / _tempScale) + _tempOffset;
  return 1;
}

int setDlpfBandwidth(int _csPin, uint8_t bandwidth) //Ok!
{
  _useSPIHS = false;
  switch (bandwidth)
  {
  case DLPF_BANDWIDTH_184HZ:
    if(writeRegister(_csPin, ACCEL_CONFIG_2, ACCEL_DLPF_184) < 0)
    {
      return -1;
    }
    if(writeRegister(_csPin, CONFIG, GYRO_DLPF_184) < 0)
    {
      return -2;
    }
    break;
  
  case DLPF_BANDWIDTH_20HZ:
    if(writeRegister(_csPin, ACCEL_CONFIG_2, ACCEL_DLPF_20) < 0)
    {
      return -1;
    }
    if(writeRegister(_csPin, CONFIG, GYRO_DLPF_20) < 0)
    {
      return -2;
    }
    break;
  }
  _bandwidth = bandwidth;
  return 1;
}

int setSrd(int _csPin, uint8_t srd) //Ok!
{
  _useSPIHS = false;

  if(writeRegister(_csPin, SMPDIV, 19) < 0)
  {
    return -1;
  }
  if(srd > 9)
  {
    if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_PWR_DOWN) < 0)
    {
      return -2;
    }
    delay(100);
    if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_CNT_MEAS_1) < 0)
    {
      return -3;
    }
    delay(100);
    readAK8963Registers(_csPin, AK8963_HXL, 7, _buffer);
  }
  else
  {
    if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_PWR_DOWN) < 0)
    {
      return -2;
    }
    delay(100);
    if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_CNT_MEAS_2) < 0)
    {
      return -3;
    }
    delay(100);
    readAK8963Registers(_csPin, AK8963_HXL, 7, _buffer);
  }
  if(writeRegister(_csPin, SMPDIV, srd) < 0)
  {
    return -4;
  }
  _srd = srd;
  return 1;
}

int setGyroRange(int _csPin, uint8_t range) //Ok!
{
  _useSPIHS = false;
  switch (range)
  {
  case GYRO_RANGE_2000DPS:
    if(writeRegister(_csPin, GYRO_CONFIG, GYRO_FS_SEL_2000DPS) < 0)
    {
      return -1;
    }
    _gyroScale = 2000.0f / 32767.5f * _d2r;
    break;
  
  case GYRO_RANGE_250DPS:
    if(writeRegister(_csPin, GYRO_CONFIG, GYRO_FS_SEL_250DPS) < 0)
    {
      return -1;
    }
    _gyroScale = 250.0f / 32767.5f * _d2r;
    break;
  }
  _gyroRange = range;
  return 1;
}

int calibrationGyro(int _csPin) //Ok!
{
  if(setGyroRange(_csPin, GYRO_RANGE_250DPS) < 0)
  {
    return -1;
  }
  if(setDlpfBandwidth(_csPin, DLPF_BANDWIDTH_20HZ) < 0)
  {
    return -2;
  }
  if(setSrd(_csPin, 19) < 0)
  {
    return -3;
  }

  _gxbD = 0;
  _gybD = 0;
  _gzbD = 0;

  for(size_t i = 0; i < _numSamples; i++)
  {
    checkSensor(_csPin);
    _gxbD += (_gx + _gxb) / ((double) _numSamples);
    _gybD += (_gy + _gyb) / ((double) _numSamples);
    _gzbD += (_gz + _gzb) / ((double) _numSamples);
    delay(20);
  }

  _gxb = (float) _gxbD;
  _gyb = (float) _gybD;
  _gzb = (float) _gzbD;

  if(setGyroRange(_csPin, _gyroRange) < 0)
  {
    return -4;
  }
  if(setDlpfBandwidth(_csPin, _bandwidth) < 0)
  {
    return -5;
  }
  if(setSrd(_csPin, _srd) < 0)
  {
    return -6;
  }

  return 1;
}

int whoAmI(int _csPin) //Ok!
{
  if(readRegisters(_csPin, WHO_AM_I, 1, _buffer) < 0)
  {
    return -1;
  }

  return _buffer[0];
}

int whoAmIAK8963(int _csPin) //Ok!
{
  if(readAK8963Registers(_csPin, AK8963_WHO_AM_I, 1, _buffer) < 0)
  {
    return -1;
  }
  return _buffer[0];
}

int initSensor(int _csPin) //Ok!
{
  if(_useSPIHS)
  {
    _useSPIHS = false;
    pinMode(_csPin, OUTPUT);
    digitalWrite(_csPin, HIGH);
    SPI.begin();
  }
  if(writeRegister(_csPin, PWR_MGMNT_1, CLOCK_SEL_PLL) < 0)
  {
    return -1;
  }
  if(writeRegister(_csPin, USER_CTRL, I2C_MST_EN) < 0)
  {
    return -2;
  }
  if(writeRegister(_csPin, I2C_MST_CTRL, I2C_MST_CLK) < 0)
  {
    return -3;
  }
  
  writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_PWR_DOWN);
  writeRegister(_csPin, PWR_MGMNT_1, PWR_RESET);
  delay(1);
  writeAK8963Register(_csPin, AK8963_CNTL2, AK8963_RESET);
  
  if(writeRegister(_csPin, PWR_MGMNT_1, CLOCK_SEL_PLL) < 0)
  {
    return -4;
  }
  if((whoAmI(_csPin) != 113) && (whoAmI(_csPin) != 115))
  {
    return -5;
  }
  if(writeRegister(_csPin, PWR_MGMNT_2, SEN_ENABLE) < 0)
  {
    return -6;
  }
  if(writeRegister(_csPin, ACCEL_CONFIG, ACCEL_FS_SEL_16G) < 0)
  {
    return -7;
  }

  _accelScale = G * 16.0f / 32767.5f;
  _accelRange = ACCEL_RANGE_16G;

  if (writeRegister(_csPin, GYRO_CONFIG, GYRO_FS_SEL_2000DPS) < 0)
  {
    return -8;
  }
  
  _gyroScale = 2000.0f / 32767.5f * _d2r;
  _gyroRange = GYRO_RANGE_2000DPS;

  if(writeRegister(_csPin, ACCEL_CONFIG_2, ACCEL_DLPF_184) < 0)
  {
    return -9;
  }
  if(writeRegister(_csPin, CONFIG, GYRO_DLPF_184) < 0)
  {
    return -10;
  }
  
  _bandwidth = DLPF_BANDWIDTH_184HZ;

  if(writeRegister(_csPin, SMPDIV, 0x00) < 0)
  {
    return -11;
  }

  _srd = 0;

  if(writeRegister(_csPin, USER_CTRL, I2C_MST_EN) < 0)
  {
    return -12;
  }
  if(writeRegister(_csPin, I2C_MST_CTRL, I2C_MST_CLK) < 0)
  {
    return -13;
  }
  if(whoAmIAK8963(_csPin) != 72)
  {
    return -14;
  }
  if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_PWR_DOWN) < 0)
  {
    return -15;
  }

  delay(100);

  if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_FUSE_ROM) < 0)
  {
    return -16;
  }

  delay(100);
  readAK8963Registers(_csPin, AK8963_ASA, 3, _buffer);
  _magScaleX = ((((float) _buffer[0]) - 128.0f) / (256.0f) + 1.0f) * 4912.0f / 32760.0f;
  _magScaleY = ((((float) _buffer[1]) - 128.0f) / (256.0f) + 1.0f) * 4912.0f / 32760.0f;
  _magScaleZ = ((((float) _buffer[2]) - 128.0f) / (256.0f) + 1.0f) * 4912.0f / 32760.0f;

  if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_PWR_DOWN) < 0)
  {
    return -17;
  }

  delay(100);

  if(writeAK8963Register(_csPin, AK8963_CNTL1, AK8963_CNT_MEAS_2) < 0)
  {
    return -18;
  }

  delay(100);

  if(writeRegister(_csPin, PWR_MGMNT_1, CLOCK_SEL_PLL) < 0)
  {
    return -19;
  }

  readAK8963Registers(_csPin, AK8963_HXL, 7, _buffer);

  if (calibrationGyro(_csPin) < 0)
  {
    return -20;
  }
  
  Serial.print("Sensor OK! ");
  return 1;
}

/* old sensor prog
char nunchuck_decode_byte(char x)
{
  x = (x ^ 0x17) + 0x17;
  return x;
}

static uint8_t nunchuck_buf[6];

void nunchuck_send_request()
{
  Wire.beginTransmission(0x52);
  Wire.write(0x00);
  Wire.endTransmission();
}

int nunchuck_get_data()
{
  int cnt = 0;
  Wire.requestFrom (0x52, 6);
  while (Wire.available())
  {
    nunchuck_buf[cnt] = nunchuck_decode_byte(Wire.read());
    cnt ++;
  }
  nunchuck_send_request();
  if(cnt >= 5)
  {
    return 1;
  }
  return 0;
}

int nunchuck_accel_x()
{
  return nunchuck_buf[2];
}*/

void checkServo_y() //Ok!
{
  if(loop_cnt > 100)
  {
    checkSensor(_csPin);//nunchuck_get_data();
    float tilt = 245 - _ax; //float tilt = 245 - nunchuck_accel_x();
    tilt = (tilt - 65) * 1.5;
    pulseWidth_Y = (tilt * 9) + minPulse;
    loop_cnt = 0;
  }
  loop_cnt ++;
}

void updateServo_Y() //Ok!
{
  checkServo_y(); //add this to option
  if(millis()-lastPulse_Y >= refreshTime)
  {
      digitalWrite(servoPin_Y, HIGH);
      delayMicroseconds(pulseWidth_Y);
      digitalWrite(servoPin_Y, LOW);
      lastPulse_Y = millis();
  }
}

void setup() //Ok!
{
  Serial.begin(115200);

  servoARM.attach(servoPin_Y);

  pinMode(servoPin_Y, OUTPUT);
  pinMode(sclPin, OUTPUT);
  pinMode(sdaPin, OUTPUT);
  pinMode(adoPin, OUTPUT);
  pinMode(ncsPin, OUTPUT);

  pulseWidth_Y = minPulse;

  if(initSensor(ncsPin) > 0)
  {
    Serial.print("Sensor and servo ready.\n");
  }
  else
  {
    Serial.print("Check sensor connection.\n");
    Serial.println(initSensor(ncsPin));
    delay(5000);
    setup();
  }
  delay(1000);
}

void loop() //Ok!
{
  //checkSensor(ncsPin);
  updateServo_Y();
  Serial.print(_ax);
  Serial.println(pulseWidth_Y);
  delay(100);
}
