#include <SPI.h>   // Pin 13 - SCL, Pin 12 - ADO, Pin 11 - SDA, Pin 10/9/.. - NCs1/NCs2/..

int LCD_loop_counter; //в конечной версии не нужные переменные
int LCD_heading_buffer, LCD_pitch_buffer, LCD_roll_buffer; //в конечной версии не нужные переменные

/* Регистры датчика */
#define RESET_REG                           0x00 //флаг для сброса/перезагрузки сенсора
#define INT_20_MHz                          0x00 //частота 20 МГц
#define GYRO_FS_500DPS                      0x08 //выбор шкалы гироскопа 500dps
#define ACCEL_FS_8G                         0x10 //выбор шкалы акселерометра на 8g
#define GYRO_CONF                           0x1B //конфигурации гироскопа
#define ACCEL_CONF                          0x1C //конфигурации акселерометра
#define ACCEL_XOUT_H                        0x3B //верхнего байта данных оси X акселерометра
#define GYRO_XOUT_H                         0x43 //верхнего байта данных оси X гироскопа
#define I2C_MST_EN                          0x20 //флаг настройки активации использования внутреннего режима I2C-ведущего интерфейса
#define MPU9250_I2C_master_enable           0x6A //активация внутреннего режима I2C-ведущего интерфейса
#define MPU9250_PWR_MGMT_1                  0x6B //управление питанием
#define MPU9250_Interface_bypass_mux_enable 0x37 //интерфейс пропускания мультиплексора
#define I2C_MST_CLK                         0x0D //флаг настройки скорости I2C-ведущего интерфейса
#define I2C_MST_CTRL                        0x24 //настройка I2C-ведущего интерфейса
#define I2C_SLV0_ADDR                       0x25 //адрес I2C-ведомого датчика
#define I2C_SLV0_REG                        0x26 //адрес регистра ведомого датчика откуда начинать передачу данных
#define I2C_SLV0_CTRL                       0x27 //активация чтения данных из регистров ведомого датчика и запись в регистры начиная с EXT_SENS_DATA_00
#define EXT_SENS_DATA_00                    0x49 //начальный регистр хранения данных из магнетометра
#define I2C_SLV0_DO                         0x63 //вывод данных, когда датчик настроен на запись
#define I2C_SLV0_EN                         0x80 //флаг активации ведомого

/* Регистры магнетометра */
#define AK8963_I2C_address                  0x0C //адрес магнетометра
#define AK8963_cntrl_reg_1                  0x0A //управление магнетометром
#define AK8963_status_reg_1                 0x02 //статус магнетометра
#define AK8963_data_ready_mask              0x01 //маска готовности данных
#define AK8963_overflow_mask                0x08 //маска переполнения магнетометра
#define AK8963_data                         0x03 //данные магнетометра
#define AK8963_fuse_ROM                     0x10 //флаг энергонезависимой памяти магнетометра
#define AK8963_PWR_DWN                      0x00 //перезагрузка/сброс магнетометра
#define AK8963_ACCESS_FUSE_ROM              0x0F //доступ в энергонезависимой памяти магнетометра
#define AK8963_16_BIT_MODE                  0x10 //режим 16-битный работы
#define AK8963_MODE_2                       0x06 //режим работы магнетометра

#define Frequency 125 //частота
#define Sensitivity 65.5 //чувствительность
#define Sensor_to_deg 1 / (Sensitivity * Frequency) //преобразование из данных сенсора в градусы
#define Sensor_to_rad Sensor_to_deg * DEG_TO_RAD //преобразование из градусов в радианы

#define Loop_time 1000000 / Frequency //время цикла
long    Loop_start; //время старта цикла

#define Sensors 4 //количество датчиков
int AD1 = 7;
int AD2 = 8;
int AD4 = 9; //присваиваем 9 пин значению AD4 (тело)
int AD5 = 10; //присваиваем 10 пин значению AD5 (голова)
#define xyz 3 //количество осей для считывания регистров датчика
#define x 0 //ось x в массиве
#define y 1 //ось y в массиве
#define z 2 //ось z в массиве

int MPU9250_Add[Sensors] = {AD1, AD2, AD4, AD5}; //массив выводов подключенных к датчикам

int Gyro_x[Sensors], Gyro_y[Sensors], Gyro_z[Sensors]; //для осей гироскопа
long Gyro_x_cal[Sensors], Gyro_y_cal[Sensors], Gyro_z_cal[Sensors]; //для калибровки осей гироскопа
float Gyro_pitch[Sensors], Gyro_roll[Sensors], Gyro_yaw[Sensors]; //для "тангажа", "крена" и "рыскания" гироскопа
float Gyro_pitch_output[Sensors], Gyro_roll_output[Sensors]; //для выходные расчитанные данные гироскопа

long Accel_x[Sensors], Accel_y[Sensors], Accel_z[Sensors], Accel_total_vector[Sensors]; //для осей акселерометра
float Accel_pitch[Sensors], Accel_roll[Sensors]; //для "тангажа" и "крена" акселерометра

float Declination = -12.14; //географическое склонение на месте http://www.magnetic-declination.com/

bool Record_data = false; //для калибровки магнетометра (удалить)

int Heading[Sensors]; //для значения направления сенсора

int Mag_x[Sensors], Mag_y[Sensors], Mag_z[Sensors]; //для осей магнетометра
float Mag_x_dampened[Sensors], Mag_y_dampened[Sensors], Mag_z_dampened[Sensors]; //"затухание" осей магнетометра
float Mag_x_hor[Sensors], Mag_y_hor[Sensors]; //расчетные значения наклона
float Mag_pitch[Sensors], Mag_roll[Sensors]; //для "тангажа" и "крена" магнетометра 

int Mag_x_offset[Sensors], Mag_y_offset[Sensors], Mag_z_offset[Sensors]; //значения смещения по осям магнетометра (взяты из калибровки каждого сенсора)
float Mag_x_scale[Sensors], Mag_y_scale[Sensors], Mag_z_scale[Sensors]; //калибровочные данные по осям магнетометра
float ASAX[Sensors], ASAY[Sensors], ASAZ[Sensors]; //значения чувствительности по осям магнетометра

const uint8_t SPI_READ = 0x80; //флаг для чтения регистров сенсора
const uint32_t SPI_LS_CLOCK = 1000000; //частота работы SPI интерфейса

void SPI_write(int sensor, int address, int data) //запись в сенсор по интерфейсу SPI
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3)); //начало транзакции
  digitalWrite(sensor, LOW); //подключение конкретного сенсора к шине
  SPI.transfer(address); //адрес регистра для записи
  SPI.transfer(data); //передача данных для записи
  digitalWrite(sensor, HIGH); //отключение сенсора от шины
  SPI.endTransaction(); //конец транзакции
}

void SPI_read(int sensor, int address, int count, int* data) //чтение по SPI интерфейсу
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3)); //начало транзакции
  digitalWrite(sensor, LOW); //подключение конкретного сенсора к шине
  SPI.transfer(address | SPI_READ); //сообщение сенсору о начальном регистре для чтения
  
  for (int registers = 0; registers < count; registers ++) //перебор всех считываемых регистров
  {
    data[registers] = SPI.transfer(0x00); //запись в массив данных с регистра сенсора
  }
  
  digitalWrite(sensor, HIGH); //отключение сенсора от шины
  SPI.endTransaction(); //конец транзакции
}

void read_magnetometer_registers(int sensor, int subAddress, int count, int* dest) //чтение данных из регистров магнетометра
{
  SPI_write(sensor, I2C_SLV0_ADDR, AK8963_I2C_address | SPI_READ); //обращение к магнетометру через сенсор с меткой чтения
  SPI_write(sensor, I2C_SLV0_REG, subAddress); //доступ к начальному регистру
  SPI_write(sensor, I2C_SLV0_CTRL, I2C_SLV0_EN | count); //сообщение о количестве считываемых регистров
  
  SPI_read(sensor, EXT_SENS_DATA_00, count, dest); //считывание данных из регистров в массив
}

void write_magnetometer_register(int sensor, int subAddress, int data) //запись в регистр магнетометра
{
  SPI_write(sensor, I2C_SLV0_ADDR, AK8963_I2C_address); //обращение к магнетометру через сенсор
  SPI_write(sensor, I2C_SLV0_REG, subAddress); //доступ к регистру магнетометра
  SPI_write(sensor, I2C_SLV0_DO, data); //передача данных в регистр
  SPI_write(sensor, I2C_SLV0_CTRL, I2C_SLV0_EN | 1); //конец обращения к регистру и магнетометру
}

void read_magnetometer(int sensor, int sensor_number) //считывание данных с магнетометра
{
  int mag_x, mag_y, mag_z; //буфер для вычислений по осям
  int status_reg_2; //буфер для записи статуса из регистра магнетометра
  int count = 7; //счетчик
  int dest[count]; //массив для записи данных из регистров магнетометра

  read_magnetometer_registers(sensor, AK8963_data, count, dest); //чтение данных из регистров магнетометра
  
  mag_x = (dest[0] | dest[1] << 8) * ASAX[sensor_number]; //данные по оси X с поправкой на чувствительность
  mag_y = (dest[2] | dest[3] << 8) * ASAY[sensor_number]; //данные по оси Y с поправкой на чувствительность
  mag_z = (dest[4] | dest[5] << 8) * ASAZ[sensor_number]; //данные по оси Z с поправкой на чувствительность
  status_reg_2 = dest[6]; //запись в буфер значения статуса магнетометра

  if (!(status_reg_2 & AK8963_overflow_mask)) //когда датчик переполнен
  {
    Mag_x[sensor_number] = (mag_x - Mag_x_offset[sensor_number]) * Mag_x_scale[sensor_number]; //вычисление значения оси X магнетометра со смещением
    Mag_y[sensor_number] = (mag_y - Mag_y_offset[sensor_number]) * Mag_y_scale[sensor_number]; //вычисление значения оси Y магнетометра со смещением
    Mag_z[sensor_number] = (mag_z - Mag_z_offset[sensor_number]) * Mag_z_scale[sensor_number]; //вычисление значения оси Z магнетометра со смещением
  }
}

void configure_magnetometer(int sensor, int sensor_number) //конфигурирование магнетометра
{
  SPI_write(sensor, MPU9250_I2C_master_enable, I2C_MST_EN); //запись в сенсор - использовать порт I2C master для управления внешним датчиком магнетометра

  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_16_BIT_MODE | AK8963_ACCESS_FUSE_ROM); //использовать 16 битный режим работы с записью в память
  
  delay(100); //ожидание

  int ASA[xyz]; //массив для значений чувствительности
  read_magnetometer_registers(sensor, AK8963_fuse_ROM, xyz, ASA); //чтение с магнетометра значений чувствительности

  ASAX[sensor_number] = (ASA[x] - 128) * 0.5 / 128 + 1; //расчет значения корректировки по чувствительности оси X магнетометра
  ASAY[sensor_number] = (ASA[y] - 128) * 0.5 / 128 + 1; //расчет значения корректировки по чувствительности оси Y магнетометра
  ASAZ[sensor_number] = (ASA[z] - 128) * 0.5 / 128 + 1; //расчет значения корректировки по чувствительности оси Z магнетометра

  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_PWR_DWN); //перезапуск/сброс магнетометра
  
  delay(100); //ожидание

  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_16_BIT_MODE | AK8963_MODE_2); //использовать 16 битный 2 режим работы с постоянным измерением
  
  delay(100); //ожидание
}

void calibrate_magnetometer(int sensor, int sensor_number)
{
  int mag_x, mag_y, mag_z; //буфер для вычислений по осям
  int status_reg_2; //буфер для записи статуса из регистра магнетометра
  int count = 7; //счетчик
  int dest[count]; //массив для записи данных из регистров магнетометра

  int mag_x_min =  32767;                                         // Raw data extremes
  int mag_y_min =  32767;
  int mag_z_min =  32767;
  int mag_x_max = -32768;
  int mag_y_max = -32768;
  int mag_z_max = -32768;

  float chord_x,  chord_y,  chord_z;                              // Used for calculating scale factors
  float chord_average;

  // ----- Display calibration message
  Serial.print("Rotate Compass");                                    // Print text to screen

  // ----- Record min/max XYZ compass readings
  for (int counter = 0; counter < 16000 ; counter ++)             // Run this code 16000 times
  {
    Loop_start = micros();                                        // Start loop timer
    if (counter % 1000 == 0)Serial.print(".");                    // Print a dot on the LCD every 1000 readings
    {
      // ----- Read data from each axis (LSB,MSB)
      read_magnetometer_registers(sensor, AK8963_data, count, dest); //чтение данных из регистров магнетометра
      
      mag_x = (dest[0] | dest[1] << 8) * ASAX[sensor_number];            // Combine LSB,MSB X-axis, apply ASA corrections
      mag_y = (dest[2] | dest[3] << 8) * ASAY[sensor_number];            // Combine LSB,MSB Y-axis, apply ASA corrections
      mag_z = (dest[4] | dest[5] << 8) * ASAZ[sensor_number];            // Combine LSB,MSB Z-axis, apply ASA corrections
      status_reg_2 = dest[6];                                 // Read status and signal data read

      // ----- Validate data
      if (!(status_reg_2 & AK8963_overflow_mask))                 // Check HOFL flag in ST2[3]
      {
        // ----- Find max/min xyz values
        mag_x_min = min(mag_x, mag_x_min);
        mag_x_max = max(mag_x, mag_x_max);
        mag_y_min = min(mag_y, mag_y_min);
        mag_y_max = max(mag_y, mag_y_max);
        mag_z_min = min(mag_z, mag_z_min);
        mag_z_max = max(mag_z, mag_z_max);
      }
    }
    delay(4);                                                     // Time interval between magnetometer readings
    Record_data = true;
  }
  Serial.println();

  // ----- Calculate hard-iron offsets
  Mag_x_offset[sensor_number] = (mag_x_max + mag_x_min) / 2;                     // Get average magnetic bias in counts
  Mag_y_offset[sensor_number] = (mag_y_max + mag_y_min) / 2;
  Mag_z_offset[sensor_number] = (mag_z_max + mag_z_min) / 2;

  // ----- Calculate soft-iron scale factors
  chord_x = ((float)(mag_x_max - mag_x_min)) / 2;                 // Get average max chord length in counts
  chord_y = ((float)(mag_y_max - mag_y_min)) / 2;
  chord_z = ((float)(mag_z_max - mag_z_min)) / 2;

  chord_average = (chord_x + chord_y + chord_z) / 3;              // Calculate average chord length

  Mag_x_scale[sensor_number] = chord_average / chord_x;                          // Calculate X scale factor
  Mag_y_scale[sensor_number] = chord_average / chord_y;                          // Calculate Y scale factor
  Mag_z_scale[sensor_number] = chord_average / chord_z;                          // Calculate Z scale factor

  // ----- Record magnetometer offsets
  /*
      When active this feature sends the magnetometer data
      to the Serial Monitor then halts the program.
  */
  if (Record_data == true)
  {
    Serial.print("Sensor - "); Serial.print(sensor_number); Serial.println(":");
    Serial.println("");
    // ----- Display data extremes
    Serial.print("XYZ Max/Min: ");
    Serial.print(mag_x_min); Serial.print("\t");
    Serial.print(mag_x_max); Serial.print("\t");
    Serial.print(mag_y_min); Serial.print("\t");
    Serial.print(mag_y_max); Serial.print("\t");
    Serial.print(mag_z_min); Serial.print("\t");
    Serial.println(mag_z_max);
    Serial.println("");

    // ----- Display hard-iron offsets
    Serial.print("Hard-iron: ");
    Serial.print(Mag_x_offset[sensor_number]); Serial.print("\t");
    Serial.print(Mag_y_offset[sensor_number]); Serial.print("\t");
    Serial.println(Mag_z_offset[sensor_number]);
    Serial.println("");

    // ----- Display soft-iron scale factors
    Serial.print("Soft-iron: ");
    Serial.print(Mag_x_scale[sensor_number]); Serial.print("\t");
    Serial.print(Mag_y_scale[sensor_number]); Serial.print("\t");
    Serial.println(Mag_z_scale[sensor_number]);
    Serial.println("");

    // ----- Display fuse ROM values
    Serial.print("ASA: ");
    Serial.print(ASAX[sensor_number]); Serial.print("\t");
    Serial.print(ASAY[sensor_number]); Serial.print("\t");
    Serial.println(ASAZ[sensor_number]);
    Serial.println("============================================================================================");

    // ----- Halt program
    // while (true);                                       // Wheelspin ... program halt
  }
}

void config_gyro(int sensor) //конфигурирование гироскопа
{
  SPI_write(sensor, MPU9250_PWR_MGMT_1, INT_20_MHz); //запись в сенсор - использовать внутренний генратор на 20МГц
  SPI_write(sensor, ACCEL_CONF, ACCEL_FS_8G); //запись в сенсор конфигурациz шкалы акселерометра на 8g
  SPI_write(sensor, GYRO_CONF, GYRO_FS_500DPS); //запись в сенсор конфигурация шкалы гироскопа на 500dps
}

void read_mpu_6050_data(int sensor, int sensor_number) //чтение данных из сенсора
{
  int dest[14]; //массив для хранения данных

  SPI_read(sensor, ACCEL_XOUT_H, 14, dest); //чтение данных с 14 регистров датчика
  
  Accel_x[sensor_number] = dest[0] << 8 | dest[1]; //объединение двух байт (верхнего и нижнего) значения оси X акселерометра
  Accel_y[sensor_number] = dest[2] << 8 | dest[3]; //объединение двух байт (верхнего и нижнего) значения оси Y акселерометра
  Accel_z[sensor_number] = dest[4] << 8 | dest[5]; //объединение двух байт (верхнего и нижнего) значения оси Z акселерометра

  Gyro_x[sensor_number] = dest[8] << 8 | dest[9]; //объединение двух байт (верхнего и нижнего) значения оси X гироскопа
  Gyro_y[sensor_number] = dest[10] << 8 | dest[11]; //объединение двух байт (верхнего и нижнего) значения оси Y гироскопа
  Gyro_z[sensor_number] = dest[12] << 8 | dest[13]; //объединение двух байт (верхнего и нижнего) значения оси Z гироскопа
}

void calibrate_gyro(int sensor, int sensor_number) //калибровка гироскопа
{
  for (int counter = 0; counter < 2000 ; counter ++) //2000 циклов для калибровки
  {
    Loop_start = micros(); //запись времени старта калибровки

    read_mpu_6050_data(sensor, sensor_number); //чтение данных сенсора
    Gyro_x_cal[sensor_number] += Gyro_x[sensor_number]; //сложение значений оси X гироскопа
    Gyro_y_cal[sensor_number] += Gyro_y[sensor_number]; //сложение значений оси Y гироскопа
    Gyro_z_cal[sensor_number] += Gyro_z[sensor_number]; //сложение значений оси Z гироскопа
    while (micros() - Loop_start < Loop_time); //ожидание
  }

  Gyro_x_cal[sensor_number] /= 2000; //вычисление среднего значения оси X
  Gyro_y_cal[sensor_number] /= 2000; //вычисление среднего значения оси Y
  Gyro_z_cal[sensor_number] /= 2000; //вычисление среднего значения оси Z
}

void setup() //старт программы
{
  Serial.begin(115200); //инициализация серийного интерфейса ардуино со скоростью 115200 бод
  SPI.begin(); //инициализация работы режима SPI

  pinMode(AD1, OUTPUT); //устанавливаем режим работы вывода как "выход"
  pinMode(AD2, OUTPUT); //устанавливаем режим работы вывода как "выход"
  pinMode(AD4, OUTPUT); //устанавливаем режим работы вывода как "выход"
  pinMode(AD5, OUTPUT); //устанавливаем режим работы вывода как "выход"
  digitalWrite(AD1, HIGH); //устанавливаем значение на выводе как "1"
  digitalWrite(AD2, HIGH); //устанавливаем значение на выводе как "1"
  digitalWrite(AD4, HIGH); //устанавливаем значение на выводе как "1"
  digitalWrite(AD5, HIGH); //устанавливаем значение на выводе как "1"

  Serial.print(" Working pragramm"); //вывод информации на серийный интерфейс
  Serial.println(" V4.01 Multi_SPI+Calibrate Mag"); //вывод информации на серийный интерфейс
  
  for (int sensor_number = 0; sensor_number < sizeof(MPU9250_Add)/2; sensor_number ++) //цикл для перебора всех датчиков
  {
    // Serial.println("Calibrate sensor "); //вывод информации на серийный интерфейс

    digitalWrite(MPU9250_Add[sensor_number], LOW); //подаем на вывод низкий уровень сигнала, даем доступ к датчику

    SPI_write(MPU9250_Add[sensor_number],MPU9250_PWR_MGMT_1, RESET_REG); //запись в регистр, режим работы с внутренним 20Гц генератором

    // Serial.println("Config magnetometer "); //вывод информации на серийный интерфейс

    configure_magnetometer(MPU9250_Add[sensor_number], sensor_number); //конфигурация магнетометра датчика

    // Serial.println("Calibrate magnetometer "); //вывод информации на серийный интерфейс

    calibrate_magnetometer(MPU9250_Add[sensor_number], sensor_number);

    // Serial.println("Configurate gyroscope "); //вывод информации на серийный интерфейс

    config_gyro(MPU9250_Add[sensor_number]); //конфигурация гироскопа

    // Serial.println("Calibrate gyroscope "); //вывод информации на серийный интерфейс

    calibrate_gyro(MPU9250_Add[sensor_number], sensor_number); //калибровка гироскопа

    digitalWrite(MPU9250_Add[sensor_number], HIGH); //подаем на вывод высокий уровень сигнала, закрываем доступ к датчику

    // Serial.println("Calibrate complete."); //вывод информации на серийный интерфейс
  }
}

void loop() //тело программы
{

}

