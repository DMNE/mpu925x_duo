#include <SPI.h>   // Pin 13 - SCL, Pin 12 - ADO, Pin 11 - SDA, Pin 10/9/.. - NCs1/NCs2/..
#include <GyverButton.h> //подключение библиотеки Гайвера для кнопок
#include <ServoSmooth.h> //подключение библиотеки Гайвера для плавного управления серво

int LCD_loop_counter; //в конечной версии не нужные переменные
int LCD_heading_buffer, LCD_pitch_buffer, LCD_roll_buffer; //в конечной версии не нужные переменные

/* Регистры датчика */
#define RESET_REG                           0x00 //флаг для сброса/перезагрузки сенсора
#define INT_20_MHz                          0x00 //частота 20 МГц
#define GYRO_FS_500DPS                      0x08 //выбор шкалы гироскопа 500dps
#define ACCEL_FS_8G                         0x10 //выбор шкалы акселерометра на 8g
#define GYRO_CONF                           0x1B //конфигурации гироскопа
#define ACCEL_CONF                          0x1C //конфигурации акселерометра
#define ACCEL_XOUT_H                        0x3B //верхнего байта данных оси X акселерометра
#define GYRO_XOUT_H                         0x43 //верхнего байта данных оси X гироскопа
#define I2C_MST_EN                          0x20 //флаг настройки активации использования внутреннего режима I2C-ведущего интерфейса
#define MPU9250_I2C_master_enable           0x6A //активация внутреннего режима I2C-ведущего интерфейса
#define MPU9250_PWR_MGMT_1                  0x6B //управление питанием
#define MPU9250_Interface_bypass_mux_enable 0x37 //интерфейс пропускания мультиплексора
#define I2C_MST_CLK                         0x0D //флаг настройки скорости I2C-ведущего интерфейса
#define I2C_MST_CTRL                        0x24 //настройка I2C-ведущего интерфейса
#define I2C_SLV0_ADDR                       0x25 //адрес I2C-ведомого датчика
#define I2C_SLV0_REG                        0x26 //адрес регистра ведомого датчика откуда начинать передачу данных
#define I2C_SLV0_CTRL                       0x27 //активация чтения данных из регистров ведомого датчика и запись в регистры начиная с EXT_SENS_DATA_00
#define EXT_SENS_DATA_00                    0x49 //начальный регистр хранения данных из магнетометра
#define I2C_SLV0_DO                         0x63 //вывод данных, когда датчик настроен на запись
#define I2C_SLV0_EN                         0x80 //флаг активации ведомого

/* Регистры магнетометра */
#define AK8963_I2C_address                  0x0C //адрес магнетометра
#define AK8963_cntrl_reg_1                  0x0A //управление магнетометром
#define AK8963_status_reg_1                 0x02 //статус магнетометра
#define AK8963_data_ready_mask              0x01 //маска готовности данных
#define AK8963_overflow_mask                0x08 //маска переполнения магнетометра
#define AK8963_data                         0x03 //данные магнетометра
#define AK8963_fuse_ROM                     0x10 //флаг энергонезависимой памяти магнетометра
#define AK8963_PWR_DWN                      0x00 //перезагрузка/сброс магнетометра
#define AK8963_ACCESS_FUSE_ROM              0x0F //доступ в энергонезависимой памяти магнетометра
#define AK8963_16_BIT_MODE                  0x10 //режим 16-битный работы
#define AK8963_MODE_2                       0x06 //режим работы магнетометра

#define Frequency 125 //частота
#define Sensitivity 65.5 //чувствительность
#define Sensor_to_deg 1 / (Sensitivity * Frequency) //преобразование из данных сенсора в градусы
#define Sensor_to_rad Sensor_to_deg * DEG_TO_RAD //преобразование из градусов в радианы

#define Loop_time 1000000 / Frequency //время цикла
long    Loop_start; //время старта цикла

#define Sensors 2 //количество датчиков
#define Sensor_head 1 //датчик "головы"
#define Sensor_body 0 //датчик "тела"
int AD4 = 9; //присваиваем 9 пин значению AD4 (тело)
int AD5 = 10; //присваиваем 10 пин значению AD5 (голова)
#define xyz 3 //количество осей для считывания регистров датчика
#define x 0 //ось x в массиве
#define y 1 //ось y в массиве
#define z 2 //ось z в массиве

int MPU9250_Add[Sensors] = {AD4, AD5}; //массив выводов подключенных к датчикам

int Gyro_x[Sensors], Gyro_y[Sensors], Gyro_z[Sensors]; //для осей гироскопа
long Gyro_x_cal[Sensors], Gyro_y_cal[Sensors], Gyro_z_cal[Sensors]; //для калибровки осей гироскопа
float Gyro_pitch[Sensors], Gyro_roll[Sensors], Gyro_yaw[Sensors]; //для "тангажа", "крена" и "рыскания" гироскопа
float Gyro_pitch_output[Sensors], Gyro_roll_output[Sensors]; //для выходные расчитанные данные гироскопа

long Accel_x[Sensors], Accel_y[Sensors], Accel_z[Sensors], Accel_total_vector[Sensors]; //для осей акселерометра
float Accel_pitch[Sensors], Accel_roll[Sensors]; //для "тангажа" и "крена" акселерометра

float Declination = -12.14; //географическое склонение на месте http://www.magnetic-declination.com/

//bool Record_data = false; //для калибровки магнетометра (удалить)

int Heading[Sensors]; //для значения направления сенсора

int Mag_x[Sensors], Mag_y[Sensors], Mag_z[Sensors]; //для осей магнетометра
float Mag_x_dampened[Sensors], Mag_y_dampened[Sensors], Mag_z_dampened[Sensors]; //"затухание" осей магнетометра
float Mag_x_hor[Sensors], Mag_y_hor[Sensors]; //расчетные значения наклона
float Mag_pitch[Sensors], Mag_roll[Sensors]; //для "тангажа" и "крена" магнетометра 

int Mag_x_offset[Sensors] = {0, 0}, Mag_y_offset[Sensors] = {0, 0}, Mag_z_offset[Sensors] = {0, 0}; //значения смещения по осям магнетометра (взяты из калибровки каждого сенсора)
float Mag_x_scale[Sensors] = {0, 0}, Mag_y_scale[Sensors] = {0, 0}, Mag_z_scale[Sensors] = {0, 0}; //калибровочные данные по осям магнетометра
float ASAX[Sensors] = {0.50, 0.50}, ASAY[Sensors] = {0.50, 0.50}, ASAZ[Sensors] = {0.50, 0.50}; //значения чувствительности по осям магнетометра
//-5182	41	41    	0,34	42,74	42,51   	0,50	0,50	0,50 //59  302 -200    1.04  1.01  0.96    1.18  1.18  1.13
//-4817	43	-50   	0,34	38,41	32,22   	0,50	0,50	0,50 //-43 169 -563    1.04  1.03  0.93    1.20  1.20  1.16
bool Gyro_synchronised[Sensors] = {false, false}; //синхронизация гироскопа и акселерометра

//#define Switch A0 // удалить //присваиваем пин А0 значению Switch
//long Loop_start_time; // удалить //время начала очередного цикла
//long Debug_start_time; // удалить

const uint8_t SPI_READ = 0x80; //флаг для чтения регистров сенсора
const uint32_t SPI_LS_CLOCK = 1000000; //частота работы SPI интерфейса
int buffer[7]; //буферный массив

int Calculated_heading; //вычисленное значение направление устройства
float Calc_pitch, Calc_roll; //вычисленные значения "тангажа" и "крена" устройства

ServoSmooth servoX; //серво оси X
ServoSmooth servoZ; //серво оси Y
ServoSmooth servoARM; //серво оси Z

int Servo_control_pin = 5; //пин для управения сервоприводами

int Servo_default_position = 90; //угол серво по-умолчанию
int ServoZ_default_position = 90; //угол серво Z по-умолчанию
int ServoX_default_position = 45; //угол серво X по-умолчанию
int Servo_current_heading_position; //текущая позиция "рыскания" серво
int Servo_current_pitch_position; //текущая позиция "тангажа" серво

int Servo_arm_off_position = 180; //угол серво когда устройство выключено
int Servo_arm_on_position = 0; //угол серво когда устройство включено

int Arm_button_pin = 4; //пин для подключения кнопки
bool Arm_device_on = LOW; //для фиксации включения/выключения устройства
bool Arm_device_state = LOW; //для фиксации состояния устройства
unsigned long Arm_device_time; //время последнего нажатия
GButton Arm_Button(Arm_button_pin, LOW_PULL); //привязка кнопкис указанием типа подключения

void SPI_write(int sensor, int address, int data) //запись в сенсор по интерфейсу SPI
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3)); //начало транзакции
  digitalWrite(sensor, LOW); //подключение конкретного сенсора к шине
  SPI.transfer(address); //адрес регистра для записи
  SPI.transfer(data); //передача данных для записи
  digitalWrite(sensor, HIGH); //отключение сенсора от шины
  SPI.endTransaction(); //конец транзакции
}

void SPI_read(int sensor, int address, int count, int* data) //чтение по SPI интерфейсу
{
  SPI.beginTransaction(SPISettings(SPI_LS_CLOCK, MSBFIRST, SPI_MODE3)); //начало транзакции
  digitalWrite(sensor, LOW); //подключение конкретного сенсора к шине
  SPI.transfer(address | SPI_READ); //сообщение сенсору о начальном регистре для чтения
  
  for (int registers = 0; registers < count; registers ++) //перебор всех считываемых регистров
  {
    data[registers] = SPI.transfer(0x00); //запись в массив данных с регистра сенсора
  }
  
  digitalWrite(sensor, HIGH); //отключение сенсора от шины
  SPI.endTransaction(); //конец транзакции
}

void read_magnetometer_registers(int sensor, int subAddress, int count, int* dest) //чтение данных из регистров магнетометра
{
  SPI_write(sensor, I2C_SLV0_ADDR, AK8963_I2C_address | SPI_READ); //обращение к магнетометру через сенсор с меткой чтения
  SPI_write(sensor, I2C_SLV0_REG, subAddress); //доступ к начальному регистру
  SPI_write(sensor, I2C_SLV0_CTRL, I2C_SLV0_EN | count); //сообщение о количестве считываемых регистров
  
  SPI_read(sensor, EXT_SENS_DATA_00, count, dest); //считывание данных из регистров в массив
}

void write_magnetometer_register(int sensor, int subAddress, int data) //запись в регистр магнетометра
{
  SPI_write(sensor, I2C_SLV0_ADDR, AK8963_I2C_address); //обращение к магнетометру через сенсор
  SPI_write(sensor, I2C_SLV0_REG, subAddress); //доступ к регистру магнетометра
  SPI_write(sensor, I2C_SLV0_DO, data); //передача данных в регистр
  SPI_write(sensor, I2C_SLV0_CTRL, I2C_SLV0_EN | 1); //конец обращения к регистру и магнетометру
}

void read_magnetometer(int sensor, int sensor_number) //считывание данных с магнетометра
{
  int mag_x, mag_y, mag_z; //буфер для вычислений по осям
  int status_reg_2; //буфер для записи статуса из регистра магнетометра
  int count = 7; //счетчик
  int dest[count]; //массив для записи данных из регистров магнетометра

  read_magnetometer_registers(sensor, AK8963_data, count, dest); //чтение данных из регистров магнетометра
  
  mag_x = (dest[0] | dest[1] << 8) * ASAX[sensor_number]; //данные по оси X с поправкой на чувствительность
  mag_y = (dest[2] | dest[3] << 8) * ASAY[sensor_number]; //данные по оси Y с поправкой на чувствительность
  mag_z = (dest[4] | dest[5] << 8) * ASAZ[sensor_number]; //данные по оси Z с поправкой на чувствительность
  status_reg_2 = dest[6]; //запись в буфер значения статуса магнетометра

  if (!(status_reg_2 & AK8963_overflow_mask)) //когда датчик переполнен
  {
    Mag_x[sensor_number] = (mag_x - Mag_x_offset[sensor_number]) * Mag_x_scale[sensor_number]; //вычисление значения оси X магнетометра со смещением
    Mag_y[sensor_number] = (mag_y - Mag_y_offset[sensor_number]) * Mag_y_scale[sensor_number]; //вычисление значения оси Y магнетометра со смещением
    Mag_z[sensor_number] = (mag_z - Mag_z_offset[sensor_number]) * Mag_z_scale[sensor_number]; //вычисление значения оси Z магнетометра со смещением
  }
}

void configure_magnetometer(int sensor, int sensor_number) //конфигурирование магнетометра
{
  int start_configure_magnetometer = millis(); //запись времени начала процесса конфигурации

  SPI_write(sensor, MPU9250_I2C_master_enable, I2C_MST_EN); //запись в сенсор - использовать порт I2C master для управления внешним датчиком магнетометра

  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_16_BIT_MODE | AK8963_ACCESS_FUSE_ROM); //использовать 16 битный режим работы с записью в память
  
  while (millis() - start_configure_magnetometer > 100); //ожидание

  int ASA[xyz]; //массив для значений чувствительности
  read_magnetometer_registers(sensor, AK8963_fuse_ROM, xyz, ASA); //чтение с магнетометра значений чувствительности

  ASAX[sensor_number] = (ASA[x] - 128) * 0.5 / 128 + 1; //расчет значения корректировки по чувствительности оси X магнетометра
  ASAY[sensor_number] = (ASA[y] - 128) * 0.5 / 128 + 1; //расчет значения корректировки по чувствительности оси Y магнетометра
  ASAZ[sensor_number] = (ASA[z] - 128) * 0.5 / 128 + 1; //расчет значения корректировки по чувствительности оси Z магнетометра

  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_PWR_DWN); //перезапуск/сброс магнетометра
  
  while (millis() - start_configure_magnetometer > 100); //ожидание

  write_magnetometer_register(sensor, AK8963_cntrl_reg_1, AK8963_16_BIT_MODE | AK8963_MODE_2); //использовать 16 битный 2 режим работы с постоянным измерением
  
  while (millis() - start_configure_magnetometer > 100); //ожидание
}

void config_gyro(int sensor) //конфигурирование гироскопа
{
  SPI_write(sensor, MPU9250_PWR_MGMT_1, INT_20_MHz); //запись в сенсор - использовать внутренний генратор на 20МГц
  SPI_write(sensor, ACCEL_CONF, ACCEL_FS_8G); //запись в сенсор конфигурациz шкалы акселерометра на 8g
  SPI_write(sensor, GYRO_CONF, GYRO_FS_500DPS); //запись в сенсор конфигурация шкалы гироскопа на 500dps
}

void read_mpu_6050_data(int sensor, int sensor_number) //чтение данных из сенсора
{
  int dest[14]; //массив для хранения данных

  SPI_read(sensor, ACCEL_XOUT_H, 14, dest); //чтение данных с 14 регистров датчика
  
  Accel_x[sensor_number] = dest[0] << 8 | dest[1]; //объединение двух байт (верхнего и нижнего) значения оси X акселерометра
  Accel_y[sensor_number] = dest[2] << 8 | dest[3]; //объединение двух байт (верхнего и нижнего) значения оси Y акселерометра
  Accel_z[sensor_number] = dest[4] << 8 | dest[5]; //объединение двух байт (верхнего и нижнего) значения оси Z акселерометра

  Gyro_x[sensor_number] = dest[8] << 8 | dest[9]; //объединение двух байт (верхнего и нижнего) значения оси X гироскопа
  Gyro_y[sensor_number] = dest[10] << 8 | dest[11]; //объединение двух байт (верхнего и нижнего) значения оси Y гироскопа
  Gyro_z[sensor_number] = dest[12] << 8 | dest[13]; //объединение двух байт (верхнего и нижнего) значения оси Z гироскопа
}

void calibrate_gyro(int sensor, int sensor_number) //калибровка гироскопа
{
  Serial.print("Calibrating gyro"); //вывод сообщения на экран

  for (int counter = 0; counter < 2000 ; counter ++) //2000 циклов для калибровки
  {
    Loop_start = micros(); //запись времени старта калибровки

    if (counter % 125 == 0) //если счетчик делится на 125 без остатка (каждые 125 циклов)
    {
      Serial.print("."); //вывод на экран точки "полоски загрузки", 16 точек
    }

    read_mpu_6050_data(sensor, sensor_number); //чтение данных сенсора
    Gyro_x_cal[sensor_number] += Gyro_x[sensor_number]; //сложение значений оси X гироскопа
    Gyro_y_cal[sensor_number] += Gyro_y[sensor_number]; //сложение значений оси Y гироскопа
    Gyro_z_cal[sensor_number] += Gyro_z[sensor_number]; //сложение значений оси Z гироскопа
    while (micros() - Loop_start < Loop_time); //ожидание
  }

  Gyro_x_cal[sensor_number] /= 2000; //вычисление среднего значения оси X
  Gyro_y_cal[sensor_number] /= 2000; //вычисление среднего значения оси Y
  Gyro_z_cal[sensor_number] /= 2000; //вычисление среднего значения оси Z

  Serial.println(); //конец строки вывода на экран
}

void calculate_Sensor_Data() //вычисление разницы между сенсорами ("0" - тело, "1" - голова)
{
  int Servo_temp_pitch_position; //временная переменная для "тангажа"
  int Servo_temp_heading_position; //временная переменная для "рыскания"
  int Servo_heading_compensator = 0; //погрешность между измерениями датчиков

  Calculated_heading = Servo_heading_compensator + Heading[Sensor_body] - Heading[Sensor_head]; //вычисление разницы "рыскания" (налево - "+", направо - "-")
  Calc_pitch = Gyro_pitch_output[Sensor_body] - Gyro_pitch_output[Sensor_head]; //вычисление разницы "тангажа" (верх - "+", вниз - "-")
  Calc_roll = Gyro_roll_output[Sensor_body] - Gyro_roll_output[Sensor_head]; //вычисление разницы "крена"

  if (Calculated_heading < -180) //если разница меньше -180 градусов
  {
    Calculated_heading += 360; //увеличить на 360 градусов
  }

  if (Calculated_heading > 180) //если разница больше 180 градусов
  {
    Calculated_heading -= 360; //уменьшить на 360 градусов
  }

  Servo_temp_pitch_position = Servo_default_position + Calc_pitch; //вычисленная позиция "тангажа" серво
  Servo_temp_heading_position = Servo_default_position + Calculated_heading; //вычисленная позиция "рыскания" серво

  if (Servo_temp_pitch_position < 45) //если угол меньше 45
  {
    Servo_current_pitch_position = 45; //держать угол 45
  }
  else if (Servo_temp_pitch_position > 135) //если угол больше 135
  {
    Servo_current_pitch_position = 135; //держать угол 135
  }
  else //в остальных случаях
  {
    Servo_current_pitch_position = Servo_temp_pitch_position; //держать угол равным вычисленному
  }

  if (Servo_temp_heading_position < 45) //если угол меньше 45
  {
    Servo_current_heading_position = 45; //держать угол 45
  }
  else if (Servo_temp_heading_position > 135) //если угол больше 135
  {
    Servo_current_heading_position = 135; //держать угол 135
  }
  else //в остальных случаях
  {
    Servo_current_heading_position = Servo_temp_heading_position; //держать угол равным вычисленному
  }

  Serial.print("Roll: ");
  Serial.print(Gyro_roll_output[Sensor_body]); Serial.print(" - "); Serial.print(Gyro_roll_output[Sensor_head]); Serial.print(" = "); Serial.print(Calc_roll);
  Serial.print("; ");

  Serial.print("Pitch: ");
  Serial.print(Gyro_pitch_output[Sensor_body]); Serial.print(" - "); Serial.print(Gyro_pitch_output[Sensor_head]); Serial.print(" = "); Serial.print(Calc_pitch);
  Serial.print("; ");

  Serial.print("Heading: ");
  Serial.print(Heading[Sensor_body]); Serial.print(" - "); Serial.print(Heading[Sensor_head]); Serial.print(" = "); Serial.print(Calculated_heading);
  Serial.println(";");

}

void write_LCD(int i) //вывод данных на серийный порт компьютера (в конечном варианте программы необязательная функция)
{
  LCD_loop_counter++; //увеличение счетчика циклов на 1

  switch (LCD_loop_counter) //считывание счетчика циклов
  {
    case 1: //счетчик = 1
      LCD_heading_buffer = round(Heading[i]); //обновление данных буфера поворота устройства
      Serial.print("Heading"); Serial.print(i); Serial.print(" = ");
      Serial.print(LCD_heading_buffer); //вывод на экран значения буфера
      break; //конец кейса

    case 2: //счетчик = 2
      Serial.print("\tGyro_pitch = "); //вывод на серийный порт
      LCD_pitch_buffer = Gyro_pitch_output[i] * 10; //обновление данных буфера подъема устройства
      Serial.print(Gyro_pitch_output[i]); //вывод на экран значения буфера
      break; //конец кейса

    case 3: //счетчик = 3
      (LCD_pitch_buffer < 0) ? Serial.print("  -") : Serial.print("  +"); //подъем или спуск?
      break; //конец кейса

    case 4: //счетчик = 4
      Serial.print(abs(LCD_pitch_buffer) / 1000); //вывод на экран "сотни"
      break; //конец кейса

    case 5: //счетчик = 5
      Serial.print((abs(LCD_pitch_buffer) / 100) % 10); //вывод на экран "десятка"
      break; //конец кейса

    case 6: //счетчик = 6
      Serial.print((abs(LCD_pitch_buffer) / 10) % 10); //вывод на экран "единицы"
      break; //конец кейса

    case 7: //счетчик = 7
      Serial.print("."); //вывод на экран десятичной точки
      break; //конец кейса

    case 8: //счетчик = 8
      Serial.print(abs(LCD_pitch_buffer) % 10); //вывод на экран "десятой доли"
      break; //конец кейса

    case 9: //счетчик = 9
      Serial.print("\tGyro_roll = "); //вывод на экран "крена"
      LCD_roll_buffer = Gyro_roll_output[i] * 10; //запись в буфер текущего покозания крена
      Serial.print(Gyro_roll_output[i]); //вывод на экран текущего показания крена
      break; //конец кейса

    case 10: //счетчик = 10
      (LCD_roll_buffer < 0) ? Serial.print("  -") : Serial.print("  +"); //влево или вправо
      break; //конец кейса

    case 11: //счетчик = 11
      Serial.print(abs(LCD_roll_buffer) / 1000); //вывод на экран "сотни"
      break; //конец кейса

    case 12: //счетчик = 12
      Serial.print((abs(LCD_roll_buffer) / 100) % 10); //вывод на экран "десятка"
      break; //конец кейса

    case 13: //счетчик = 13
      Serial.print((abs(LCD_roll_buffer) / 10) % 10); //вывод на экран "единицы"
      break; //конец кейса

    case 14: //счетчик = 14
      Serial.print("."); //вывод на экран десятичной точки
      break; //конец кейса

    case 15: //счетчик = 15
      Serial.println(abs(LCD_roll_buffer) % 10); //вывод на экран "десятой доли"
      LCD_loop_counter = 0; //сброс счетчика
      break; //конец кейса
  }
}

void check_Sensor() //проверка датчиков и вычисления данных сенсоров
{
  for (int sensor_number = 0; sensor_number < sizeof(MPU9250_Add)/2; sensor_number ++) //перебор датчиков по очереди
  {
    read_mpu_6050_data(MPU9250_Add[sensor_number], sensor_number); //считывание регистров с датчика i

    Gyro_x[sensor_number] -= Gyro_x_cal[sensor_number]; //калибровка данных оси Х гироскопа
    Gyro_y[sensor_number] -= Gyro_y_cal[sensor_number]; //калибровка данных оси У гироскопа
    Gyro_z[sensor_number] -= Gyro_z_cal[sensor_number]; //калибровка данных оси Z гироскопа

    Gyro_pitch[sensor_number] += -Gyro_y[sensor_number] * Sensor_to_deg; //преобразование значения оси Y гироскопа в градусы
    Gyro_roll[sensor_number] += Gyro_x[sensor_number] * Sensor_to_deg; //преобразование значения оси X гироскопа в градусы
    Gyro_yaw[sensor_number] += -Gyro_z[sensor_number] * Sensor_to_deg; //преобразование значения оси Z гироскопа в градусы

    Gyro_pitch[sensor_number] += Gyro_roll[sensor_number] * sin(Gyro_z[sensor_number] * Sensor_to_rad); //корректировка значения "тангажа" гироскопа
    Gyro_roll[sensor_number] -= Gyro_pitch[sensor_number] * sin(Gyro_z[sensor_number] * Sensor_to_rad); //корректировка значения "крена" гироскопа

    Accel_total_vector[sensor_number] = sqrt((Accel_x[sensor_number] * Accel_x[sensor_number]) + (Accel_y[sensor_number] * Accel_y[sensor_number]) + (Accel_z[sensor_number] * Accel_z[sensor_number])); //вычисление общего вектора акселерации
    Accel_pitch[sensor_number] = asin((float)Accel_x[sensor_number] / Accel_total_vector[sensor_number]) * RAD_TO_DEG; //вычисление вектора "тангажа" акселерометра
    Accel_roll[sensor_number] = asin((float)Accel_y[sensor_number] / Accel_total_vector[sensor_number]) * RAD_TO_DEG; //вычисление вектора "крена" акселерометра

    Accel_pitch[sensor_number] -= -0.2f; //корректировка значения "тангажа" акселерометра
    Accel_roll[sensor_number] -= -1.6f; //корректировка значения "крена" акселерометра

    if (Gyro_synchronised[sensor_number]) //если значения гироскопа и акселерометра синхронизированы
    {
      Gyro_pitch[sensor_number] = Gyro_pitch[sensor_number] * 0.9996 + Accel_pitch[sensor_number] * 0.0004; //корректируем значение "тангажа" гироскопа относительно значения акселерометра
      Gyro_roll[sensor_number] = Gyro_roll[sensor_number] * 0.9996 + Accel_roll[sensor_number] * 0.0004; //корректируем значение "крена" гироскопа относительно значения акселерометра
    }

    else //если не синхронизированы
    {
      Gyro_pitch[sensor_number] = Accel_pitch[sensor_number]; //присваиваем значение "тангажа" акселерометра гироскопу 
      Gyro_roll[sensor_number] = Accel_roll[sensor_number]; //присваиваем значение "крена" акселерометра гироскопу
      Gyro_synchronised[sensor_number] = true; //значения синхронизированы
    }

    Gyro_pitch_output[sensor_number] = Gyro_pitch_output[sensor_number] * 0.9 + Gyro_pitch[sensor_number] * 0.1; //вычисление выходного значения "тангажа" гироскопа
    Gyro_roll_output[sensor_number] = Gyro_roll_output[sensor_number] * 0.9 + Gyro_roll[sensor_number] * 0.1; //вычисление выходного значения "крена" гироскопа

    read_magnetometer(MPU9250_Add[sensor_number], sensor_number); //считывание данных магнитометра датчика i

    Mag_pitch[sensor_number] = -Gyro_roll_output[sensor_number] * DEG_TO_RAD; //данные для корректировки показаний "тангажа" магнетометра относительно выходных значений "крена" гироскопа
    Mag_roll[sensor_number] = Gyro_pitch_output[sensor_number] * DEG_TO_RAD; //данные для корректировки показаний "крена" магнетометра относительно выходных значений "тангажа" гироскопа

    Mag_x_hor[sensor_number] = Mag_x[sensor_number] * cos(Mag_pitch[sensor_number]) + Mag_y[sensor_number] * sin(Mag_roll[sensor_number]) * sin(Mag_pitch[sensor_number]) - Mag_z[sensor_number] * cos(Mag_roll[sensor_number]) * sin(Mag_pitch[sensor_number]); //вычисление корректирующего значения X оси магнетометра
    Mag_y_hor[sensor_number] = Mag_y[sensor_number] * cos(Mag_roll[sensor_number]) + Mag_z[sensor_number] * sin(Mag_roll[sensor_number]); //вычисление корректирующего значения Y оси магнетометра

    /*Удалить или изменить на калибровку магнетометра*/
//    if (!(digitalRead(Switch))) //если переключение не активно
//    {
//      Mag_x_hor[sensor_number] = Mag_x[sensor_number]; //значению X оси магнетометра присваивается значение с датчика
//      Mag_y_hor[sensor_number] = Mag_y[sensor_number]; //значению Y оси магнетометра присваивается значение с датчика
//    }

    Mag_x_dampened[sensor_number] = Mag_x_dampened[sensor_number] * 0.9 + Mag_x_hor[sensor_number] * 0.1; //скорректированное значение X оси магнетометра
    Mag_y_dampened[sensor_number] = Mag_y_dampened[sensor_number] * 0.9 + Mag_y_hor[sensor_number] * 0.1; //скорректированное значение Y оси магнетометра

    Heading[sensor_number] = atan2(Mag_x_dampened[sensor_number], Mag_y_dampened[sensor_number]) * RAD_TO_DEG; //вычисление значения поворота сенсора в градусах

    Heading[sensor_number] += Declination; //подстройка под географическое расположение датчика

    if (Heading[sensor_number] > 360.0) //если поворот больше 360 градусов
    {
      Heading[sensor_number] -= 360.0; //вычесть 360 градусов
    }

    if (Heading[sensor_number] < 0.0) //если поворот меньше 0 градусов
    {
      Heading[sensor_number] += 360.0; //прибавить 360 градусов
    }

    if (Heading[sensor_number] < 0) //если поворот меньше 0 градусов
    {
      Heading[sensor_number] += 360; //прибавить 360 градусов
    }
    
    if (Heading[sensor_number] >= 360) //если поворот больше 360 градусов
    {
      Heading[sensor_number] -= 360; //вычесть 360 градусов
    }

    // write_LCD(sensor_number);

    // Serial.println(sensor_number);
    // Serial.print(Heading[sensor_number]);
    // Serial.print("\t");
    // Serial.print(Gyro_pitch_output[sensor_number]);
    // Serial.print("\t");
    // Serial.println(Gyro_roll_output[sensor_number]);
  }
}

void Servo_Update() //обновление позиции приводов
{
  servoX.setTargetDeg(Servo_current_pitch_position); //запись нового состояния servoX
  servoX.tick();

  servoZ.write(Servo_current_heading_position); //запись нового состояния servoZ
  servoZ.tick();
}

void Arm_Button_Down() //проверка состояния кнопки включения устройства
{
  Arm_Button.tick(); //опрос кнопки

  if (Arm_Button.isPress()) //если кнопка нажата
  {
    Serial.println("Press!"); // удалить
    Arm_device_on = !Arm_device_on; //изменить флаг включенного устройства на противоположный
  }
}

void Arm_Servo_Up() //включение устройства
{
  Arm_device_time = millis(); //устанавливаем время отсчета

  servoARM.setTargetDeg(Servo_arm_on_position); //привести устройство в состояние включено

  while(millis() - Arm_device_time < 3000) //время на приведение устройства в рабочее состояние
  {
    servoARM.tick(); //двигаем серво
    servoX.setTargetDeg(Servo_current_pitch_position); //начальное значение "тангажа" серво
    servoX.tick(); //двигаем серво
    servoZ.setTargetDeg(Servo_current_heading_position); //начальное значение "рыскания" серво
    servoZ.tick(); //двигаем серво
  }
  
  Arm_device_state = HIGH; //устройство включено
  Serial.println("Ok!"); // удалить
}

void Arm_Servo_Down() //выключение устройства
{
  Arm_device_time = millis(); //устанавливаем время отсчета
  
  servoX.setTargetDeg(ServoX_default_position); //изменить положение servoX в положение по умолчанию
  servoZ.setTargetDeg(ServoZ_default_position); //изменить положение servoZ в положение по умолчанию
  servoARM.setTargetDeg(Servo_arm_off_position); //привести устройство в состояние выключено

  while (millis() - Arm_device_time < 3000) //время на приведение устройства в нерабочее состояние
  {
    servoX.tick(); //двигаем серво
    servoZ.tick(); //двигаем серво
    servoARM.tick(); //двигаем серво
  }

  digitalWrite(Servo_control_pin, LOW); //подаем на вывод управления приводов низкий уровень сигнала, отключаем
  Arm_device_state = LOW; //устройство выключено
  Serial.println("Ok!"); // удалить
}

void setup() //старт программы
{
  Serial.begin(115200); //инициализация серийного интерфейса ардуино со скоростью 115200 бод
  SPI.begin(); //инициализация работы режима SPI

  pinMode(Servo_control_pin, OUTPUT); //устанавливаем режим работы вывода как "выход"
  digitalWrite(Servo_control_pin, LOW); //устанавливаем значение на выводе как "0"

  pinMode(AD4, OUTPUT); //устанавливаем режим работы вывода как "выход"
  pinMode(AD5, OUTPUT); //устанавливаем режим работы вывода как "выход"
  digitalWrite(AD4, HIGH); //устанавливаем значение на выводе как "1"
  digitalWrite(AD5, HIGH); //устанавливаем значение на выводе как "1"

  servoX.attach(8, ServoX_default_position); //привязываем 8 пин к servoX
  servoZ.attach(7, ServoZ_default_position); //привязываем 7 пин к servoZ
  servoARM.attach(6, Servo_arm_off_position); //привязываем 6 пин к servoARM и устанавливаем стартовую позицию

  Arm_Button.setDebounce(50); //настройка антидребезга 50 мс

  Serial.print(" Working pragramm"); //вывод информации на серийный интерфейс
  Serial.println(" V4.01 Double_SPI+Botton+Servo"); //вывод информации на серийный интерфейс
  
  for (int sensor_number = 0; sensor_number < sizeof(MPU9250_Add)/2; sensor_number ++) //цикл для перебора всех датчиков
  {
    Serial.print("Setup sensor "); //вывод информации на серийный интерфейс

    digitalWrite(MPU9250_Add[sensor_number], LOW); //подаем на вывод низкий уровень сигнала, даем доступ к датчику

    SPI_write(MPU9250_Add[sensor_number],MPU9250_PWR_MGMT_1, RESET_REG); //запись в регистр, режим работы с внутренним 20Гц генератором

    configure_magnetometer(MPU9250_Add[sensor_number], sensor_number); //конфигурация магнетометра датчика

    config_gyro(MPU9250_Add[sensor_number]); //конфигурация гироскопа
    calibrate_gyro(MPU9250_Add[sensor_number], sensor_number); //калибровка гироскопа

    digitalWrite(MPU9250_Add[sensor_number], HIGH); //подаем на вывод высокий уровень сигнала, закрываем доступ к датчику

    Serial.println("Config complete."); //вывод информации на серийный интерфейс
  }
}

void loop() //тело программы
{
  check_Sensor(); //считывание данных с датчика
  calculate_Sensor_Data(); //вычисления над данными
  Arm_Button_Down(); //считывание состояния кнопки

  if ((Arm_device_on == HIGH) && (Arm_device_state == LOW)) //если устройство выключено, а кнопка нажата
  {
    Serial.print("Arm UP! "); // удалить
    digitalWrite(Servo_control_pin, HIGH); //подаем на вывод управления приводов высокий уровень сигнала, включаем
    Arm_Servo_Up(); //приводим устройство в состояние включено
  }

  if ((Arm_device_on == LOW) && (Arm_device_state == HIGH)) //если устройство включено, а кнопка нажата
  {
    Serial.print("Arm DOWN! "); // удалить
    Arm_Servo_Down(); //приводим устройство в состояние выключено
    digitalWrite(Servo_control_pin, LOW); //подаем на вывод управления приводов низкий уровень сигнала, выключаем
  }
  
  if (Arm_device_state == HIGH) //если устройство включено
  {
    Servo_Update(); //обновить состояние приводов
  }
}

